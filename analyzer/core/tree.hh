#ifndef ANALYZER_CORE_TREE_LOADED 
#define ANALYZER_CORE_TREE_LOADED

#include <cstdint>
#include <memory>
#include <string>
#include <vector>

#include <TFile.h>
#include <TTree.h>
#include <TChain.h>
#include <TTreeReader.h>

#include <analyzer/core/exception.hh>

namespace analyzer {
// error prototypes
// Note that there are only specialized tree_writer_errors.
// tree_reader_errors are communicated through bool status codes, as they do not
// necessarily correspond to a critical problem (eg. EOF markers).
class tree_error;
class tree_writer_error;
class tree_writer_file_error;
class tree_writer_mem_error;
}

// =============================================================================
// tree_reader:
//  Base class for tree readers, does all the generic TTreeReader bookkeeping.
//
// Derived classes are responsible for the TTreeReaderValues/Arrays.
// =============================================================================
namespace analyzer {
class tree_reader {
public:
  tree_reader(const std::vector<std::string>& fnames, const std::string& tname);

  // iterator to begin/end of TTreeReader
  TTreeReader::Iterator_t begin() { return reader_.begin(); }
  TTreeReader::Iterator_t end() { return reader_.end(); }
  bool next() { return reader_.Next(); }
  bool skip(size_t index) {
    return reader_.SetEntry(index) == TTreeReader::kEntryValid;
  }
  size_t index() const { return reader_.GetCurrentEntry(); }
  size_t size() const {
    if (tree_) {
      return tree_->GetEntries();
    }
    return 0;
  }

  operator bool() const {
    return (reader_.GetEntryStatus() == TTreeReader::kEntryValid);
  }

  std::shared_ptr<TChain> tree() { return tree_; }

protected:
  std::shared_ptr<TChain> tree_;
  TTreeReader reader_;
};
} // ns analyzer

// =============================================================================
// tree_writer:
//   * Create an output file for an arbitrary datatype DataType
//   * specialization: tree_writer<tree_reader> copies (skims) a tree
// =============================================================================
namespace analyzer {
template <class DataType> class tree_writer {
public:
  using data_type = DataType;
  tree_writer(std::shared_ptr<TFile> file, const std::string& name,
              const std::string& title = "");
  ~tree_writer();
  void fill(const data_type& data);

private:
  void error(const std::string& msg) const;

  std::shared_ptr<TFile> file_;
  TTree* tree_; // managed by file_;
  data_type event_buffer_;
};
} // ns analyzer 
// =============================================================================
// tree_writer<tree_reader> (skimming) specialization
//
// The slim_tree boolean determines if we want to copy the all input tree
// entries, or only the ones we accessed. Note that copying the full tree entry
// is slower and more space-consuming.
// =============================================================================
namespace analyzer {
template <> class tree_writer<tree_reader> {
public:
  using data_type = tree_reader;
  tree_writer(std::shared_ptr<TFile> file, tree_reader& r,
              bool slim_tree = true);
  ~tree_writer();
  void fill();

private:
  void error(const std::string& msg) const;

  std::shared_ptr<TFile> file_;
  TTree* tree_; // managed by file_;
  tree_reader& reader_;
  const bool slim_tree_;
};
} // ns analyzer
// =============================================================================
// Definitions: exceptions
// =============================================================================
namespace analyzer {
class tree_error : public analyzer::exception {
public:
  tree_error(const std::string& msg, const std::string& type = "tree_error");
};
class tree_writer_error : public tree_error {
public:
  tree_writer_error(const std::string& msg,
                    const std::string& type = "tree_writer_error");
};
class tree_writer_file_error : public tree_writer_error {
public:
  tree_writer_file_error(const std::string& msg, const std::string& tname);
};
struct tree_writer_mem_error : public tree_writer_error {
public:
  tree_writer_mem_error(const std::string& msg);
};
} // ns analyzer
// =============================================================================
// Implementation: tree_writer
// =============================================================================
namespace analyzer {
template <class DataType>
tree_writer<DataType>::tree_writer(std::shared_ptr<TFile> file,
                                   const std::string& name,
                                   const std::string& title)
    : file_{std::move(file)}, tree_{nullptr} {
  if (file_) {
    file_->cd();
    tree_ = new TTree(name.c_str(), title.c_str());
    tree_->Branch("data", &event_buffer_);
  } else {
    error("Failed to create tree.");
  }
}
template <class DataType> tree_writer<DataType>::~tree_writer() {
  if (file_ && tree_) {
    file_->cd();
    tree_->AutoSave();
  } else {
    error("Failed to write tree.");
  }
}
template <class DataType>
void tree_writer<DataType>::error(const std::string& msg) const {
  if (!file_ && tree_) {
    throw tree_writer_file_error{msg, tree_->GetName()};
  } else if (!tree_) {
    throw tree_writer_mem_error(msg);
  } else {
    throw tree_writer_error{msg};
  }
}
template <class DataType>
void tree_writer<DataType>::fill(const data_type& data) {
  if (file_ && tree_) {
    event_buffer_ = data;
    file_->cd();
    tree_->Fill();
  } else {
    error("Failed to fill tree.");
  }
}
inline void tree_writer<tree_reader>::fill() {
  if (file_ && tree_) {
    // load the full tree entry if we are not slimming
    if (!slim_tree_) {
      if (!reader_.tree()) {
        throw tree_writer_mem_error{"Failed to load full reader entry."};
      }
      reader_.tree()->GetEntry(reader_.index());
    }
    file_->cd();
    tree_->Fill();
  } else {
    error("Failed to fill tree.");
  }
}
} // ns analyzer

#endif
