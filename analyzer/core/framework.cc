#include "framework.hh"

#include <exception>
#include <cstdlib>

#include <analyzer/core/exception.hh>
#include <analyzer/core/configuration.hh>
#include <analyzer/core/logger.hh>
#include <analyzer/core/stringify.hh>

#include <TSystem.h>

#include <boost/exception/diagnostic_information.hpp>
#include <boost/filesystem.hpp>

// useful aliases
namespace fs = boost::filesystem;
namespace po = boost::program_options;
// utility functions (unnamed namespace)
namespace {
void file_exists(const std::string& file) {
  if (!fs::exists(file)) {
    throw analyzer::framework_file_error{file};
  }
}
} // ns unnamed

namespace analyzer {
// =============================================================================
// framework constructor: suppress ROOT signals, parse command line arguments
// and provide for error handling
// =============================================================================
framework::framework(int argc, char* argv[],
                     analyzer_function_type analyzer_function)
    : analyzer_function_{analyzer_function} {
  try {
    LOG_INFO("analyzer", "Starting analyzer framework");
    // suppress ROOT signal handler
    root_suppress_signals();
    // parse the command line
    args_ = parse_arguments(argc, argv);
    // input files:
    input_ = get_input_files();
    LOG_INFO("analyzer",
             "Number of files to process: " + std::to_string(input_.size()));
    LOG_DEBUG("analyzer", "Files to process: " + stringify(input_));
    // output root
    output_ = args_["out"].as<std::string>();
    LOG_INFO("analyzer", "Output files will be written to: " + output_ + ".*");
    // configuration
    settings_ = get_settings();
    LOG_INFO("analyzer", "Configuration file: " + args_["conf"].as<std::string>());
  } catch (const framework_help& h) {
    std::cerr << h.what() << std::endl;
    exit(0);
  } catch (const analyzer::exception& e) {
    LOG_ERROR(e.type(), e.what());
    LOG_ERROR(e.type(), "Run with -h for help.");
    throw e;
  } catch (const boost::exception& e) {
    LOG_CRITICAL("boost::exception", boost::diagnostic_information(e));
    LOG_CRITICAL("boost::exception", "Unhandled boost exception");
    LOG_CRITICAL("boost::exception", "Please contact developer for support.");
    throw analyzer::exception("Unhandled boost exception", "boost::exception");
  } catch (const std::exception& e) {
    LOG_CRITICAL("std::exception", e.what());
    LOG_CRITICAL("std::exception", "Unhandled standard exception");
    LOG_CRITICAL("std::exception", "Please contact developer for support.");
    throw analyzer::exception("Unhandled standard exception", "std::exception");
  }
}
int framework::run() const {
  try{
    LOG_INFO("analyzer", "Starting data processing...");
    int ret = analyzer_function_(settings_, input_, output_);
    LOG_INFO("analyzer", "Data processing complete.");
    return ret;
  } catch (const analyzer::exception& e) {
    LOG_ERROR(e.type(), e.what());
    LOG_ERROR(e.type(), "Run with -h for help.");
    throw e;
  } catch (const boost::exception& e) {
    LOG_CRITICAL("boost::exception", boost::diagnostic_information(e));
    LOG_CRITICAL("boost::exception", "Unhandled boost exception");
    LOG_CRITICAL("boost::exception", "Please contact developer for support.");
    throw analyzer::exception("Unhandled boost exception", "boost::exception");
  } catch (const std::exception& e) {
    LOG_CRITICAL("std::exception", e.what());
    LOG_CRITICAL("std::exception", "Unhandled standard exception");
    LOG_CRITICAL("std::exception", "Please contact developer for support.");
    throw analyzer::exception("Unhandled standard exception", "std::exception");
  }
}
} // ns analyzer

// =============================================================================
// framework private utility functions 
// =============================================================================
namespace analyzer {
// =============================================================================
// Implementation: framework::parse_arguments
// Also sets the verbosity level to what was requested
// =============================================================================
po::variables_map framework::parse_arguments(int argc, char* argv[]) const {
  po::variables_map args;
  try {
    po::options_description opts_visible{"Allowed options"};
    opts_visible.add_options()("help,h", "Produce help message")(
        "force,f", "Ignore missing input files")(
        "conf,c", po::value<std::string>()->required()->notifier(file_exists),
        "Configuration JSON file")(
        "verb,v", po::value<unsigned>()->default_value(
                           static_cast<unsigned>(log_level::INFO)),
        "Verbosity level (0 -> 7; 0: silent, 4: default, 5: debug)")(
        "flist,i", po::value<std::string>()->notifier(file_exists),
        "Run list")("out,o", po::value<std::string>()->required(),
                    "Output file name root");
    po::options_description opts_hidden{"Hidden options"};
    opts_hidden.add_options()(
        "input-file", po::value<std::vector<std::string>>()->composing(),
        "Input file");
    po::options_description opts_flags;
    opts_flags.add(opts_visible).add(opts_hidden);
    po::positional_options_description opts_positional;
    opts_positional.add("input-file", -1);

    po::store(po::command_line_parser(argc, argv)
                  .options(opts_flags)
                  .positional(opts_positional)
                  .run(),
              args);

    // help message requested? (BEFORE notify!)
    if (args.count("help")) {
      throw framework_help{argv[0], opts_visible};
    }
    // do our actual processing
    po::notify(args);
    // set the verbosity level if requested
    if (args.count("verb")) {
      unsigned v{args["verb"].as<unsigned>()};
      LOG_INFO("analyzer", "Verbosity level: " + std::to_string(v));
      global::logger.set_level(v);
    }
    return args;
  } catch (const po::error& e) {
    throw framework_error{e.what()};
  }
  return args;
}
// =============================================================================
// Implementation: frameowork::get_input_files
// =============================================================================
std::vector<std::string> framework::get_input_files() const {
  std::vector<std::string> input;
  // first, get the positional files
  if (args_.count("input-file")) {
    input = args_["input-file"].as<std::vector<std::string>>();
  }
  // then get the input files from the flist
  if (args_.count("flist")) {
    std::ifstream flist{args_["flist"].as<std::string>()};
    while (!flist.eof()) {
      std::string name;
      flist >> name;
      if (name.size()) {
        input.push_back(std::move(name));
      }
    }
  }
  if (!input.size()) {
    throw framework_error(
        "Program requires at least one input file or file list (--flist)");
  }
  // check the input files for validity
  if (!args_.count("force")) {
    for (const auto& file : input) {
      file_exists(file);
    }
  }
  return input;
}
// =============================================================================
// Implementation: framework::get_settings
// =============================================================================
ptree framework::get_settings() const {
  ptree settings;
  try {
    read_json(args_["conf"].as<std::string>(), settings);
  } catch (const boost::property_tree::ptree_error& e) {
    LOG_ERROR("framework_parse_error", e.what());
    throw framework_parse_error{args_["conf"].as<std::string>()};
  }
  return settings;
}
// =============================================================================
// Implementation: framework::root_suppress_signals
// Suppress the ROOT signal handlers, as they can cause undefined behavior and
// interfere with debugging
// =============================================================================
void framework::root_suppress_signals() const {
  gSystem->ResetSignal(kSigChild);
  gSystem->ResetSignal(kSigBus);
  gSystem->ResetSignal(kSigSegmentationViolation);
  gSystem->ResetSignal(kSigIllegalInstruction);
  gSystem->ResetSignal(kSigSystem);
  gSystem->ResetSignal(kSigPipe);
  gSystem->ResetSignal(kSigAlarm);
  gSystem->ResetSignal(kSigUrgent);
  gSystem->ResetSignal(kSigFloatingException);
  gSystem->ResetSignal(kSigWindowChanged);
}
} // ns analyzer

// =============================================================================
// Implementation: Exceptions
// =============================================================================
namespace analyzer {
framework_file_error::framework_file_error(const std::string& file)
    : framework_error{"No such file or directory: " + file,
                      "framework_file_error"} {}
framework_parse_error::framework_parse_error(const std::string& file)
    : framework_error{"Failed to parse: " + file, "framework_parse_error"} {}
framework_help::framework_help(const std::string& program,
                               const po::options_description& opts)
    : framework_error{message(program, opts), "help"} {}
std::string framework_help::message(const std::string& program,
                                    const po::options_description& opts) const {
  std::stringstream ss;
  ss << "\nUsage: " << program << " [options] [input-files...]\n" << opts
     << "\n";
  return ss.str();
}
} // ns analyzer
