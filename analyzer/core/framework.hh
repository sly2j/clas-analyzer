#ifndef ANALYZER_FRAMEWORK_LOADED
#define ANALYZER_FRAMEWORK_LOADED

#include <functional>
#include <vector>
#include <string>

#include <analyzer/core/configuration.hh>

#include <boost/program_options.hpp>

namespace analyzer {
// error prototypes
class framework_error;
class framework_help;
class framework_file_error;
class framework_parse_error;
} // ns analyzer

// =============================================================================
// analyser::framework
//
// If you want to use the framework, call MAKE_ANALYZER_FRAMEWORK(function) at
// the end of your main source file. "function" is the analysis function you
// want to call. The analysis function should take the following inputs:
//    * settings: ptree made from the input configuration file
//    * input: std::vector of the input file names
//    * output: the base name for the output files.
//  Note:
//    * the framework takes care of all fancy exception handling
// =============================================================================
#define MAKE_ANALYZER_FRAMEWORK(function)                                      \
  int main(int argc, char* argv[]) {                                           \
    try {                                                                      \
      analyzer::framework analyzer{argc, argv, (function)};                    \
      analyzer.run();                                                          \
      return 0;                                                                \
    } catch (...) {                                                            \
      return 1;                                                                \
    }                                                                          \
  }

namespace analyzer {
class framework {
public:
  using analyzer_function_type = std::function<int(
      const ptree& settings, const std::vector<std::string>& input,
      const std::string& output)>;

  // setup the analysis framework
  framework(int argc, char* argv[], analyzer_function_type analyzer_function);
  // run the analyzis framework
  int run() const;

private:
  boost::program_options::variables_map parse_arguments(int argc,
                                                        char* argv[]) const;
  std::vector<std::string> get_input_files() const;
  ptree get_settings() const;
  void root_suppress_signals() const;

  analyzer_function_type analyzer_function_;
  boost::program_options::variables_map args_;
  std::vector<std::string> input_;
  std::string output_;
  ptree settings_;
};
} // ns analyzer

// =============================================================================
// Definition: exceptions
// =============================================================================
namespace analyzer {
class framework_error : public analyzer::exception {
public:
  framework_error(const std::string& msg,
                  const std::string& type = "framework_error")
      : analyzer::exception{msg, type} {}
};
class framework_help : public framework_error {
public:
  framework_help(const std::string& program,
                 const boost::program_options::options_description& opts);

private:
  std::string
  message(const std::string& program,
          const boost::program_options::options_description& opts) const;
};
class framework_file_error : public framework_error {
public:
  framework_file_error(const std::string& file);
};
class framework_parse_error : public framework_error {
public:
  framework_parse_error(const std::string& file);
};
} // ns analyzer

#endif
