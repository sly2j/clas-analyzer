#ifndef ANALYZER_CORE_MATH_LOADED
#define ANALYZER_CORE_MATH_LOADED

#include <cmath>
#include <cassert>
#include <vector>
#include <TVector3.h>
#include <TLorentzVector.h>

namespace analyzer {

namespace constants {
// pi
constexpr double pi{
    3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214808651e+00};
} // ns constants

// =======================================================================================
// eval_polynomial(params, x):
//
// evaluate an arbitrary polynomial of the form p0 + p1*x + p2*x^2 + ...
// given a value x and a compatible Range with its parameters {p0, p1, p2, ...}
// =======================================================================================
template <class Range, class T>
T eval_polynomial(const Range& params, const T x) {
  T x_pow{1};
  T ret{0};
  for (const auto& p : params) {
    ret += p * x_pow;
    x_pow *= x;
  }
  return ret;
}


// =======================================================================================
// atan2 version that returns an angle in [0, 2pi] instead of [-pi, pi]
// =======================================================================================
inline double atan2_pos(const double y, const double x) {
  double phi{atan2(y, x)};
  if (phi < 0) {
    phi += 2 * constants::pi;
  }
  assert(phi > 0);
  return phi;
}
// =======================================================================================
// Convert between radians and degrees
// =======================================================================================
inline double radian_to_degree(const double angle) {
  return angle / constants::pi * 180.;
}
inline double degree_to_radian(const double angle) {
  return angle * constants::pi / 180.;
}

// =======================================================================================
// Angle between two vectors
// =======================================================================================
inline double angle(const TVector3& v1, const TVector3& v2) {
  return v1.Angle(v2);
}
inline double angle(const TLorentzVector& v1, const TLorentzVector& v2) {
  return angle(v1.Vect(), v2.Vect());
}
// =======================================================================================
// Angle between the planes defined by (v1, v2) and (v1, v3)
// =======================================================================================
inline double planar_angle(const TVector3& v1, const TVector3& v2,
                           const TVector3& v3) {
  const TVector3 n12{v1.Cross(v2)};
  const TVector3 n23{v1.Cross(v3)};
  const int hemifactor = (n12.Dot(v3) > 0) ? 1 : -1;
  double angle{n12.Angle(n23)};
  return hemifactor * angle;
}
inline double planar_angle(const TLorentzVector& v1, const TLorentzVector& v2,
                           const TLorentzVector& v3) {
  return planar_angle(v1.Vect(), v2.Vect(), v3.Vect());
}
// =======================================================================================
// Smallest angle between the planes defined by (v1, v2) and (v1, v3) (coplanarity angle)
// =======================================================================================
inline double coplanarity(const TVector3& v1, const TVector3& v2,
                          const TVector3& v3) {
  double angle(planar_angle(v1, v2, v3));
  if (angle > 0.5 * constants::pi) {
    angle -= constants::pi;
  } else if (angle < -0.5 * constants::pi) {
    angle += constants::pi;
  }
  return angle;
}
inline double coplanarity(const TLorentzVector& v1, const TLorentzVector& v2,
                          const TLorentzVector& v3) {
  return coplanarity(v1.Vect(), v2.Vect(), v3.Vect());
}

} // ns analyzer

#endif
