#ifndef ANALYZER_CORE_CUT_LOADED
#define ANALYZER_CORE_CUT_LOADED

#include <memory>

#include <TFile.h>

#include <analyzer/core/histogrammer.hh>
#include <analyzer/core/interval.hh>
#include <analyzer/core/configuration.hh>

namespace analyzer {

// CRTP base class for all cuts. Contains the facilities to automatically
// generate custom pre/post cut histograms.
//
// derived cut classes must implement at least the cut_impl(Reader&) member
// function (returns: false on failure, true on success)
template <class DerivedCut, class... Data> class cut : public configurable {
public:
  using cut_type = DerivedCut;
  using histogrammer_type = histogrammer<Data...>;
  using histo_var_type = typename histogrammer_type::histo_var_type;

  cut(const ptree& settings, const string_path& path, const std::string& title)
      : configurable{settings, path}
      , pre_histos_{path, "pre", format_title(title, "before:")}
      , post_histos_{path, "post", format_title(title, "after:")} {}
  cut(const cut&) = delete;
  cut& operator=(const cut&) = delete;

  // add a 1D histo
  void add_histo(std::shared_ptr<TFile> file, const std::string& name,
                 const std::string& title, const histo_var_type& var) {
    pre_histos_.add_histo(file, name, title, var);
    post_histos_.add_histo(file, name, title, var);
  }
  void add_histo(std::shared_ptr<TFile> file, const std::string& name,
                 const histo_var_type& var) {
    add_histo(std::move(file), name, name, var);
  }
  // add a 2D histo
  void add_histo(std::shared_ptr<TFile> file, const std::string& name,
                 const std::string& title, const histo_var_type& var_x,
                 const histo_var_type& var_y) {
    pre_histos_.add_histo(file, name, title, var_x, var_y);
    post_histos_.add_histo(file, name, title, var_x, var_y);
  }
  void add_histo(std::shared_ptr<TFile> file, const std::string& name,
                 const histo_var_type& var_x, const histo_var_type& var_y) {
    add_histo(std::move(file), name, name, var_x, var_y);
  }

  bool apply(Data... d) {
    pre_histos_.fill(d...);
    bool ok = derived().cut_impl(d...);
    // fill the post histos if we passed the cut
    if (ok) {
      post_histos_.fill(d...);
    }
    return ok;
  }
  bool operator()(Data... d) { return apply(d...); }

private:
  cut_type& derived() { return static_cast<cut_type&>(*this); }
  const cut_type& derived() const {
    return static_cast<const cut_type&>(*this);
  }

  histogrammer_type pre_histos_;
  histogrammer_type post_histos_;
};

} // ns analyzer

#ifndef ANALYZER_CORE_CUT_INTERNAL
#include <analyzer/core/cut/basic_cut.hh>
#include <analyzer/core/cut/gaus_cut.hh>
#include <analyzer/core/cut/polygaus_cut.hh>
#include <analyzer/core/cut/range_cut.hh>
#include <analyzer/core/cut/polyrange_cut.hh>
#include <analyzer/core/cut/shape_cut.hh>
#endif

#endif
