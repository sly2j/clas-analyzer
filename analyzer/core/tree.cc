#include <analyzer/core/tree.hh>

// =============================================================================
// Implementation: tree_reader
// =============================================================================
namespace analyzer {
tree_reader::tree_reader(const std::vector<std::string>& fnames,
                         const std::string& tname)
    : tree_{std::make_shared<TChain>(tname.c_str())}, reader_{tree_.get()} {
  for (const auto& fname : fnames) {
    tree_->Add(fname.c_str());
  }
}
} // ns analyzer
// =============================================================================
// Implementation: tree_writer<tree_reader>
// =============================================================================
namespace analyzer {
tree_writer<tree_reader>::tree_writer(std::shared_ptr<TFile> file,
                                      tree_reader& r, bool slim_tree)
    : file_{std::move(file)}
    , tree_{nullptr}
    , reader_{r}
    , slim_tree_{slim_tree} {
  if (file_ && reader_.tree()) {
    file_->cd();
    if (!slim_tree_) {
      reader_.tree()->SetBranchStatus("*", 1);
    }
    tree_ = reader_.tree()->CloneTree(0);
  } else {
    error("Failed to create tree copy.");
  }
}
tree_writer<tree_reader>::~tree_writer() {
  if (file_ && tree_) {
    file_->cd();
    tree_->AutoSave();
  } else {
    error("Failed to write tree.");
  }
}
void tree_writer<tree_reader>::error(const std::string& msg) const {
  if (!file_ && tree_) {
    throw tree_writer_file_error{msg, tree_->GetName()};
  } else if (!tree_) {
    throw tree_writer_mem_error{msg};
  } else {
    throw tree_writer_error{msg};
  }
}
} // ns analyzer
// =============================================================================
// Implementation: exceptions
// =============================================================================
namespace analyzer {
tree_error::tree_error(const std::string& msg, const std::string& type)
    : analyzer::exception{msg, type} {}
tree_writer_error::tree_writer_error(const std::string& msg,
                                     const std::string& type)
    : tree_error{msg, type} {}
tree_writer_file_error::tree_writer_file_error(const std::string& msg,
                                               const std::string& tname)
    : tree_writer_error{msg + "Tree: " + tname, "tree_writer_file_error"} {}
tree_writer_mem_error::tree_writer_mem_error(const std::string& msg)
    : tree_writer_error{msg, "tree_writer_mem_error"} {}
} // ns analyzer
