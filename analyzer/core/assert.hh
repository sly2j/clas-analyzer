#ifndef ANALYZER_CORE_ASSERT_LOADED
#define ANALYZER_CORE_ASSERT_LOADED

#include <analyzer/core/logger.hh>
#include <analyzer/core/exception.hh>
#include <string>

// =============================================================================
// throwing assert throws an analyzer::exception with verbose error message.
// =============================================================================
#define tassert(condition, msg)                                                \
  if (!(condition)) {                                                          \
    analyzer::tassert_impl(#condition, __FILE__, __LINE__, msg);               \
  }

// =============================================================================
// implementation: throwing assert
// =============================================================================
namespace analyzer {
inline void tassert_impl(const std::string& condition,
                         const std::string& location, const int line,
                         const std::string& msg) {
  LOG_ERROR(location,
            "l" + std::to_string(line) + ": assert(" + condition + ") failed");
  throw analyzer::exception{msg, "assert"};
}
} // ns analyzer

#endif
