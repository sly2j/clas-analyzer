#ifndef ANALYZER_CORE_CUT_GAUS_CUT_LOADED
#define ANALYZER_CORE_CUT_GAUS_CUT_LOADED

#include <analyzer/core/configuration.hh>
#include <analyzer/core/cut/basic_cut.hh>
#include <analyzer/core/math.hh>
#include <cstdint>
#include <vector>
#include <functional>

namespace analyzer {
namespace cut_impl {
template <class T, class... Data> class gaus_condition {
public:
  using value_type = T;
  using getter_type = std::function<value_type(Data...)>;

private:
  struct specification {
    const std::string name;
    const getter_type getter;
    specification(const std::string& name, const getter_type& getter)
        : name{name}, getter{getter} {}
  };

public:
  using spec_type = specification;

  gaus_condition(const configuration& conf, const spec_type& spec)
      : spec_{spec}
      , mean_{conf.get<value_type>(spec.name + "/mean")}
      , sigma_{conf.get<value_type>(spec.name + "/sigma")}
      , n_sigma_{conf.get<value_type>(spec.name + "/n_sigma")} {}

  bool apply(Data... d) const {
    value_type val{spec_.getter(d...)};

    return (fabs(val - mean_) < sigma_ * n_sigma_);
  }

private:
  const spec_type spec_;
  const value_type mean_;
  const value_type sigma_;
  const value_type n_sigma_;
};
} // ns cut_impl

template <class T, class... Data>
using gaus_cut = basic_cut<cut_impl::gaus_condition<T, Data...>, Data...>;

} // ns analyzer

#endif
