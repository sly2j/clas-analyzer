#ifndef ANALYZER_CORE_CUT_BASIC_CUT_LOADED
#define ANALYZER_CORE_CUT_BASIC_CUT_LOADED

#ifndef ANALYZER_CORE_CUT_INTERNAL
#define ANALYZER_CORE_CUT_INTERNAL
#endif

#include <analyzer/core/configuration.hh>
#include <analyzer/core/cut.hh>

#include <cstdint>
#include <vector>

namespace analyzer {

// =======================================================================================
// basic_cut<Condition, Data...>
//
// A basic cut that checks a set of conditions on our input Data... 
//
// the Condition type needs to conform to:
//    * defines an spec_type
//    * constructor takes a configuration and an spec_type object
//    * has an "bool apply(Data...)" member function that evaluates the
//      condition on the current input data.
// =======================================================================================
template <class Condition, class... Data>
class basic_cut : public cut<basic_cut<Condition, Data...>, Data...> {
public:
  using parent_type = cut<basic_cut, Data...>;
  using condition_type = Condition;
  using spec_type = typename condition_type::spec_type;

public:
  basic_cut(const ptree& settings, const string_path& path,
            const std::string& title, const std::vector<spec_type>& spec)
      : parent_type(settings, path, title), cuts_(construct_cuts(spec)) {}
  basic_cut(const ptree& settings, const string_path& path,
            const std::string& title, const spec_type& spec)
      : basic_cut{settings, path, title, std::vector<spec_type>{spec}} {}

  bool cut_impl(Data... d) {
    for (const auto& cut : cuts_) {
      if (!cut.apply(d...)) {
        return false;
      }
    }
    return true;
  }

private:
  std::vector<condition_type>
  construct_cuts(const std::vector<spec_type>& spec) const {
    std::vector<condition_type> cuts;
    for (const auto& cut : spec) {
      cuts.push_back({parent_type::conf(), cut});
    }
    return cuts;
  }

  const std::vector<condition_type> cuts_;
};

} // ns analyzer

#ifdef ANALYZER_CORE_CUT_INTERNAL
#undef ANALYZER_CORE_CUT_INTERNAL
#endif

#endif
