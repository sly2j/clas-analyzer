#ifndef ANALYZER_CORE_CUT_POLYGAUS_CUT_LOADED
#define ANALYZER_CORE_CUT_POLYGAUS_CUT_LOADED

#include <analyzer/core/configuration.hh>
#include <analyzer/core/cut/basic_cut.hh>
#include <analyzer/core/math.hh>
#include <cstdint>
#include <vector>
#include <functional>

namespace analyzer {
namespace cut_impl {
template <class T, class... Data> class polygaus_condition {
public:
  using value_type = T;
  using getter_type = std::function<value_type(Data...)>;

private:
  struct specification {
    const std::string name;
    const getter_type x_getter;
    const getter_type y_getter;
    specification(const std::string& name, const getter_type& x_getter,
                  const getter_type& y_getter)
        : name{name}, x_getter{x_getter}, y_getter{y_getter} {}
  };

public:
  using spec_type = specification;

  polygaus_condition(const configuration& conf, const spec_type& spec)
      : spec_{spec}
      , mean_param_{conf.get_vector<value_type>(spec.name + "/mean")}
      , sigma_param_{conf.get_vector<value_type>(spec.name + "/sigma")}
      , n_sigma_{conf.get<value_type>(spec.name + "/n_sigma")} {}

  bool apply(Data... d) const {
    value_type x{spec_.x_getter(d...)};
    value_type y{spec_.y_getter(d...)};
    value_type mean{eval_polynomial(mean_param_, x)};
    value_type sigma{eval_polynomial(sigma_param_, x)};

    return (fabs(y - mean) < sigma * n_sigma_);
  }

private:
  const spec_type spec_;
  const std::vector<value_type> mean_param_;
  const std::vector<value_type> sigma_param_;
  const value_type n_sigma_;
};
} // ns cut_impl

template <class T, class... Data>
using polygaus_cut =
    basic_cut<cut_impl::polygaus_condition<T, Data...>, Data...>;

} // ns analyzer

#endif
