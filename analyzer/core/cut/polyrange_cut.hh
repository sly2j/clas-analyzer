#ifndef ANALYZER_CORE_CUT_POLYRANGE_CUT_LOADED
#define ANALYZER_CORE_CUT_POLYRANGE_CUT_LOADED

#include <analyzer/core/configuration.hh>
#include <analyzer/core/cut/basic_cut.hh>
#include <analyzer/core/math.hh>
#include <cstdint>
#include <vector>
#include <functional>

namespace analyzer {
namespace cut_impl {
template <class T, class... Data> class polyrange_condition {
public:
  using value_type = T;
  using getter_type = std::function<value_type(Data...)>;

private:
  struct specification {
    const std::string name;
    const getter_type x_getter;
    const getter_type y_getter;
    specification(const std::string& name, const getter_type& x_getter,
                  const getter_type& y_getter)
        : name{name}, x_getter{x_getter}, y_getter{y_getter} {}
  };

public:
  using spec_type = specification;

  polyrange_condition(const configuration& conf, const spec_type& spec)
      : spec_{spec}
      , min_param_{conf.get_vector<value_type>(spec.name + "/min")}
      , max_param_{conf.get_vector<value_type>(spec.name + "/max")} {}

  bool apply(Data... d) const {
    value_type x{spec_.x_getter(d...)};
    value_type y{spec_.y_getter(d...)};
    value_type min{eval_polynomial(min_param_, x)};
    value_type max{eval_polynomial(max_param_, x)};

    return (y > min && y < max);
  }

private:
  const spec_type spec_;
  const std::vector<value_type> min_param_;
  const std::vector<value_type> max_param_;
};
} // ns cut_impl

template <class T, class... Data>
using polyrange_cut =
    basic_cut<cut_impl::polyrange_condition<T, Data...>, Data...>;

} // ns analyzer

#endif
