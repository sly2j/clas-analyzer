#ifndef ANALYZER_CORE_CUT_SHAPE_CUT_LOADED
#define ANALYZER_CORE_CUT_SHAPE_CUT_LOADED

#include <analyzer/core/configuration.hh>
#include <analyzer/core/cut/basic_cut.hh>
#include <analyzer/core/math.hh>
#include <cstdint>
#include <vector>
#include <functional>

#include <TMath.h>

namespace analyzer {
namespace cut_impl {
template <class T, class... Data> class shape_condition {
public:
  using value_type = T;
  using getter_type = std::function<value_type(Data...)>;

private:
  struct specification {
    const std::string name;
    const getter_type x_getter;
    const getter_type y_getter;
    specification(const std::string& name, const getter_type& x_getter,
                  const getter_type& y_getter)
        : name{name}, x_getter{x_getter}, y_getter{y_getter} {}
  };

public:
  using spec_type = specification;

  shape_condition(const configuration& conf, const spec_type& spec)
      : spec_{spec}
      , x_coords_{conf.get_vector<value_type>(spec.name + "/x")}
      , y_coords_{conf.get_vector<value_type>(spec.name + "/y")}
      , inside_{conf.get_optional<bool>(spec.name + "/inside")
                    ? *conf.get_optional<bool>(spec.name + "/inside")
                    : true} {
    if (x_coords_.size() != y_coords_.size()) {
      throw conf.value_error(spec.name, "<INCOMPATIBLE ARRAY LENGTHS>");
    }
  }

  bool apply(Data... d) const {
    value_type x{spec_.x_getter(d...)};
    value_type y{spec_.y_getter(d...)};
    bool ret{TMath::IsInside<const value_type>(x, y, x_coords_.size(),
                                               &x_coords_[0], &y_coords_[0])};
    return inside_ ? ret : !ret;
  }

private:
  const spec_type spec_;
  const bool inside_;
  std::vector<value_type> x_coords_; // not const because IsInside takes
  std::vector<value_type> y_coords_; // raw pointers
};
} // ns cut_impl

template <class T, class... Data>
using shape_cut = basic_cut<cut_impl::shape_condition<T, Data...>, Data...>;

} // ns analyzer

#endif
