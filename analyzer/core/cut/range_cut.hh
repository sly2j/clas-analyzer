#ifndef ANALYZER_CORE_CUT_RANGE_CUT_LOADED
#define ANALYZER_CORE_CUT_RANGE_CUT_LOADED

#include <analyzer/core/configuration.hh>
#include <analyzer/core/interval.hh>
#include <analyzer/core/cut/basic_cut.hh>
#include <vector>
#include <cstdint>
#include <functional>

namespace analyzer {
namespace cut_impl {
template <class T, class... Data> class range_condition {
public:
  using value_type = T;
  using getter_type = std::function<value_type(Data...)>;
  using range_type = interval<T>;

private:
  struct specification {
    const std::string name;
    const getter_type getter;
    specification(const std::string& name, const getter_type& getter)
        : name{name}, getter{getter} {}
  };

public:
  using spec_type = specification;

  range_condition(const configuration& conf, const spec_type& spec)
      : spec_{spec}
      , range_{conf.get<value_type>(spec.name + "/min"),
               conf.get<value_type>(spec.name + "/max")} {}

  bool apply(Data... d) const {
    value_type val{spec_.getter(d...)};
    return range_.includes(val);
  }

private:
  const spec_type spec_;
  const range_type range_;
};
} // ns cut_impl

template <class T, class... Data>
using range_cut = basic_cut<cut_impl::range_condition<T, Data...>, Data...>;

} // ns analyzer

#endif
