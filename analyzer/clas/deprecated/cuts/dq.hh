#ifndef EG6_CUTS_DQ_LOADED
#define EG6_CUTS_DQ_LOADED

#include <string>
#include <cmath>
#include <memory>

#include "util/histogrammer.hh"
#include "util/cut.hh"

#include "eg6/h10_reader.hh"
#include "eg6/specs.hh"

namespace eg6 {
namespace cuts {
// The cut constructors take an additional output file as argument. If this is
// specified, a set of diagnostic histograms, as defined in the constructor,
// will be written to that particular file.

// =============================================================================
// good_track: do some quick sanity checks to quickly weed out
//                 problem tracks
// =============================================================================
class good_track
    : public cut<good_track, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<good_track, h10_reader&, const unsigned>;

  good_track(int qq, const std::string& path = "",
             const std::string& context = "", std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "good_track"),
                    make_title(context, "Good Track Cut")}
      , qq_{qq} {
    if (hfile) {
      // nothing here
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // integrity check and charge
    if (r.evnt.stat[iEVNT] <= 0 || qq_ * r.evnt.q[iEVNT] <= 0 ||
        r.evnt.p[iEVNT] > MAX_MOM) {
      return false;
    }
    // index to the other banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    const int iCC = r.evnt.cc[iEVNT] - 1;
    const int iDC = r.evnt.dc[iEVNT] - 1;
    const int iSC = r.evnt.sc[iEVNT] - 1;
    if (iEC < 0 || iCC < 0 || iDC < 0 || iSC < 0 || iEC > *r.ec.ec_part ||
        iCC > *r.cc.cc_part || iDC > *r.dc.dc_part || iSC > *r.sc.sc_part ||
        r.dc.dc_stat[iDC] <= 0) {
      return false;
    }
    // all OK!
    return true;
  }

private:
  constexpr static const double MAX_MOM{6.064};
  const int qq_;
};

// =============================================================================
// good_ec: select good particles of the correct charge that
//          left a hit in the EC, but not in the DC
// =============================================================================
class good_ec : public cut<good_ec, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<good_ec, h10_reader&, const unsigned>;

  good_ec(int qq, const std::string& path, const std::string& context,
          std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "good_ec_part"),
                    make_title(context, "Good EC Cut")}
      , qq_{qq} {
    if (hfile) {
      // nothing here
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // integrity and charge check
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] != qq_) {
      return false;
    }
    // index to EC and DC banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    const int iDC = r.evnt.dc[iEVNT] - 1;
    // has matching EC track
    if (iEC < 0 || iEC >=*r.ec.ec_part) {
      return false;
    // has NO matching DC track
    } else if (iDC >= 0) {
      return false;
    }
    // all OK
    return true;
  }

private:
  const int qq_;
};
// =============================================================================
// good_ic: only allow IC when the IC was functioning correctly
//          (reject hot IC)
// =============================================================================
constexpr const std::array<int, 9 * 2> GOOD_IC_BADPIX{
    0, 0, 3, 4, -8, -2, -4, -6, -2, -6, -1, -8, 3, -10, -5, 8, -9, -6};

class good_ic
    : public cut<good_ic, h10_reader&, const unsigned /* iIC */> {
public:
  using parent_type = cut<good_ic, h10_reader&, const unsigned>;

  good_ic(const std::string& path, const std::string& context,
          std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "good_ic"),
                    make_title(context, "Good IC Cut")} {
    if (hfile) {
      // nothing here
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iIC) {
    unsigned iICHB = index::ichb(r, iIC);
    const int xpix{static_cast<int>(round(r.ichb.ich_xgl[iICHB] / DX))};
    const int ypix{static_cast<int>(round(r.ichb.ich_ygl[iICHB] / DY))};
    for (unsigned ii = 0; ii < GOOD_IC_BADPIX.size() / 2; ii++) {
      if (xpix == GOOD_IC_BADPIX[ii * 2] &&
          ypix == GOOD_IC_BADPIX[ii * 2 + 1]) {
        return false;
      }
    }
    return true;
  }

private:
  constexpr static const double DY{1.346}; // cm
  constexpr static const double DX{1.360}; // cm
};


} // ns cuts
} // ns eg6

#endif
