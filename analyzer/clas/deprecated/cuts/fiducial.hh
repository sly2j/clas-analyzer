#ifndef EG6_CUTS_FIDUCIAL_LOADED
#define EG6_CUTS_FIDUCIAL_LOADED

#include <array>
#include <cmath>
#include <memory>

#include "util/histogrammer.hh"
#include "util/cut.hh"
#include "util/constants.hh"

#include "eg6/h10_reader.hh"
#include "eg6/specs.hh"

#include <TVector3.h>
#include <TCutG.h>

namespace eg6 {
namespace cuts {
// The cut constructors take an additional output file as argument. If this is
// specified, a set of diagnostic histograms, as defined in the constructor,
// will be written to that particular file.

// =============================================================================
// clas_vertex
// =============================================================================
class vertex : public cut<vertex, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<vertex, h10_reader&, const unsigned>;

  vertex(min_max_pair vz_cut, const std::string& path = "",
         const std::string& context = "",
         std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "vertex"),
                    make_title(context, "Vertex Cut")}
      , vz_cut_{vz_cut} {
    if (hfile) {
      // z-vertex position
      add_histo(hfile, "vz", "z-vertex",
                {"z [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.vz[iEVNT];
                }, 78, -99., -21.});
    }
  }
  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // vertex check (this is done without the vertex correction? // TODO
    if (r.evnt.vz[iEVNT] < vz_cut_.min || r.evnt.vz[iEVNT] > vz_cut_.max) {
      return false;
    }
    return true;
  }

private:
  const min_max_pair vz_cut_;
};
// =============================================================================
// ec cut
// based on SpecsFID::EC()
// =============================================================================
class ec : public cut<ec, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<ec, h10_reader&, const unsigned>;

  ec(min_max_pair u_cut, min_max_pair v_cut, min_max_pair w_cut,
     const std::string& path = "", const std::string& context = "",
     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ec"), make_title(context, "EC UVW Cut")}
      , u_cut_{u_cut}
      , v_cut_{v_cut}
      , w_cut_{w_cut} {
    if (hfile) {
      // XY position at the EC
      add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ech_x[index::ec(r, iEVNT)];
                 },
                 200, -400., 400.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.ec.ech_y[index::ec(r, iEVNT)];
                }, 200, -400., 400.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const unsigned iEC = index::ec(r, iEVNT);
    const TVector3 uvw{
        ec_xyz2uvw({r.ec.ech_x[iEC], r.ec.ech_y[iEC], r.ec.ech_z[iEC]})};
    if (uvw.X() < u_cut_.min || uvw.X() > u_cut_.max || uvw.Y() < v_cut_.min ||
        uvw.Y() > v_cut_.max || uvw.Z() < w_cut_.min || uvw.Z() > w_cut_.max) {
      return false;
    }
    // All OK!
    return true;
  }

private:
  // calculate the U/V/W coordinates at the EC surface
  // copy-paste from Nathan's SpecsFID::ECxyz2uvw, needs to be factorized TODO
  TVector3 ec_xyz2uvw(const TVector3& xyz) {
    constexpr static const double zoffset = 510.32;
    constexpr static const double ec_the = 0.43633230;
    constexpr static const double ylow = -182.97400000;
    constexpr static const double yhi = 189.95600000;
    constexpr static const double tgrho = 1.95325000; // 1.097620829
    constexpr static const double sinrho = 0.89012560;
    constexpr static const double cosrho = 0.45571500;
    static const double sinthe = sin(ec_the);
    static const double costhe = cos(ec_the);

    double clas_phi = xyz.Phi() * 180 / constants::pi;
    if (clas_phi < 0.)
      clas_phi += 360;
    clas_phi += 30;
    if (clas_phi >= 360.)
      clas_phi -= 360;
    const double sector_phi = (int)(clas_phi / 60.) * (constants::pi / 3);

    double rot[3][3];
    rot[0][0] = costhe * cos(sector_phi);
    rot[0][1] = -sin(sector_phi);
    rot[0][2] = sinthe * cos(sector_phi);
    rot[1][0] = costhe * sin(sector_phi);
    rot[1][1] = cos(sector_phi);
    rot[1][2] = sinthe * sin(sector_phi);
    rot[2][0] = -sinthe;
    rot[2][1] = 0.;
    rot[2][2] = costhe;

    double xyzi[3];
    xyzi[1] = xyz.X() * rot[0][0] + xyz.Y() * rot[1][0] + xyz.Z() * rot[2][0];
    xyzi[0] = xyz.X() * rot[0][1] + xyz.Y() * rot[1][1] + xyz.Z() * rot[2][1];
    xyzi[2] = xyz.X() * rot[0][2] + xyz.Y() * rot[1][2] + xyz.Z() * rot[2][2] -
              zoffset;

    TVector3 uvw;
    uvw.SetX((xyzi[1] - ylow) / sinrho);
    uvw.SetY((yhi - ylow) / tgrho - xyzi[0] + (yhi - xyzi[1]) / tgrho);
    uvw.SetZ(((yhi - ylow) / tgrho + xyzi[0] + (yhi - xyzi[1]) / tgrho) / 2. /
             cosrho);

    return uvw;
  }
  const min_max_pair u_cut_;
  const min_max_pair v_cut_;
  const min_max_pair w_cut_;
};
// =============================================================================
// ic_shadow, cuts out the IC shadow
// based on SpecsFID::MohammadIC()
//
// requires: track with evnt and dc entry
// =============================================================================
//
// this defines the IC shape (at the back of the IC?)
// outside of class definition because to avoid issues with older compilers
// that (incorrectly) prevent the constructor from accessing static constexpr
// members.
constexpr static const int IC_CUT_NN = 11;
constexpr static const std::array<double, IC_CUT_NN> IC_CUT_XPOS{
    -11.15, -11.15, -23.1, -23.1, -10.3, 9.91, 23.73, 23.73, 12.3, 12.3, -11.5};
constexpr static const std::array<double, IC_CUT_NN> IC_CUT_YPOS{
    -26.07, -23.1, -12.85, 11.5,   22.95, 22.95,
    13.1,   -12.4, -22.36, -26.07, -26.07};

class ic_shadow
    : public cut<ic_shadow, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<ic_shadow, h10_reader&, const unsigned>;

  ic_shadow(const std::string& path = "", const std::string& context = "",
            std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ic_shadow"),
                    make_title(context, "IC Shadow Cut")}
                    //WARNING TCutG will act very strangely if handed a weirdly
                    //formatted name (or duplicate name)
      , geocut_{context.c_str(), IC_CUT_NN, &IC_CUT_XPOS[0],
                &IC_CUT_YPOS[0]} {
    if (hfile) {
      // XY position at the DC projected to the IC plane
      add_histo(
          hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
          {"x [cm]",
           [](h10_reader& r, const unsigned iEVNT) {
             const unsigned iDC = index::dc(r, iEVNT);
             return geo::project_dc1_to_ic(
                        {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                        {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                 .X();
           },
           200, -50., 50.},
          {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iDC = index::dc(r, iEVNT);
            return geo::project_dc1_to_ic(
                       {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                       {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                .Y();
          }, 200, -50., 50.});
      // XY position at the DC without projections
      add_histo(hfile, "dc_y_vs_x", "DC y vs x",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   const unsigned iDC = index::dc(r, iEVNT);
                   return r.dc.tl1_x[iDC];
                 },
                 200, -60., 60.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  const unsigned iDC = index::dc(r, iEVNT);
                  return r.dc.tl1_y[iDC];
                }, 200, -60., 60.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // project the DC position onto the IC
    const unsigned iDC = index::dc(r, iEVNT);
    const TVector3 icpos{geo::project_dc1_to_ic(
        {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
        {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})};
    return !geocut_.IsInside(icpos.X(), icpos.Y());
  }

private:
  const TCutG geocut_;
};

// =============================================================================
// dc
// based on SpecsFID::FxInSector
// =============================================================================
class dc : public cut<dc, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<dc, h10_reader&, const unsigned>;

  dc(const std::string& path = "", const std::string& context = "",
     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "dc"),
                    make_title(context, "DC Fiducial Cut")} {
    if (hfile) {
      // XY position at the DC projected to the IC plane
      add_histo(
          hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
          {"x [cm]",
           [](h10_reader& r, const unsigned iEVNT) {
             const unsigned iDC = index::dc(r, iEVNT);
             return geo::project_dc1_to_ic(
                        {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                        {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                 .X();
           },
           200, -50., 50.},
          {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iDC = index::dc(r, iEVNT);
            return geo::project_dc1_to_ic(
                       {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                       {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                .Y();
          }, 200, -50., 50.});
      // XY position at the DC without projections
      add_histo(hfile, "dc_y_vs_x", "DC y vs x",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   const unsigned iDC = index::dc(r, iEVNT);
                   return r.dc.tl1_x[iDC];
                 },
                 200, -60., 60.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  const unsigned iDC = index::dc(r, iEVNT);
                  return r.dc.tl1_y[iDC];
                }, 200, -60., 60.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // project the DC position onto the IC
    const unsigned iDC = index::dc(r, iEVNT);
    double xx = r.dc.tl1_x[iDC];
    double yy = r.dc.tl1_y[iDC];
    const int sector = geo::sector(xx, yy);
    if ((sector + 1) % 6 < 3) {
      if (yy < xx * tan(constants::pi * (sector / 3. - 1. / 9.))) {
        return false;
      }
      if (yy > xx * tan(constants::pi * (sector / 3. + 1. / 9.))) {
        return false;
      }
    } else {
      if (yy > xx * tan(constants::pi * (sector / 3. - 1. / 9.))) {
        return false;
      }
      if (yy < xx * tan(constants::pi * (sector / 3. + 1. / 9.))) {
        return false;
      }
    }
    // ALL OK!
    return true;
  }

private:
  // nothing here
};
// =============================================================================
// rtpc_endplate
// check if track clears the downstream RTPC endplate
// =============================================================================
class rtpc_endplate
    : public cut<rtpc_endplate, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<rtpc_endplate, h10_reader&, const unsigned>;

  rtpc_endplate(const std::string& path = "", const std::string& context = "",
                std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "rtpc_endplate"),
                    make_title(context, "RTPC Endplate Cut")} {
    if (hfile) {
      // phi vs theta 
      add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
                {"#theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
                 },
                 100, 0., 50.},
                {"#phi [deg.]", [](h10_reader& r, const unsigned iEVNT) {
                  double phi = atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]);
                  return geo::phi_in_sector_deg(phi, geo::sector(phi));
                }, 100, -40., 40.});
      // z-vertex position
      add_histo(hfile, "vz", "z-vertex",
                {"z [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.vz[iEVNT];
                }, 78, -99., -21.});
      // theta vs z-vertex
      add_histo(
          hfile, "theta_vs_zv", "theta vs z-vertex",
          {"z [cm]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.vz[iEVNT]; },
           78, -99, -21},
          {"#theta [deg.]", [](h10_reader& r, const unsigned iEVNT) {
            return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
          }, 100, -40., 40.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const double vz{convert_to_rtpc(r.evnt.vz[iEVNT])};
    return r.evnt.cz[iEVNT] > cos(atan2(RADIUS, 100 - vz));
  }

private:
  double convert_to_rtpc(double zz) {
    return 10 * zz + 640;
  }
  constexpr static const double RADIUS{60.09};
};

// =============================================================================
// cc
// based on Michael's CCfid
//
// requires: track with evnt and cc entries
// =============================================================================
// parameterization for the theta(phi) cut
// outside of the class definition to play nice with older compilers
constexpr const std::array<double, 3> CC_PAR_THETA_MIN{{11.0, 0.03, 0.00005}};
constexpr const std::array<double, 2> CC_PAR_THETA_MAX{{44., 0.005}};
class cc : public cut<cc, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<cc, h10_reader&, const unsigned>;

  cc(const std::string& path = "", const std::string& context = "",
     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "cc"),
                    make_title(context, "CC Fiducial Cut")} {
    if (hfile) {
      // phi vs theta at the CC
      add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
                {"#theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
                 },
                 200, 0., 50.},
                {"#phi [deg.]", [](h10_reader& r, const unsigned iEVNT) {
                  double phi = atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]);
                  return geo::phi_in_sector_deg(phi, geo::sector(phi));
                }, 200, -40., 40.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // get the track theta and phi, and the sector
    double theta = acos(r.evnt.cz[iEVNT]);
    double phi = atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]);
    const int sector = geo::sector(phi);
    // compare the sector to the CC sector
    const unsigned iCC = index::cc(r, iEVNT);
    if ((sector + 1) - r.cc.cc_sect[iCC]) {
      return false;
    }
    // get phi, theta (in degrees) in the CC virtual plane
    phi = geo::phi_in_sector_deg(phi, sector);
    theta *= 180. / constants::pi;
    // check if theta makes the cut
    return (theta > theta_min(phi) && theta < theta_max(phi));
  }

private:
  double theta_min(const double phi) {
    double theta{0};
    double phi_n{1};
    for (const auto& par : CC_PAR_THETA_MIN) {
      theta += par * phi_n;
      phi_n *= phi * phi;
    }
    return theta;
  }
  double theta_max(const double phi) {
    double theta{0};
    double phi_n{1};
    for (const auto& par : CC_PAR_THETA_MAX) {
      theta += par * phi_n;
      phi_n *= phi * phi;
    }
    return theta;
  }
};
// =============================================================================
// ic cut  ## TODO cleanup
// =============================================================================
class ic : public cut<ic, h10_reader&, const unsigned /* iIC */> {
public:
  using parent_type = cut<ic, h10_reader&, const unsigned>;

  ic(const std::string& path, const std::string& context,
     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ic"),
                    make_title(context, "IC Fiducial Cut")}
      , root2{sqrt(2)} {
    if (hfile) {
      // xy-ic position
      add_histo(hfile, "ic_y_vs_x", "IC y vs x",
                {"x [cm]", [](h10_reader& r,
                              const unsigned iIC) { return r.icpb.xc[iIC]; },
                 200, -16., 16.},
                {"y [cm]", [](h10_reader& r, const unsigned iIC) {
                  return r.icpb.yc[iIC];
                }, 200, -16., 16.});
    }
  }
  bool cut_impl(h10_reader& r, const unsigned iIC) {
    const double xx{r.icpb.xc[iIC]};
    const double yy{r.icpb.yc[iIC]};
    // copy-paste from Nathan (SpecsFID::FxIC)
    // inputs are xc,yc from ICPB
    // this is to reject gammas near the inner/outer edges of the IC
    // INNER:
    if (fabs(xx) / dx <= nin && fabs(yy) / dy <= nin &&
        fabs(xx / dx - yy / dy) <= nin * root2 &&
        fabs(xx / dx + yy / dy) <= nin * root2)
      return false;

    // OUTER:
    if (fabs(xx) / dx >= nout || fabs(yy) / dy >= nout ||
        fabs(xx / dx - yy / dy) >= nout * root2 ||
        fabs(xx / dx + yy / dy) >= nout * root2)
      return false;

    return true;
  }

private:
  constexpr static const double dx = 1.346; // cm
  constexpr static const double dy = 1.360; // cm
  constexpr static const double nin = 3.25;
  constexpr static const double nout = 10.75;
  const double root2;
};

// =============================================================================
// electron_fiducial
// applies:
//  * vertex
//  * track_ec
//  * ic_shadow
//  * dc
//  * cc
// =============================================================================
class electron_fiducial
    : public cut<electron_fiducial, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<electron_fiducial, h10_reader&, const unsigned>;

  electron_fiducial(min_max_pair vz_cut, min_max_pair u_cut, min_max_pair v_cut,
                    min_max_pair w_cut, const std::string& path = "",
                    const std::string& context = "",
                    std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "electron_fiducial"),
                    make_title(context, "Electron Fiducial Cuts")}
      , vertex_cut_{vz_cut, histo_path(), "(electrons)", hfile}
      , ec_cut_{u_cut, v_cut, w_cut, histo_path(), "(electrons)", hfile}
      , ic_cut_{histo_path(), "(electrons)", hfile}
      , dc_cut_{histo_path(), "(electrons)", hfile}
      , cc_cut_{histo_path(), "(electrons)", hfile} {
    // diagnosics before and after ALL electron fiducial cuts
    if (hfile) {
      // z-vertex position
      add_histo(hfile, "vz", "Electron z-vertex",
                {"z [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.vz[iEVNT];
                }, 78, -99., -21.});
      // XY position at the EC
      add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ech_x[index::ec(r, iEVNT)];
                 },
                 200, -400., 400.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.ec.ech_y[index::ec(r, iEVNT)];
                }, 200, -400., 400.});
      // XY position at the DC projected tot the IC
      add_histo(
          hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
          {"x [cm]",
           [](h10_reader& r, const unsigned iEVNT) {
             const unsigned iDC = index::dc(r, iEVNT);
             return geo::project_dc1_to_ic(
                        {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                        {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                 .X();
           },
           200, -50., 50.},
          {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iDC = index::dc(r, iEVNT);
            return geo::project_dc1_to_ic(
                       {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                       {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                .Y();
          }, 200, -50., 50.});
      // XY position at the DC without projections
      add_histo(hfile, "dc_y_vs_x", "DC y vs x",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   const unsigned iDC = index::dc(r, iEVNT);
                   return r.dc.tl1_x[iDC];
                 },
                 200, -60., 60.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  const unsigned iDC = index::dc(r, iEVNT);
                  return r.dc.tl1_y[iDC];
                }, 200, -60., 60.});
      // phi vs theta at the CC
      add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
                {"#theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
                 },
                 200, 0., 50.},
                {"#phi [deg.]", [](h10_reader& r, const unsigned iEVNT) {
                  return atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]) * 180. /
                         constants::pi;
                }, 200, -180., 180.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip to the 5 actual fiducial cuts
    bool cut_status{true};
    cut_status &= vertex_cut_(r, iEVNT);
    cut_status &= ec_cut_(r, iEVNT);
    cut_status &= ic_cut_(r, iEVNT);
    cut_status &= dc_cut_(r, iEVNT);
    cut_status &= cc_cut_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::vertex vertex_cut_;
  cuts::ec ec_cut_;
  cuts::ic_shadow ic_cut_;
  cuts::dc dc_cut_;
  cuts::cc cc_cut_;
};
// =============================================================================
// proton_fiducial
// applies:
//  * ic_shadow
//  * dc
//  * rtpc_endplate
// =============================================================================
class proton_fiducial
    : public cut<proton_fiducial, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<proton_fiducial, h10_reader&, const unsigned>;

  proton_fiducial(const std::string& path = "", const std::string& context = "",
                  std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "proton_fiducial"),
                    make_title(context, "Proton Fiducial Cuts")}
      , ic_cut_{histo_path(), "(protons)", hfile}
      , dc_cut_{histo_path(), "(protons)", hfile}
      , rtpc_endplate_cut_{histo_path(), "(protons)", hfile} {
    // diagnosics before and after ALL proton fiducial cuts
    if (hfile) {
      // z-vertex position
      add_histo(hfile, "vz", "z-vertex",
                {"z [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.vz[iEVNT];
                }, 78, -99., -21.});
      // XY position at the DC projected tot the IC
      add_histo(
          hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
          {"x [cm]",
           [](h10_reader& r, const unsigned iEVNT) {
             const unsigned iDC = index::dc(r, iEVNT);
             return geo::project_dc1_to_ic(
                        {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                        {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                 .X();
           },
           100, -50., 50.},
          {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iDC = index::dc(r, iEVNT);
            return geo::project_dc1_to_ic(
                       {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                       {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
                .Y();
          }, 100, -50., 50.});
      // XY position at the DC without projections
      add_histo(hfile, "dc_y_vs_x", "DC y vs x",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   const unsigned iDC = index::dc(r, iEVNT);
                   return r.dc.tl1_x[iDC];
                 },
                 100, -60., 60.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  const unsigned iDC = index::dc(r, iEVNT);
                  return r.dc.tl1_y[iDC];
                }, 100, -60., 60.});
      // phi vs theta
      add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
                {"#theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
                 },
                 100, 0., 50.},
                {"#phi [deg.]", [](h10_reader& r, const unsigned iEVNT) {
                  return atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]) * 180. /
                         constants::pi;
                }, 100, -180., 180.});
      // theta vs z-vertex
      add_histo(
          hfile, "theta_vs_zv", "theta vs z-vertex",
          {"z [cm]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.vz[iEVNT]; },
           78, -99, -21},
          {"#theta [deg.]", [](h10_reader& r, const unsigned iEVNT) {
            return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
          }, 100, -40., 40.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip to the 5 actual fiducial cuts
    bool cut_status{true};
    cut_status &= ic_cut_(r, iEVNT);
    cut_status &= dc_cut_(r, iEVNT);
    cut_status &= rtpc_endplate_cut_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::ic_shadow ic_cut_;
  cuts::dc dc_cut_;
  cuts::rtpc_endplate rtpc_endplate_cut_;
};
// =============================================================================
// neutral_fiducial
// =============================================================================
class photon_ec_fiducial
    : public cut<photon_ec_fiducial, h10_reader&, const unsigned /* iEVNT */,
                 const double /* zvertex */> {
public:
  using parent_type =
      cut<photon_ec_fiducial, h10_reader&, const unsigned, const double>;

  photon_ec_fiducial(min_max_pair u_cut, min_max_pair v_cut, min_max_pair w_cut,
                     const std::string& path = "",
                     const std::string& context = "",
                     std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "photon_ec_fiducial"),
                    make_title(context, "EC Photon Fiducial Cuts")}
      , ec_cut_{u_cut, v_cut, w_cut, histo_path(), "(EC photons)", hfile} {
    // diagnosics before and after ALL photon_ec fiducial cuts
    if (hfile) {
      // XY position at the EC
      add_histo(
          hfile, "ec_y_vs_x", "EC y vs x position",
          {"x [cm]",
           [](h10_reader& r, const unsigned iEVNT, const double) {
             return r.ec.ech_x[index::ec(r, iEVNT)];
           },
           200, -400., 400.},
          {"y [cm]", [](h10_reader& r, const unsigned iEVNT, const double) {
            return r.ec.ech_y[index::ec(r, iEVNT)];
          }, 200, -400., 400.});
      add_histo(hfile, "ec_phi_vs_theta", "EC #phi vs #theta",
                {"#theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT, const double zvertex) {
                   const unsigned iEC{index::ec(r, iEVNT)};
                   const TVector3 dir{r.ec.ech_x[iEC], r.ec.ech_y[iEC],
                                      r.ec.ech_z[iEC] - zvertex};
                   return acos(dir.Z() / dir.Mag()) / constants::pi * 180.;
                 },
                 200, 0., 70.},
                {"theta [deg.]",
                 [](h10_reader& r, const unsigned iEVNT, const double) {
                   const unsigned iEC{index::ec(r, iEVNT)};
                   return atan2(r.ec.ech_y[iEC], r.ec.ech_x[iEC]) /
                          constants::pi * 180.;
                 },
                 200, -180, 180.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT, const double) {
    // assume a minimal good photon, skip to the actual fiducial cuts
    bool cut_status{true};
    cut_status &= ec_cut_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::ec ec_cut_;
};
class photon_ic_fiducial
    : public cut<photon_ic_fiducial, h10_reader&, const unsigned /* iIC */,
                 const double /* zvertex */> {
public:
  using parent_type =
      cut<photon_ic_fiducial, h10_reader&, const unsigned, const double>;

  photon_ic_fiducial(const std::string& path, const std::string& context,
                     std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "photon_ic_fiducial"),
                    make_title(context, "IC Photon Fiducial Cuts")}
      , ic_cut_{histo_path(), "(IC photons)", hfile} {
    // diagnosics before and after ALL photon_ic fiducial cuts
    if (hfile) {
      // xy-ic position
      add_histo(hfile, "ic_y_vs_x", "IC y vs x",
                {"x [cm]", [](h10_reader& r, const unsigned iIC,
                              const double) { return r.icpb.xc[iIC]; },
                 200, -16., 16.},
                {"y [cm]", [](h10_reader& r, const unsigned iIC, const double) {
                  return r.icpb.yc[iIC];
                }, 200, -16., 16.});
      add_histo(
          hfile, "ic_phi_vs_theta", "IC #phi vs #theta",
          {"#theta [deg.]",
           [](h10_reader& r, const unsigned iIC, const double zvertex) {
             TVector3 dir{r.icpb.xc[iIC], r.icpb.yc[iIC], 0. - zvertex};
             dir = dir.Unit();
             return acos(dir.Z()) / constants::pi * 180.;
           },
           200, 0., 70.},
          {"theta [deg.]", [](h10_reader& r, const unsigned iIC, const double) {
            return atan2(r.icpb.yc[iIC], r.icpb.xc[iIC]) / constants::pi * 180.;
          }, 200, -180, 180.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iIC, const double) {
    // assume a minimal good photon, skip to the actual fiducial cuts
    bool cut_status{true};
    cut_status &= ic_cut_(r, iIC);
    // done!
    return cut_status;
  }

private:
  cuts::ic ic_cut_;
};

} // ns cuts
} // eg6

#endif
