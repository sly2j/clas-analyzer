#ifndef EG6_CUTS_DVCS_LOADED
#define EG6_CUTS_DVCS_LOADED

#include <string>
#include <cmath>
#include <memory>

#include <util/histogrammer.hh>
#include <util/cut.hh>

#include <eg6/dvcs.hh>

// TODO look into dz cut

namespace eg6 {

namespace cuts {

// TODO make file histodef.hh
template <class Cut>
void add_dvcs_histos(Cut& c, const std::shared_ptr<TFile> hfile) {
  if (!hfile) {
    return;
  }
  // 1D histos
  c.add_histo(hfile, "E_photon", "E_{#gamma}",
              {"E_{#gamma} [GeV]", [](const dvcs_event e) {
                return e.rphoton.p.E();
              }, 100, 0., 5.});
  c.add_histo(hfile, "pX_epg.x", "ep#gamma Missing P_{x}",
              {"P [GeV/c]", [](const dvcs_event e) {
                return e.excl.pX_egr.X();
              }, 100, -1.5, 1.5});
  c.add_histo(hfile, "pX_epg.y", "ep#gamma Missing P_{y}",
              {"P [GeV/c]", [](const dvcs_event e) {
                return e.excl.pX_egr.Y();
              }, 100, -1.5, 1.5});
  c.add_histo(hfile, "pX_egr.M2", "ep#gamma Missing M^{2}",
              {"M^{2} [GeV^{2}/c^{4}]", [](const dvcs_event e) {
                return e.excl.pX_egr.M2();
              }, 100, -2, 4});
  c.add_histo(hfile, "pX_eg.M2", "e#gamma Missing M^{2}",
              {"M^{2} [GeV^{2}/c^{4}]", [](const dvcs_event e) {
                return e.excl.pX_eg.M2();
              }, 100, -2, 4});
  c.add_histo(hfile, "pX_er.M2", "ep Missing M^{2}",
              {"M^{2} [GeV^{2}/c^{4}]", [](const dvcs_event e) {
                return e.excl.pX_er.M2();
              }, 100, -2, 4});
  c.add_histo(hfile, "pX_epg.E", "ep#gamma Missing E",
              {"E [GeV]", [](const dvcs_event e) { return e.excl.pX_egr.E(); },
               100, -2, 4});
  c.add_histo(hfile, "t_c", "t_{c}",
              {"t_{c} [GeV^{2}]", [](const dvcs_event e) { return e.excl.t_c; },
               100, -5, 5});
  c.add_histo(hfile, "phi", "#phi", {"#phi [deg.]", [](const dvcs_event e) {
                return e.excl.phi * 180. / constants::pi;
              }, 100, 0., 360.});
  c.add_histo(hfile, "coplanarity", "#gamma#gamma^{*}p coplanarity angle",
              {"#phi [deg.]", [](const dvcs_event e) {
                return e.excl.coplanarity * 180. / constants::pi;
              }, 100, 0., 360.});
  // 2D histos
  c.add_histo(hfile, "Q2_vs_x", "Q^{2} vs x",
              {"x", [](const dvcs_event e) { return e.incl.x; }, 100, 0., .8},
              {"Q^{2} [GeV^{2}]", [](const dvcs_event e) { return e.incl.Q2; },
               100, 0., 5});
  c.add_histo(hfile, "pX_epg.M2_vs_x", "ep#gamma Missing M^{2} vs x",
              {"x", [](const dvcs_event e) { return e.incl.x; }, 100, 0., 1.},
              {"M^{2} [GeV^{2}/c^{4}]",
               [](const dvcs_event e) { return e.excl.pX_egr.M2(); }, 100, -2,
               4});
  c.add_histo(
      hfile, "pX_epg.y_vs_x", "ep#gamma Missing P_{y} vs P_{x}",
      {"P_{x} [GeV/c]", [](const dvcs_event e) { return e.excl.pX_egr.X(); },
       100, -1.5, 1.5},
      {"P_{y} [GeV/c]", [](const dvcs_event e) { return e.excl.pX_egr.Y(); },
       100, -1.5, 1.5});
  c.add_histo(hfile, "pX_epg.M2_vs_t_c", "ep#gamma Missing M^{2} vs t_{c}",
              {"t_{c} [GeV^{2}]", [](const dvcs_event e) { return e.excl.t_c; },
               100, -5, 5},
              {"M^{2} [GeV^{2}/c^{4}]",
               [](const dvcs_event e) { return e.excl.pX_egr.M2(); }, 100, -2,
               4});
}

// =============================================================================
// dvcs_hard: select hard dvcs candidate events
// =============================================================================
class dvcs_hard : public cut<dvcs_hard, const dvcs_event> {
public:
  using parent_type = cut<dvcs_hard, const dvcs_event>;

  dvcs_hard(min_max_pair W_cut, min_max_pair y_cut, min_max_pair Q2_cut,
            min_max_pair E_photon_cut, const std::string& path,
            const std::string context, std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "dvcs_hard"),
                    make_path(context, "DVCS Hard Scattering Cut")}
      , W_cut_{W_cut}
      , y_cut_{y_cut}
      , Q2_cut_{Q2_cut}
      , E_photon_cut_{E_photon_cut} {
    add_dvcs_histos(*this, hfile);
  }
  bool cut_impl(const dvcs_event e) {
    return (e.incl.W > W_cut_.min && e.incl.W < W_cut_.max &&
            e.incl.y > y_cut_.min && e.incl.y < y_cut_.max &&
            e.incl.Q2 > Q2_cut_.min && e.incl.Q2 < Q2_cut_.max &&
            e.rphoton.p.E() > E_photon_cut_.min &&
            e.rphoton.p.E() < E_photon_cut_.max);
  }
private:
  const min_max_pair W_cut_;
  const min_max_pair y_cut_;
  const min_max_pair Q2_cut_;
  const min_max_pair E_photon_cut_;
};
// =============================================================================
// dvcs_exclusivity: DVCS exclusivity cuts
// =============================================================================
class dvcs_exclusivity : public cut<dvcs_exclusivity, const dvcs_event> {
public:
  using parent_type = cut<dvcs_exclusivity, const dvcs_event>;

  // TODO not hardcoded
  dvcs_exclusivity(const std::string& path, const std::string context,
                   std::shared_ptr<TFile> hfile = nullptr)
    : parent_type{make_path(path, "dvcs_exclusivity"),
                  make_path(context, "DVCS Exclusivity Cut")} {
    add_dvcs_histos(*this, hfile);
  }

  // TODO epgamma_PT_mis < 0.25
  bool cut_impl(const dvcs_event e) {
    if (fabs(e.excl.pX_egr.X()) < 3 * 0.103 &&
        fabs(e.excl.pX_egr.Y()) < 3 * 0.103 &&
        fabs((e.excl.pX_egr.M2() + 0.011)) < 3 * 0.026 &&
        fabs((e.excl.pX_egr.M2() + 0.011)) < 3 * 0.026 &&
        fabs(e.excl.coplanarity * 180. / constants::pi - 0.154) < 3 * 2.67 &&
        fabs(e.excl.M2X_er - 0.351) < 3 * 0.507 &&
        fabs(e.excl.M2X_eg - 1.143) < 3 * 0.36 &&
        fabs(e.excl.theta_gg) < 4. * constants::pi / 180.) {
      return true;
    }
    return false;
  }
};

// =============================================================================
// dvcs: DVCS cuts
// =============================================================================
class dvcs: public cut<dvcs, const dvcs_event> {
public:
  using parent_type = cut<dvcs, const dvcs_event>;

  // TODO not hardcoded
  dvcs(const std::string& path, const std::string context,
       std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "dvcs_exclusivity"),
                    make_path(context, "DVCS Exclusivity Cut")}
      , dvcs_hard_{{2., 100.},
                   {0, 0.85},
                   {1, 100},
                   {2, 100},
                   histo_path(),
                   "(DVCS)",
                   hfile}
      , dvcs_exclusivity_{histo_path(), "(DVCS)", hfile} {
    if (hfile) {
      add_dvcs_histos(*this, hfile);
    }
  }

  bool cut_impl(const dvcs_event e) {
    bool cut_status{true};
    cut_status &= dvcs_hard_(e);
    cut_status &= dvcs_exclusivity_(e);
    return cut_status;
  }
private:
  dvcs_hard dvcs_hard_;
  dvcs_exclusivity dvcs_exclusivity_;
};
} // ns cuts
} // ns eg6

#endif
