#ifndef EG6_CUTS_FIDUCIAL_LOADED
#define EG6_CUTS_FIDUCIAL_LOADED

#include <cmath>
#include <memory>

#include "util/histogrammer.hh"
#include "util/cut.hh"
#include "util/contants.hh"

#include "eg6/h10_reader.hh"

#include <TMath.h>
#include <TVector3.h>
#include <TCutG.h>

namespace eg6 {
// The cut constructors take an additional output file as argument. If this is
// specified, a set of diagnostic histograms, as defined in the constructor,
// will be written to that particular file.

// =============================================================================
// clas_vertex_cut
// =============================================================================
class vertex_cut : public cut<vertex_cut, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<vertex_cut, h10_reader&, const unsigned>;
  using min_max_pair = std::pair<double, double>;

  vertex_cut(min_max_pair vz_cut, const std::string& name = "",
             std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_name("vertex_cut", name),
                    make_title("Vertex Cut ", name)}
      , vz_cut_{vz_cut}
      , max_{max} {
    if (hfile) {
      // z-vertex position
      add_histo(hfile, "e_vz", "Electron z-vertex",
                {"z [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.vz[iEVNT];
                }, 78, -99., -21.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // vertex check (this is done without the vertex correction? // TODO
    if (r.evnt.vz[iEVNT] < vz_cut_.first || r.evnt.vz[iEVNT] > vz_cut_.second) {
      return false;
    }
    return true;
  }

private:
  const min_max_pair vz_cut_;
};
// =============================================================================
// track_ec_cut
// based on SpecsFID::EC()
// =============================================================================
class track_ec_cut
    : public cut<track_ec_cut, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<track_ec_cut, h10_reader&, const unsigned>;
  using min_max_pair = std::pair<double, double>;

  track_ec_cut(min_max_pair u_cut, min_max_pair v_cut, min_max_pair w_cut,
                  std::string& name = "",
                  std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_name("track_ec_cut", name),
                    make_title("EC UVW Cut", name)}
      , u_cut_{u_cut}
      , v_cut_{v_cut}
      , w_cut_{w_cut} {
    if (hfile) {
      // XY position at the EC
      add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ech_x[index::ec(r, iEVNT)];
                 },
                 100, -400., 400.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.ec.ech_y[index::ec(r, iEVNT)];
                }, 100, -400., 400.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const int iEC = index::ec(r, iEVNT);
    TVector3 uvw =
        ec_xyz2uvw({r.ec.ech_x[iEC], r.ec.ech_y[iEC], r.ec.ech_z[iEC]});
    if (uvw.X() < u_cut_.first || uvw.X() > u_cut_.second
      || uvw.Y < v_cut.first || uvw.Y() > v_cut_.second
      || uvw.Z < w_cut.first || uvw.Z() > w_cut_.second) {
      return false;
    }
    // All OK!
    return true;
  }

private:
  // calculate the U/V/W coordinates at the EC surface
  // copy-paste from Nathan's SpecsFID::ECxyz2uvw, needs to be factorized TODO
  ec_xyz2uvw(const TVector3& xyz) {
    constexpr static const double zoffset = 510.32;
    constexpr static const double ec_the = 0.43633230;
    constexpr static const double ylow = -182.97400000;
    constexpr static const double yhi = 189.95600000;
    constexpr static const double tgrho = 1.95325000; // 1.097620829
    constexpr static const double sinrho = 0.89012560;
    constexpr static const double cosrho = 0.45571500;
    static const double sinthe = sin(ec_the);
    static const double costhe = cos(ec_the);

    double clas_phi = xyz.Phi() * 180 / constants::pi;
    if (clas_phi < 0.)
      clas_phi += 360;
    clas_phi += 30;
    if (clas_phi >= 360.)
      clas_phi -= 360;
    const double sector_phi = (int)(clas_phi / 60.) * (constants::pi / 3);

    double rot[3][3];
    rot[0][0] = costhe * cos(sector_phi);
    rot[0][1] = -sin(sector_phi);
    rot[0][2] = sinthe * cos(sector_phi);
    rot[1][0] = costhe * sin(sector_phi);
    rot[1][1] = cos(sector_phi);
    rot[1][2] = sinthe * sin(sector_phi);
    rot[2][0] = -sinthe;
    rot[2][1] = 0.;
    rot[2][2] = costhe;

    double xyzi[3];
    xyzi[1] = xyz.X() * rot[0][0] + xyz.Y() * rot[1][0] + xyz.Z() * rot[2][0];
    xyzi[0] = xyz.X() * rot[0][1] + xyz.Y() * rot[1][1] + xyz.Z() * rot[2][1];
    xyzi[2] = xyz.X() * rot[0][2] + xyz.Y() * rot[1][2] + xyz.Z() * rot[2][2] -
              zoffset;

    TVector3 uvw;
    uvw.SetX((xyzi[1] - ylow) / sinrho);
    uvw.SetY((yhi - ylow) / tgrho - xyzi[0] + (yhi - xyzi[1]) / tgrho);
    uvw.SetZ(((yhi - ylow) / tgrho + xyzi[0] + (yhi - xyzi[1]) / tgrho) / 2. /
             cosrho);

    return uvw;
  }
  const std::pair<double, double> u_cut_;
  const std::pair<double, double> v_cut_;
  const std::pair<double, double> w_cut_;
};
// =============================================================================
// track_ic_cut, cuts out the IC shadow
// based on SpecsFID::MohammadIC()
// =============================================================================
class track_ic_cut
    : public cut<track_ic_cut, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<track_ic_cut, h10_reader&, const unsigned>;
  using min_max_pair = std::pair<double, double>;

  track_ic_cut(std::string& name = "", std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_name("track_ic_cut", name),
                    make_title("IC Fiducial Cut", name)}
      , geocut_{"track_ic_cut", NN-1, XPOS, YPOS} {
    if (hfile) {
      // XY position at the EC
      add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
                {"x [cm]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ech_x[index::ec(r, iEVNT)];
                 },
                 100, -400., 400.},
                {"y [cm]", [](h10_reader& r, const unsigned iEVNT) {
                  return r.ec.ech_y[index::ec(r, iEVNT)];
                }, 100, -400., 400.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // project the DC position onto the IC
    const int iDC = index::dc(r, iEVNT);
    const TVector3 r1dir{r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]};
    const TVector3 r1pos{r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]};
    const TVector3 shift{(r1pos.Z() + IC_FROM_DC) / r1dir.Z() * r1dir};
    const TVector3 icpos{r1pos - shift};

    SPECS.FID.ProjectDC1toIC(r1dir,r1pos,16.0));

    return !geocut_.IsInside(r.ic.[iIC]
    TVector3 uvw =
        ec_xyz2uvw({r.ec.ech_x[iEC], r.ec.ech_y[iEC], r.ec.ech_z[iEC]});
    if (uvw.X() < u_cut_.first || uvw.X() > u_cut_.second
      || uvw.Y < v_cut.first || uvw.Y() > v_cut_.second
      || uvw.Z < w_cut.first || uvw.Z() > w_cut_.second) {
      return false;
    }
    // All OK!
    return true;
  }

private:
  constexpr static const int NN=11;
  constexpr static const double XPOS[NN] = {-11.15, -11.15, -23.1, -23.1,
                                            -10.3,  9.91,   23.73, 23.73,
                                            12.3,   12.3,   -11.5};
  constexpr static const double YPOS[NN] = {-26.07, -23.1,  -12.85, 11.5,
                                            22.95,  22.95,  13.1,   -12.4,
                                            -22.36, -26.07, -26.07};
  constexpr static double IC_FROM_DC{-16.0}; // <== TODO this is the end of the IC
                                             //     according to a comment in
                                             //     Nathan's code?
  TCutG geocut_;
};

// =============================================================================
// track_fiducial_cut
// =============================================================================
#if 0
class track_fiducial_cut
    : cut<track_fiducial_cut, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type =
      cut<track_fiducial_cut, h10_reader&, const unsigned /*iEVNT */>;
  fiducial_cut(const std::string& name, std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_name("track_fiducial_cut", "name"),
                    make_title("Track Fiducial Cut", "name")} {}
      };
#endif
// =============================================================================
// neutral_fiducial_cut
// =============================================================================

} // eg6

#endif
