#ifndef ANALYZER_CLAS_DETECTOR_PHOTON_LOADED
#define ANALYZER_CLAS_DETECTOR_PHOTON_LOADED

#include <algorithm>
#include <cstdint>
#include <memory>
#include <vector>

#include <TParticlePDG.h>
#include <TVector3.h>
#include <TFile.h>

#include <analyzer/util/cut.hh>
#include <analyzer/physics/pdg.hh>
#include <analyzer/clas/detector/index.hh>
#include <analyzer/clas/detector/pid.hh>
#include <analyzer/clas/detector/fiducial.hh>
#include <analyzer/clas/detector/base.hh>
#include <analyzer/clas/detector/particle.hh>

namespace analyzer {
namespace clas {
namespace detector {

namespace cut {
template <class Reader> class ec_photon_dq;
template <class Reader> class ec_photon_id;
template <class Reader> class ec_photon_fiducial;
template <class Reader> class ic_photon_dq;
template <class Reader> class ic_photon_id;
template <class Reader> class ic_photon_fiducial;
} // ns cut

template <class Reader>
class ec_photon_detector : public track_base<ec_photon_detector, class Reader&,
                                             const clas_particle&> {
public:
  using reader_type = Reader
  using particle_type = clas_particle;
  using base_type = detector_base<ec_photon_detector, Reader&>;

  ec_photon_detector(shared_ptr<TFile> hfile)
      : pdg_photon_{*pdg_particle(pdg_id::gamma)}
      , dq_cut_{"detector_cuts", "", hfile}
      , pid_cut_{"detector_cuts", "", hfile}
      , fiducial_cut_{"detector_cuts", "", hfile} {}

private:
  friend class base_type;

  bool dq(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return dq_cut_(r, iEVNT);
  }
  bool pid(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return pid_cut_(r, iEVNT);
  }
  bool fiducial(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return fiducial_cut_(r, iEVNT);
  }
  particle_type particle(Reader& r, const clas_particle& electron,
                         const size_t iEVNT) {
    const unsigned iEC = index::ec(r, iEVNT);
    TVector3 dir{r.ec.ech_x[iEC], r.ec.ech_y[iEC],
                 r.ec.ech_z[iEC] - electron.vz};
    double mom = fmax(r.ex.ectot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC]) /
                 0.273;
    double dt =
        r.ec.ec_t[iEC] - r.ec.ec_r[iEC] / constants::c_cm_ns - *r.head.tr_time;
    return {electron.run,
            electron.event,
            {*pdg_[pdg_id::gamma], mom * dir.Unit()},
            electron.vz,
            dt};
  }

  const TParticlePDG pdg_photon_;
  cut::ec_photon_dq<Reader> dq_cut_;
  cut::ec_photon_id<Reader> pid_cut_;
  cut::ec_photon_fiducial<Reader> fiducial_cut_;
};
template <class Reader>
class ic_photon_detector : public track_base<ic_photon_detector, class Reader&,
                                             const clas_particle&> {
public:
  using reader_type = Reader
  using particle_type = clas_particle;
  using base_type = detector_base<ic_photon_detector, Reader&>;

  ic_photon_detector(shared_ptr<TFile> hfile)
      : pdg_photon_{*pdg_particle(pdg_id::gamma)}
      , dq_cut_{"detector_cuts", "", hfile}
      , pid_cut_{"detector_cuts", "", hfile}
      , fiducial_cut_{"detector_cuts", "", hfile} {}

private:
  friend class base_type;

  bool dq(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return dq_cut_(r, iEVNT);
  }
  bool pid(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return pid_cut_(r, iEVNT);
  }
  bool fiducial(Reader& r, const clas_particle& electron, const size_t iEVNT) {
    return fiducial_cut_(r, iEVNT);
  }
  particle_type particle(Reader& r, const clas_particle& electron,
                         const size_t iEVNT) {
    TVector3 dir{r.icpb.xc[iIC], r.icpb.yc[iIC], 0. - electron.vz};
    double dt =
        r.icpb.tc[iIC] - dir.Mag() / constants::c_cm_ns - *r.head.tr_time;

    return {electron.run,
            electron.event,
            {*pdg_[pdg_id::gamma], r.icpb.etc[iIC] * dir.Unit()},
            electron.vz,
            dt,
            true};
  }

  const TParticlePDG pdg_photon_;
  cut::ic_photon_dq<Reader> dq_cut_;
  cut::ic_photon_id<Reader> pid_cut_;
  cut::ic_photon_fiducial<Reader> fiducial_cut_;
};

namespace cut {
template <class Reader>
class ec_photon_dq
    : public cut<ec_photon_dq, Reader&, const size_t /* iEVNT */> {
public:
  using parent_type = cut<ec_photon_dq, Reader&, const size_t>;

  ec_photon_dq(const std::string& path, const std::string& context,
               std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ec_photon_dq"),
                    make_title(context, "EC Photon DQ Cut")} {
    ; // do nothing
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    // integrity check and charge
    if (r.evnt.q[iEVNT] == 0) {
      return false;
    }
    // index to the other banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    const int iDC = r.evnt.dc[iEVNT] - 1;
    // has matching EC track TODO no stat???
    if (iEC < 0 || EC > *r.ec.ec_part) {
      return false;
      // has NO matching DC track
    } else if (iDC >= 0) {
      return false;
    }
    // all OK!
    return true;
  }
};
template <class Reader>
class ec_photon_id : public cut<ec_photon_id, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<ec_photon_id, Reader&, const size_t>;

  ec_photon_id(const std::string& path, const std::string& context,
               std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ec_photon_id"),
                    make_title(context, "EC Photon PID Cuts")}
      , ec_etot_{histo_path(), "(ec_photon)", hfile}
      , beta_{histo_path(), "(ec_photon)", hfile} {
    ec_photon_id_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= ec_etot_(r, iEVNT);
    cut_status &= beta_(r, iEVNT);
    return cut_status;
  }

private:
  ec_etot<Reader> ec_etot_;
  beta<Reader> beta_;
};
template <class Reader>
class ec_photon_fiducial
    : public cut<ec_photon_fiducial, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<ec_photon_fiducial, Reader&, const size_t>;

  ec_photon_fiducial(const std::string& path, const std::string& context,
                     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ec_photon_fiducial"),
                    make_title(context, "EC Photon Fiducial Cuts")}
      , ec_cut_{histo_path(), "(ec_photon)", hfile, true} {
    ec_photon_fiducial_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= ec_cut_(r, iEVNT);
    return cut_status;
  }

private:
  ec_cut<Reader> ec_cut_;
}

// TODO hacky hacky
constexpr const std::array<int, 9 * 2> GOOD_IC_BADPIX{
    0, 0, 3, 4, -8, -2, -4, -6, -2, -6, -1, -8, 3, -10, -5, 8, -9, -6};
template <class Reader>
class ic_photon_dq
    : public cut<ic_photon_dq, Reader&, const size_t /* iIC */> {
public:
  using parent_type = cut<ic_photon_dq, Reader&, const size_t>;

  ic_photon_dq(const std::string& path, const std::string& context,
               std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ic_photon_dq"),
                    make_title(context, "IC Photon DQ Cut")} {
    ; // do nothing
  }

  bool cut_impl(Reader& r, const size_t iIC) {
    unsigned iICHB = index::ichb(r, iIC);
    const int xpix{static_cast<int>(round(r.ichb.ich_xgl[iICHB] / 1.360))}; // dx TODO double check
    const int ypix{static_cast<int>(round(r.ichb.ich_ygl[iICHB] / 1.346))}; // dy
    for (unsigned ii = 0; ii < GOOD_IC_BADPIX.size() / 2; ii++) {
      if (xpix == GOOD_IC_BADPIX[ii * 2] &&
          ypix == GOOD_IC_BADPIX[ii * 2 + 1]) {
        return false;
      }
    }
    return true;
  }
};
template <class Reader>
class ic_photon_id : public cut<ic_photon_id, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<ic_photon_id, Reader&, const size_t>;

  ic_photon_id(const std::string& path, const std::string& context,
               std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ic_photon_id"),
                    make_title(context, "IC Photon PID Cuts")}
      , ic_etc_{histo_path(), "(ic_photon)", hfile} {
    ic_photon_id_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= ic_etc_(r, iEVNT);
    return cut_status;
  }

private:
  ic_etc_cut<Reader> ic_etc_;
};
template <class Reader>
class ic_photon_fiducial
    : public cut<ic_photon_fiducial, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<ic_photon_fiducial, Reader&, const size_t>;

  ic_photon_fiducial(const std::string& path, const std::string& context,
                     std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "ic_photon_fiducial"),
                    make_title(context, "IC Photon Fiducial Cuts")}
      , ic_cut_{histo_path(), "(ic_photon)", hfile} {
    ic_photon_fiducial_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= ic_cut_(r, iEVNT);
    return cut_status;
  }

private:
  ic_cut<Reader> ic_cut_;
};

} // ns cut

} // ns detector
} // ns clas
} // ns analyzer
#endif
