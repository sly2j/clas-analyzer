#ifndef ANALYSER_CLAS_DETECTOR_FIDUCIAL_LOADED
#define ANALYSER_CLAS_DETECTOR_FIDUCIAL_LOADED

#include <cmath>
#include <cstdint>
#include <memory>

#include <analyzer/physics/constants.hh>
#include <analyzer/detector/index.hh>
#include <analyzer/detector/specs.hh>

namespace analyzer {
namespace clas {
namespace detector {
namespace cut {

template <class Cut>
void ic_photon_fiducial_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  c.add_histo(hfile, "ic_y_vs_x", "IC y vs x",
            {"x [cm]", [](typename Cut::reader_type& r, const unsigned iIC,
                          const double) { return r.icpb.xc[iIC]; },
             200, -16., 16.},
            {"y [cm]", [](typename Cut::reader_type& r, const unsigned iIC, const double) {
              return r.icpb.yc[iIC];
            }, 200, -16., 16.});
  c.add_histo(
      hfile, "ic_phi_vs_theta", "IC #phi vs #theta",
      {"#theta [deg.]",
       [](typename Cut::reader_type& r, const unsigned iIC, const double zvertex) {
         TVector3 dir{r.icpb.xc[iIC], r.icpb.yc[iIC], 0. - zvertex};
         dir = dir.Unit();
         return acos(dir.Z()) / constants::pi * 180.;
       },
       200, 0., 70.},
      {"theta [deg.]", [](typename Cut::reader_type& r, const unsigned iIC, const double) {
        return atan2(r.icpb.yc[iIC], r.icpb.xc[iIC]) / constants::pi * 180.;
      }, 200, -180, 180.});
}
template <class Cut>
void ec_photon_fiducial_histos(Cut& c, std::shared_ptr<TFile> hfile) {
template <class Cut>
void ec_photon_fiducial_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  c.add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
            {"x [cm]",
             [](typename Cut::reader_type& r, const unsigned iEVNT, const double) {
               return r.ec.ech_x[index::ec(r, iEVNT)];
             },
             200, -400., 400.},
            {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT, const double) {
              return r.ec.ech_y[index::ec(r, iEVNT)];
            }, 200, -400., 400.});
  c.add_histo(
      hfile, "ec_phi_vs_theta", "EC #phi vs #theta",
      {"#theta [deg.]",
       [](typename Cut::reader_type& r, const unsigned iEVNT, const double zvertex) {
         const unsigned iEC{index::ec(r, iEVNT)};
         const TVector3 dir{r.ec.ech_x[iEC], r.ec.ech_y[iEC],
                            r.ec.ech_z[iEC] - zvertex};
         return acos(dir.Z() / dir.Mag()) / constants::pi * 180.;
       },
       200, 0., 70.},
      {"theta [deg.]", [](typename Cut::reader_type& r, const unsigned iEVNT, const double) {
        const unsigned iEC{index::ec(r, iEVNT)};
        return atan2(r.ec.ech_y[iEC], r.ec.ech_x[iEC]) / constants::pi * 180.;
      }, 200, -180, 180.});
}
template <class Cut>
void proton_id_histos(Cut& c, std::shared_ptr<TFile> hfile) {
      add_histo(
          hfile, "delta_beta", "#Delta#beta",
          {"p [GeV]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; },
           200, 0., 3.},
          {"#Delta#beta", [=](h10_reader& r, const unsigned iEVNT) {
            return delta_beta_.calc_delta_beta(r, iEVNT);
          }, 200, -.7, .7});
}
template <class Cut>
void proton_fiducial_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  // z-vertex position
  c.add_histo(hfile, "vz", "z-vertex",
            {"z [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
              return r.evnt.vz[iEVNT];
            }, 78, -99., -21.});
  // XY position at the DC projected tot the IC
  c.add_histo(
      hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
      {"x [cm]",
       [](typename Cut::reader_type& r, const unsigned iEVNT) {
         const unsigned iDC = index::dc(r, iEVNT);
         return geo::project_dc1_to_ic(
                    {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                    {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
             .X();
       },
       100, -50., 50.},
      {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
        const unsigned iDC = index::dc(r, iEVNT);
        return geo::project_dc1_to_ic(
                   {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                   {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
            .Y();
      }, 100, -50., 50.});
  // XY position at the DC without projections
  c.add_histo(hfile, "dc_y_vs_x", "DC y vs x",
            {"x [cm]",
             [](typename Cut::reader_type& r, const unsigned iEVNT) {
               const unsigned iDC = index::dc(r, iEVNT);
               return r.dc.tl1_x[iDC];
             },
             100, -60., 60.},
            {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
              const unsigned iDC = index::dc(r, iEVNT);
              return r.dc.tl1_y[iDC];
            }, 100, -60., 60.});
  // phi vs theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
            {"#theta [deg.]",
             [](typename Cut::reader_type& r, const unsigned iEVNT) {
               return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
             },
             100, 0., 50.},
            {"#phi [deg.]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
              return atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]) * 180. /
                     constants::pi;
            }, 100, -180., 180.});
  // theta vs z-vertex
  c.add_histo(hfile, "theta_vs_zv", "theta vs z-vertex",
            {"z [cm]", [](typename Cut::reader_type& r,
                          const unsigned iEVNT) { return r.evnt.vz[iEVNT]; },
             78, -99, -21},
            {"#theta [deg.]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
              return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
            }, 100, -40., 40.});
}

template <class Cut>
void electron_id_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  // electron EC_eo vs EC_ei
  add_histo(hfile, "e_eo_vs_ei", "Electron EC_eo vs EC_ei",
            {"E_{in} [GeV]",
             [](h10_reader& r, const unsigned iEVNT) {
               return r.ec.ec_ei[index::ec(r, iEVNT)];
             },
             200, 0., 0.6},
            {"E_{out} [GeV]", [](h10_reader& r, const unsigned iEVNT) {
              return double{r.ec.ec_eo[index::ec(r, iEVNT)]};
            }, 200, 0., 0.4});
  // electron (e_tot/p vs p)
  add_histo(
      hfile, "e_etot_p_vs_p", "Electron E_{tot}/p vs p",
      {"p_{e} [GeV]",
       [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; }, 200,
       0.5, 5.},
      {"E_{tot}/p", [](h10_reader& r, const unsigned iEVNT) {
        const unsigned iEC = index::ec(r, iEVNT);
        const double p{r.evnt.p[iEVNT]};
        const double etot{
            fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC])};
        return (p > 0) ? etot / p : -1;
      }, 200, 0., 1.});
  // number of photo-electrons
  add_histo(hfile, "e_nphe", "Electron N_{phe}",
            {"#", [](h10_reader& r, const unsigned iEVNT) {
              const unsigned iCC = index::cc(r, iEVNT);
              return r.cc.nphe[iCC];
            }, 50, 0., 350.});
}
template <class Cut>
void electron_fiducial_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  // z-vertex position
  c.add_histo(hfile, "vz", "Electron z-vertex",
              {"z [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
                return r.evnt.vz[iEVNT];
              }, 78, -99., -21.});
  // XY position at the EC
  c.add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
              {"x [cm]",
               [](typename Cut::reader_type& r, const unsigned iEVNT) {
                 return r.ec.ech_x[index::ec(r, iEVNT)];
               },
               200, -400., 400.},
              {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
                return r.ec.ech_y[index::ec(r, iEVNT)];
              }, 200, -400., 400.});
  // XY position at the DC projected tot the IC
  c.add_histo(
      hfile, "dcproj_y_vs_x", "DC projection to IC y vs x",
      {"x [cm]",
       [](typename Cut::reader_type& r, const unsigned iEVNT) {
         const unsigned iDC = index::dc(r, iEVNT);
         return geo::project_dc1_to_ic(
                    {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                    {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
             .X();
       },
       200, -50., 50.},
      {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
        const unsigned iDC = index::dc(r, iEVNT);
        return geo::project_dc1_to_ic(
                   {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
                   {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})
            .Y();
      }, 200, -50., 50.});
  // XY position at the DC without projections
  c.add_histo(hfile, "dc_y_vs_x", "DC y vs x",
              {"x [cm]",
               [](typename Cut::reader_type& r, const unsigned iEVNT) {
                 const unsigned iDC = index::dc(r, iEVNT);
                 return r.dc.tl1_x[iDC];
               },
               200, -60., 60.},
              {"y [cm]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
                const unsigned iDC = index::dc(r, iEVNT);
                return r.dc.tl1_y[iDC];
              }, 200, -60., 60.});
  // phi vs theta at the CC
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](typename Cut::reader_type& r, const unsigned iEVNT) {
                 return acos(r.evnt.cz[iEVNT]) * 180. / constants::pi;
               },
               200, 0., 50.},
              {"#phi [deg.]", [](typename Cut::reader_type& r, const unsigned iEVNT) {
                return atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT]) * 180. /
                       constants::pi;
              }, 200, -180., 180.});
}

}
}
}
}

#endif
