#ifndef EG6_DETECTOR_LOADED
#define EG6_DETECTOR_LOADED

#include <algorithm>
#include <vector>

namespace eg6 {
namespace detector {

// CRTP base class for our detectors
// Child Detector classes define at least a particle type, and 
// dq(), pid() and fiducial() cut member functions, as well as a particle() member
// function that creates a new particle
template <class Detector>
class base {
public:
  using child_type = Detector;
  using particle_type = typename child_type::particle_type;

protected:
  // returns the detected particles in a vector sorted by momentum
  auto detect(h10_reader& r, const unsigned begin, const unsigned end) {
    std::vector<particle_type> particles;
    // detection loop
    for (unsigned i {begin}; i < end; ++begin) {
      if (child().dq(r, i) && child().pid(r, i) && child().fiducial(r, i)) {
        particles.push_back(particle(r, i));
      }
    }
    // sort particles by momentum
    std::sort(particles.begin, particles.end(),
              [](const auto& a, const auto& b) { return (a.mom < b.mom); });

    // return result
    return particles;
  }
  
private:
  // CRTP boilerplate
  child_type& child() { return static_cast<child_type&>(*this); }
  const child_type& child() const {
    return static_cast<const child_type&>(*this);
  }
};

// CRTP base class for charged particle detectors
template <class Detector>
class track_base : base<track_base> {
public:
  using base_type = base<track_base>;
  using child_type = Detector;
  using particle_type = typename child_type::particle_type;

  auto detect(h10_reader& r) {
    return base_type::detect(r, 0, index.nEVNT(r));
  }

private:
  // CRTP boilerplate
  friend class base_type;
  child_type& child() { return static_cast<child_type&>(*this); }
  const child_type& child() const {
    return static_cast<const child_type&>(*this);
  }
  // create a charged particle
  particle_type particle(h10_reader& r, const unsigned iEVNT) {
    const particle part{*pdg_[id], r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                        atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])};
    const double vz{corrections::vertex_CLAS(*r.head.runnb, r.evnt.vz[iEVNT],
                                             part.theta, part.phi)};
    const unsigned iSC = index::sc(r, iEVNT);
    const double dt{r.sc.sc_t[iSC] -
                    r.sc.sc_r[iSC] / (constants::c_cm_ns * part.beta) -
                    *r.head.tr_time};
    return {*r.head.runnb, *r.head.evntid, part, vz, dt};
  }
  // forward cuts to the actual detector
  bool dq(h10_reader& r, const unsigned index) { return child().dq(r, index); }
  bool pid(h10_reader& r, const unsigned index) {
    return child().pid(r, index);
  }
  bool fiducial(h10_reader& r, const unsigned index) {
    return child().fiducial(r, index);
  }
};

class electron_detector : public track_base<electron_detector> {
public:
  using base_type = track_base<electron_detector>;
  using particle_type = clas_particle;

private:
  friend class base_type;

  bool dq(h10_reader& r, const unsigned index) {
    return dq_cut_(r, index);
  }
  bool pid(h10_reader& r, const unsigned index) {
    return pid_cut_(r, index);
  }
  bool fiducial(h10_reader& r, const unsigned index) {
    return fiducial_cut_(r, index);
  }

  cuts::electron::dq dq_cut_;
  cuts::electron::id pid_cut_;
  cuts::electron::fiducial fiducial_cut_;
};
class proton_detector : public track_base<proton_detector> {
public:
  using base_type = track_base<proton_detector>;
  using particle_type = clas_particle;

private:
  friend class base_type;

  bool dq(h10_reader& r, const unsigned index) {
    return dq_cut_(r, index);
  }
  bool pid(h10_reader& r, const unsigned index) {
    return pid_cut_(r, index);
  }
  bool fiducial(h10_reader& r, const unsigned index) {
    return fiducial_cut_(r, index);
  }

  cuts::proton::dq dq_cut_;
  cuts::proton::id pid_cut_;
  cuts::proton::fiducial fiducial_cut_;
};
class ec_photon_detector : public base<ec_photon_detector> {
public: 
  using base_type = base<ec_photon_detector>;
  using particle_type = clas_particle;

private:
  friend class base_type;

  particle_type scat_buffer_;

  bool dq(h10_reader& r, const unsigned index) {
    return dq_cut_(r, index);
  }
  bool pid(h10_reader& r, const unsigned index) {
    return pid_cut_(r, index);
  }
  bool fiducial(h10_reader& r, const unsigned index) {
    return fiducial_cut_(r, index, scat_buffer_);
  }

  cuts::ec_pho


  


// outside of class definition because of old compiler woes
constexpr const min_max_pair PIDENG_ELEC_VX{-77., -50};
constexpr const min_max_pair PIDENG_ELEC_EC_U{0., 390.}; // Nathan: 40 -> 400
constexpr const min_max_pair PIDENG_ELEC_EC_V{0., 360.};
constexpr const min_max_pair PIDENG_ELEC_EC_W{0., 390.};

constexpr const min_max_pair PIDENG_ELEC_EC_EI_CUT{0.06, 10000.};

constexpr const min_max_pair PIDENG_PHOT_EC_U{100, 390.};
constexpr const min_max_pair PIDENG_PHOT_EC_V{0., 360.};
constexpr const min_max_pair PIDENG_PHOT_EC_W{0., 390.};
constexpr const min_max_pair PIDENG_PHOT_BETA{0.93, 1.07};
#warning PHOT_U _V _W not used where they should

class detector {
public:
  using particle_type = particle;
  using particle_vector_type = std::vector<particle>;

  // TODO factor out hardcoded settings into configuration file
  detector(std::shared_ptr<TFile> hfile)
      : electron_good_track_cut_{ELEC_CHARGE, "detector_cuts", "", hfile}
      , electron_id_cut_{ELEC_E_OVER_P_MEAN, ELEC_E_OVER_P_WIDTH,
                         ELEC_CC_N_PHOTO, ELEC_E_OVER_P_NSIGMA,
                         PIDENG_ELEC_EC_EI_CUT, "detector_cuts", "", hfile}
      , electron_fiducial_cut_{PIDENG_ELEC_VX, PIDENG_ELEC_EC_U,
                               PIDENG_ELEC_EC_V, PIDENG_ELEC_EC_W,
                               "detector_cuts", "", hfile}
      , photon_good_ec_cut_{0, "detector_cuts", "", hfile}
      , photon_ec_id_cut_{PHOTON_EC_SAMPFRAC, PHOTON_EC_THRESHOLD,
                          PIDENG_PHOT_BETA, "detector_cuts", "", hfile}
      , photon_ec_fiducial_cut_{PIDENG_ELEC_EC_U, PIDENG_ELEC_EC_V,
                                PIDENG_ELEC_EC_W, "detector_cuts", "", hfile}
      , photon_good_ic_cut_{"detector_cuts", "", hfile}
      , photon_ic_id_cut_{{0.8, 1000}, "detector_cuts", "", hfile}
      , photon_ic_fiducial_cut_{"detector_cuts", "", hfile}
      , proton_good_track_cut_{PROT_CHARGE, "detector_cuts", "", hfile}
      , proton_id_cut_{PROT_DELTA_BETA_MAX, "detector_cuts", "", hfile}
      , proton_fiducial_cut_{"detector_cuts", "", hfile} {}

  particle_vector_type find_electrons(h10_reader& r) {
    const unsigned nEVNT{index::nEVNT(r)};
    particle_vector_type electrons;
    for (unsigned iEVNT = 0; iEVNT < nEVNT; ++iEVNT) {
      if (!electron_good_track_cut_(r, iEVNT) || !electron_id_cut_(r, iEVNT) ||
          !electron_fiducial_cut_(r, iEVNT)) {
        continue;
      }
      // good electron candidate!
      electrons.push_back(make_charged_particle(r, iEVNT, lund_id::e_minus));
    }
    // sort and return our electrons
    sort_by_momentum(electrons);
    return electrons;
  }
  particle_vector_type find_ec_photons(h10_reader& r, const particle& leading) {
    const unsigned nEVNT{index::nEVNT(r)};
    particle_vector_type photons;
    for (unsigned iEVNT = 0; iEVNT < nEVNT; ++iEVNT) {
      if (!photon_good_ec_cut_(r, iEVNT) || !photon_ec_id_cut_(r, iEVNT) ||
          !photon_ec_fiducial_cut_(r, iEVNT, leading.vz)) {
        continue;
      }
      // good photon candidate
      photons.push_back(make_photon_ec(r, iEVNT, leading));
    }
    sort_by_momentum(photons);
    return photons;
  }
  particle_vector_type find_ic_photons(h10_reader& r, const particle& leading) {
    const unsigned nIC{index::nIC(r)};
    particle_vector_type photons;
    for (unsigned iIC = 0; iIC < nIC; ++iIC) {
      if (!photon_good_ic_cut_(r, iIC) || !photon_ic_id_cut_(r, iIC) ||
          !photon_ic_fiducial_cut_(r, iIC, leading.vz)) {
        continue;
      }
      // good photon candidate
      photons.push_back(make_photon_ic(r, iIC, leading));
    }
    sort_by_momentum(photons);
    return photons;
  }
  particle_vector_type find_all_photons(h10_reader& r,
                                        const particle& leading) {
    particle_vector_type photons{find_ec_photons(r, leading)};
    particle_vector_type photons_ic{find_ic_photons(r, leading)};
    photons.insert(photons.end(), photons_ic.begin(), photons_ic.end());
    sort_by_momentum(photons);
    return photons;
  }
  particle_vector_type find_protons(h10_reader& r) {
    const unsigned nEVNT{index::nEVNT(r)};
    particle_vector_type protons;
    for (unsigned iEVNT = 0; iEVNT < nEVNT; ++iEVNT) {
      if (!proton_good_track_cut_(r, iEVNT) || !proton_id_cut_(r, iEVNT) ||
          !proton_fiducial_cut_(r, iEVNT)) {
        continue;
      }
      // good proton candidate!
      protons.push_back(make_charged_particle(r, iEVNT, lund_id::p));
    }
    sort_by_momentum(protons);
    return protons;
  }

private:
  clas_particle make_charged_particle(h10_reader& r, const unsigned iEVNT,
                                      const pdg_id id) {
    const particle part{*pdg_[id], r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                        atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])};
    const double vz{corrections::vertex_CLAS(*r.head.runnb, r.evnt.vz[iEVNT],
                                             part.theta, part.phi)};
    const unsigned iSC = index::sc(r, iEVNT);
    const double dt{r.sc.sc_t[iSC] -
                    r.sc.sc_r[iSC] / (constants::c_cm_ns * part.beta) -
                    *r.head.tr_time};
    return {*r.head.runnb, *r.head.evntid, part, vz, dt};
  }

  clas_particle make_ec_photon(h10_reader& r, const unsigned iEVNT,
                               const clas_particle& electron) {
    const unsigned iEC = index::ec(r, iEVNT);
    TVector3 dir{r.ec.ech_x[iEC], r.ec.ech_y[iEC],
                 r.ec.ech_z[iEC] - electron.vz};
    double mom = fmax(r.ex.ectot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC]) /
                 PHOTON_EC_SAMPFRAC;
    double dt =
        r.ec.ec_t[iEC] - r.ec.ec_r[iEC] / constants::c_cm_ns - *r.head.tr_time;
    return {electron.run,
            electron.event,
            {*pdg_[pdg_id::gamma], mom * dir.Unit()},
            electron.vz,
            dt};
  }
  clas_particle make_ic_photon(h10_reader& r, const unsigned iIC,
                               const clas_particle& electron) {
    TVector3 dir{r.icpb.xc[iIC], r.icpb.yc[iIC], 0. - electron.vz};
    double dt =
        r.icpb.tc[iIC] - dir.Mag() / constants::c_cm_ns - *r.head.tr_time;

    return {electron.run,
            electron.event,
            {*pdg_[pdg_id::gamma], r.icpb.etc[iIC] * dir.Unit()},
            electron.vz,
            dt,
            true};
  }
  void sort_by_momentum(particle_vector_type& parts) {
    std::sort(parts.begin(), parts.end(),
              [](const particle_type& a, const particle_type& b) {
                return (a.mom < b.mom);
              });
  }

  // electron cuts
  constexpr static const double ELEC_CC_N_PHOTO{15.};
  // E_OVER_P_MEAN and E_OVER_P_DELTA are only used when E_OVER_P_NSIGMA is set
  // to zero, else the fancier (hardcoded) method is used with a sigma of
  // E_OVER_P_NSIGMA
  constexpr static const double ELEC_E_OVER_P_MEAN{0.31};
  constexpr static const double ELEC_E_OVER_P_WIDTH{0.31};
  constexpr static const double ELEC_E_OVER_P_NSIGMA{2.5}; // Nathan has 3.5,
                                                           // Mohammed has 2.5
  constexpr static int ELEC_CHARGE{-1};                    // ...duh!
  constexpr static int PROT_CHARGE{1};
  constexpr static double PROT_DELTA_BETA_MAX{0.05};

  cuts::good_track electron_good_track_cut_;
  cuts::electron_id electron_id_cut_;
  cuts::electron_fiducial electron_fiducial_cut_;

  // EC photon cuts
  constexpr static const double PHOTON_EC_SAMPFRAC{0.273};
  constexpr static const double PHOTON_EC_THRESHOLD{0.800};
  cuts::good_ec photon_good_ec_cut_;
  cuts::photon_ec_id photon_ec_id_cut_;
  cuts::photon_ec_fiducial photon_ec_fiducial_cut_;

  // IC photon cuts
  cuts::good_ic photon_good_ic_cut_;
  cuts::photon_ic_id photon_ic_id_cut_;
  cuts::photon_ic_fiducial photon_ic_fiducial_cut_;

  // proton cuts
  cuts::good_track proton_good_track_cut_;
  cuts::proton_id proton_id_cut_;
  cuts::proton_fiducial proton_fiducial_cut_;
};
}

#endif
