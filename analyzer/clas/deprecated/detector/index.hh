#ifndef ANALYZER_CLAS_SPECS_LOADED
#define ANALYZER_CLAS_SPECS_LOADED

#include <cstdint>

#include "util/constants.hh"

namespace clas {

// =============================================================================
// "safe" indexing functions always return a valid index
// =============================================================================
namespace index {
template <class Reader> size_t nEVNT(Reader& r) {
  return *r.evnt.gpart > 0 ? static_cast<size_t>(*r.evnt.gpart) : 0;
}
template <class Reader> unsigned nIC(Reader& r) {
  return *r.icpb.icpart > 0 ? static_cast<size_t>(*r.icpb.icpart) : 0;
}
template <class Reader> size_t sc(Reader& r, const size_t iEVNT) {
  const int sc{r.evnt.sc[iEVNT] - 1};
  return (sc > 0 && sc < *r.sc.sc_part) ? sc : 0;
}
template <class Reader> size_t ec(Reader& r, const size_t iEVNT) {
  const int ec{r.evnt.ec[iEVNT] - 1};
  return (ec > 0 && ec < *r.ec.ec_part) ? ec : 0;
}
template <class Reader> size_t dc(Reader& r, const unsigned iEVNT) {
  const int dc{r.evnt.dc[iEVNT] - 1};
  return (dc > 0 && dc < *r.dc.dc_part) ? dc : 0;
}
template <class Reader> size_t cc(REader& r, const size_t iEVNT) {
  const int cc{r.evnt.cc[iEVNT] - 1};
  return (cc > 0 && cc < *r.cc.cc_part) ? cc : 0;
}
template <class Reader> size_t ichb(Reader& r, const size_t iIC) {
  const int ichb =
      (r.icpb.statc[iIC] - (r.icpb.statc[iIC] % 10000)) / 10000 - 1;
  return (ichb > 0) ? ichb : 0;
}
} // ns index
} // ns clas

#endif
