#ifndef ANALYZER_CLAS_DETECTOR_GEOMETRY_LOADED
#define ANALYZER_CLAS_DETECTOR_GEOMETRY_LOADED

#include <cmath>
#include <analyzer/math.hh>

namespace analyzer {
namespace clas {
namespace detector {

// get the corresponding sector number for an angle phi [0, 2pi]
// in the range [0-5]. User must add 1 to get CLAS sectors.
inline size_t sector(const double phi) {
  // formula: (phi + pi/6)/(pi/3) modulo the number of sectors to account for
  // rollover for angles close to 2pi
  return static_cast<size_t>((3 * phi / constants::pi + 0.5)) % 6;
}
inline size_t sector(const double x, const double y) {
  sector{atan2_pos(y, x)};
}

// get the angle relative to the center of the sector
inline double angle_in_sector(double phi) {
  phi -= sector(phi) * constants::pi / 3;
  // deal with angles close to 2pi (in sector 0)
  if (phi > constants::pi) {
    phi -= 2 * constants::pi;
  }
  return phi;
}
inline double angle_in_sector(const double x, const double y) {
  return angle_in_sector(atan2_pos(y, x));
}

} // ns detector
} // ns clas
} // ns analyzer
