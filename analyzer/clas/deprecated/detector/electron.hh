#ifndef ANALYZER_CLAS_DETECTOR_ELECTRON_LOADED
#define ANALYZER_CLAS_DETECTOR_ELECTRON_LOADED

#include <algorithm>
#include <cstdint>
#include <memory>
#include <vector>

#include <TFile>
#include <TParticlePDG.h>

#include <analyzer/util/cut.hh>
#include <analyzer/util/configuration.hh>
#include <analyzer/physics/pdg.hh>

#include <analyzer/clas/detector/base.hh>
#include <analyzer/clas/detector/index.hh>
#include <analyzer/clas/detector/particle.hh>
#include <analyzer/clas/detector/pid.hh>
#include <analyzer/clas/detector/fiducial.hh>

namespace analyzer {
namespace clas {
namespace detector {

namespace cut {
template <class Reader> class electron_dq;
template <class Reader> class electron_fiducial;
template <class Reader> class electron_id;
} // ns cut

template <class Reader>
class electron_detector : public detector_base<electron_detector, Reader&>,
                          public configurable {
public:
  using reader_type = Reader
  using particle_type = clas_particle;
  using base_type = detector_base<electron_detector, Reader&>;

  electron_detector(const ptree& settings, const string_path& path,
                    std::shared_ptr<TFile> hfile)
      : configurable{settings, path}
      , pdg_electron_{*pdg_particle(pdg_id::e_minus)}
      , dq_cut_{settings, path / "dq", hfile}
      , fiducial_cut_{settings, path / "fiducial", hfile}
      , pid_cut_{settings, path / "pid", hfile} {}

private:
  friend class base_type;

  bool dq(const particle_type& part, Reader& r, const size_t iEVNT) {
    return dq_cut_(part, r, iEVNT);
  }
  bool fiducial(const particle_type& part, Reader& r, const size_t iEVNT) {
    return fiducial_cut_(part, r, iEVNT);
  }
  bool pid(const particle_type& part, Reader& r, const size_t iEVNT) {
    return pid_cut_(part, r, iEVNT);
  }
  // create a charged particle
  particle_type particle(Reader& r, const size_t iEVNT) {
    const particle part{pdg_electron_, r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                        atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])};
    const double vz{corrections::vertex_CLAS(*r.head.runnb, r.evnt.vz[iEVNT],
                                             part.theta, part.phi)};
    const unsigned iSC = index::sc(r, iEVNT);
    const double dt{r.sc.sc_t[iSC] -
                    r.sc.sc_r[iSC] / (constants::c_cm_ns * part.beta) -
                    *r.head.tr_time};
    return {*r.head.runnb, *r.head.evntid, part, vz, dt};
  }

  const TParticlePDG pdg_electron_;
  cut::electron_dq<Reader> dq_cut_;
  cut::electron_fiducial<Reader> fiducial_cut_;
  cut::electron_id<Reader> pid_cut_;
};

namespace cut {
template <class Reader>
class electron_dq : public cut<electron_dq, Reader&, const size_t /* iEVNT */> {
public:
  using parent_type = cut<electron_dq, Reader&, const size_t>;

  electron_dq(const ptree& settings, const string_path& path,
              std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{settings, path, hfile} {}

  const std::string& path, const std::string& context,
              std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "electron_dq"),
                    make_title(context, "Electron DQ Cut")} {
    ; // do nothing
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    // integrity check and charge
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] >= 0 ||
        r.evnt.p[iEVNT] <= MIN_MOM || r.evnt.p[iEVNT] > MAX_MOM) {
      return false;
    }
    // index to the other banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    const int iCC = r.evnt.cc[iEVNT] - 1;
    const int iDC = r.evnt.dc[iEVNT] - 1;
    const int iSC = r.evnt.sc[iEVNT] - 1;
    if (iEC < 0 || iCC < 0 || iDC < 0 || iSC < 0 || iEC > *r.ec.ec_part ||
        iCC > *r.cc.cc_part || iDC > *r.dc.dc_part || iSC > *r.sc.sc_part ||
        r.dc.dc_stat[iDC] <= 0 || r.ec.ec_stat[iEC] <= ||
        r.sc.sc_stat[iSC] <= 0 || r.cc.cc_stat[iCC] <= 0) {
      return false;
    }
    // all OK!
    return true;
  }

private:
  constexpr static const double MIN_MOM{0.8};
  constexpr static const double MAX_MOM{6.064};
};
template <class Reader>
class electron_id : public cut<electron_id, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<electron_id, Reader&, const size_t>;

  electron_id(const std::string& path, const std::string& context,
              std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "electron_id"),
                    make_title(context, "Electron PID Cuts")}
      , ec_e_over_p_{histo_path(), "(electron)", hfile}
      , ec_ei_eo_{histo_path(), "(electron)", hfile}
      , cc_nphe_{histo_path(), "(electron)", hfile} {
    electron_id_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= ec_e_over_p_(r, iEVNT);
    cut_status &= ec_ei_eo_(r, iEVNT);
    cut_status &= cc_nphe_(r, iEVNT);
    return cut_status;
  }

private:
  ec_e_over_p ec_e_over_p_;
  ec_ei_eo ec_ei_eo_;
  cc_nphe cc_nphe_;
};
template <class Reader>
class electron_fiducial
    : public cut<electron_fiducial, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<electron_fiducial, Reader&, const size_t>;

  electron_fiducial(const std::string& path, const std::string& context,
                    std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "electron_fiducial"),
                    make_title(context, "Electron Fiducial Cuts")}
      , vertex_cut_{histo_path(), "(electron)", hfile}
      , ec_cut_{histo_path(), "(electron)", hfile}
      , ic_cut_{histo_path(), "(electron)", hfile}
      , dc_cut_{histo_path(), "(electron)", hfile}
      , cc_cut_{histo_path(), "(electron)", hfile} {
    electron_fiducial_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= vertex_cut_(r, iEVNT);
    cut_status &= ec_cut_(r, iEVNT);
    cut_status &= ic_cut_(r, iEVNT);
    cut_status &= dc_cut_(r, iEVNT);
    cut_status &= cc_cut_(r, iEVNT);
    return cut_status;
  }

private:
  vertex_cut<Reader> vertex_cut_;
  ec_cut<Reader> ec_cut_;
  ic_cut<Reader> ic_cut_;
  dc_cut<Reader> dc_cut_;
  cc_cut<Reader> cc_cut_;
};

} // ns cut

} // ns detector
} // ns clas
} // ns analyzer

#endif
