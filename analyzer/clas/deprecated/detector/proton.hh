#ifndef ANALYZER_CLAS_DETECTOR_PROTON_LOADED
#define ANALYZER_CLAS_DETECTOR_PROTON_LOADED

#include <algorithm>
#include <cstdint>
#include <memory>
#include <vector>

#include <TFile.h>
#include <TParticlePDG.h>

#include <analyzer/util/cut.hh>
#include <analyzer/physics/pdg.hh>
#include <analyzer/clas/detector/index.hh>
#include <analyzer/clas/detector/pid.hh>
#include <analyzer/clas/detector/fiducial.hh>
#include <analyzer/clas/detector/base.hh>
#include <analyzer/clas/detector/particle.hh>

namespace analyzer {
namespace clas {
namespace detector {

namespace cut {
template <class Reader> class proton_dq;
template <class Reader> class proton_id;
template <class Reader> class proton_fiducial;
} // ns cut

template <class Reader>
class proton_detector : public track_base<proton_detector, Reader&> {
public:
  using reader_type = Reader
  using particle_type = clas_particle;
  using base_type = detector_base<proton_detector, Reader&>;

  proton_detector(std::shared_ptr<TFile> hfile)
      : pdg_proton_{*pdg_particle(pdg_id::p)}
      , dq_cut_{"detector_cuts", "", hfile}
      , pid_cut_{"detector_cuts", "", hfile}
      , fiducial_cut_{"detector_cuts", "", hfile} {}

private:
  friend class base_type;

  bool dq(Reader& r, const size_t iEVNT) {
    return dq_cut_(r, iEVNT);
  }
  bool pid(Reader& r, const size_t iEVNT) {
    return pid_cut_(r, iEVNT);
  }
  bool fiducial(Reader& r, const size_t iEVNT) {
    return fiducial_cut_(r, iEVNT);
  }
  particle_type particle(Reader& r, const size_t iEVNT) {
    const particle part{pdg_proton, r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                        atan2(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])};
    const double vz{corrections::vertex_CLAS(*r.head.runnb, r.evnt.vz[iEVNT],
                                             part.theta, part.phi)};
    const unsigned iSC = index::sc(r, iEVNT);
    const double dt{r.sc.sc_t[iSC] -
                    r.sc.sc_r[iSC] / (constants::c_cm_ns * part.beta) -
                    *r.head.tr_time};
    return {*r.head.runnb, *r.head.evntid, part, vz, dt};
  }

  const TParticlePDG pdg_proton_;
  cut::proton::dq<Reader> dq_cut_;
  cut::proton::id<Reader> pid_cut_;
  cut::proton::fiducial<Reader> fiducial_cut_;
};

namespace cut {
template <class Reader>
class proton_dq : public cut<proton_dq, Reader&, const size_t /* iEVNT */> {
public:
  using parent_type = cut<proton_dq, Reader&, const size_t>;

  proton_dq(const std::string& path, const std::string& context,
              std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "proton_dq"),
                    make_title(context, "Electron DQ Cut")} {
    ; // do nothing
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    // integrity check and charge
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] <= 0 ||
        r.evnt.p[iEVNT] <= MIN_MOM || r.evnt.p[iEVNT] > MAX_MOM) {
      return false;
    }
    // iEVNT to the other banks
    const int iDC = r.evnt.dc[iEVNT] - 1;
    const int iSC = r.evnt.sc[iEVNT] - 1;
    // TODO dc_stat check as well? or maybe redundant with stat?
    if (iDC < 0 || iSC < 0 || iDC > *r.dc.dc_part || iSC > *r.sc.sc_part ||
        r.sc.sc_stat[iSC] <= 0) {
      return false;
    }
    // all OK!
    return true;
  }

private:
  constexpr static const double MIN_MOM{0.};
  constexpr static const double MAX_MOM{6.064};
};
template <class Reader>
class proton_id : public cut<proton_id, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<proton_id, Reader&, const size_t>;

  proton_id(const std::string& path, const std::string& context,
            std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "proton_id"),
                    make_title(context, "Proton PID Cuts")}
      , delta_beta_cut_{histo_path(), "(proton)", hfile} {
    proton_id_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= delta_beta_cut_(r, iEVNT);
    return cut_status;
  }

private:
  delta_beta_cut<Reader> delta_beta_cut_;
};
// =============================================================================
// proton_fiducial
// applies:
//  * vertex
//  * ic_shadow
//  * dc
//  * rtpc_endplate TODO (not implemented!) (needed???)
// =============================================================================
template <class Reader>
class proton_fiducial
    : public cut<proton_fiducial, Reader&, const size_t /* iEVNT */> {
  using parent_type = cut<proton_fiducial, Reader&, const size_t>;

  proton_fiducial(const std::string& path, const std::string& context,
                    std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{make_path(path, "proton_fiducial"),
                    make_title(context, "Proton Fiducial Cuts")}
      , vertex_cut_{histo_path(), "(proton)", hfile}
      , ic_cut_{histo_path(), "(proton)", hfile}
      , dc_cut_{histo_path(), "(proton)", hfile} {
    proton_fiducial_histos(*this, hfile);
  }

  bool cut_impl(Reader& r, const size_t iEVNT) {
    bool cut_status{true};
    cut_status &= vertex_cut_(r, iEVNT);
    cut_status &= ic_cut_(r, iEVNT);
    cut_status &= dc_cut_(r, iEVNT);
    return cut_status;
  }

private:
  vertex_cut<Reader> vertex_cut_;
  ic_cut<Reader> ic_cut_;
  dc_cut<Reader> dc_cut_;
};

} // ns cut

} // ns detector
} // ns clas
} // ns analyzer

#endif

#endif
