#ifndef ANALYZER_CLAS_DETECTOR_PID_CUTS_LOADED
#define ANALYZER_CLAS_DETECTOR_PID_CUTS_LOADED

#include <cmath>
#include <memory>

#include <analyzer/util/assert.hh>
#include <analyzer/util/cut.hh>
#include <analyzer/util/histogrammer.hh>
#include <analyzer/util/math.hh>

#include <clas/particle.hh>

namespace analyzer {
namespace clas {
namespace cuts {

// =============================================================================
//  EC E/p cut
// =============================================================================
template <class Reader>
class ec_e_over_p : public cut<ec_e_over_p, Reader&, const size_t> {
public:
  using parent_type = cut<ec_e_over_p, Reader&, const size_t>;

  ec_e_over_p(const ptree& settings, const string_path& path,
              const std::string& context,
              std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{path, context}
      , mean_param_{settings.get_vector<double>(path / "mean")}
      , sigma_param_{settings.get_vector<double>(path / "sigma")}
      , nsigma_{settings.get<double>(path / "nsigma")} {}

  bool cut_impl(Reader& r, const size_t iEVNT) {
    const size_t iEC = index::ec(r, iEVNT);
    // get E and p
    const double E{fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec_eo[iEC])};
    const double p{r.evnt.p[iEVNT]};
    // evalulate the parameterization at p
    const double mean {eval_poly(mean_par_, p)};
    const double delta{eval_poly(sigma_par_, p) * nsigma_};
    // actual cut
    return (fabs(mean - E / p) < delta);
  }

private:
  const std::vector<double>& mean_param_;  // mean EC E/p as a function of p
  const std::vector<double>& sigma_param_; // sigma of EC E/p as a function of p
  const double nsigma_; // number of standard deviations to cut on
};

// =============================================================================
// cut on ec_energy
// =============================================================================
class ec_energy : public cut<ec_energy, h10_reader&, const size_t> {
public:
  using parent_type = cut<ec_energy, h10_reader&, const size_t>;

  ec_energy(const ptree& settings, const string_path& path,
            const std::string& context, std::shared_ptr<TFile> hfile = nullptr)
      : parent_type{path, context}
      , ei_range_{settings.get<double>(path / "ei" / "min"),
                  settings.get<double>(path / "ei" / "max")}
      , eo_range_{settings.get<double>(path / "eo" / "min"),
                  settings.get<double>(path / "eo" / "max")} {}

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const int iEC = index::ec(r, iEVNT);
    const double ei{r.ec.ec_ei[iEC]};
    const double eo{r.ec.ec_eo[iEC]};
    return (ei_range_.includes(ei) && eo_range_.includes(eo));
  }

private:
  const interval ei_range_;
  const interval eo_range_;
};
// =============================================================================
// cut on number of photo-electrons in the CC
// =============================================================================
class cc_nphe : public cut<cc_nphe, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<cc_nphe, h10_reader&, const unsigned>;

  cc_nphe(double min, const std::string& path = "",
          const std::string& context = "", std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "cc_nphe"),
                    make_title(context, "N_{phe} Cut")}
      , cc_nphe_min_{min} {
    // plot the other electron PID variables
    if (hfile) {
      // electron EC_eo vs EC_ei
      add_histo(hfile, "e_eo_vs_ei", "Electron EC_eo vs EC_ei",
                {"E_{in} [GeV]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ec_ei[index::ec(r, iEVNT)];
                 },
                 200, 0., 0.6},
                {"E_{out} [GeV]", [](h10_reader& r, const unsigned iEVNT) {
                  return double{r.ec.ec_eo[index::ec(r, iEVNT)]};
                }, 200, 0., 0.4});
      // electron (e_tot/p vs p)
      add_histo(
          hfile, "e_etot_p_vs_p", "Electron E_{tot}/p vs p",
          {"p_{e} [GeV]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; },
           200, 0.5, 5.},
          {"E_{tot}/p", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iEC = index::ec(r, iEVNT);
            const double p{r.evnt.p[iEVNT]};
            const double etot{
                fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC])};
            return (p > 0) ? etot / p : -1;
          }, 200, 0., 1.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const unsigned iCC = index::cc(r, iEVNT);
    if (r.cc.nphe[iCC] < cc_nphe_min_) {
      return false;
    }
    // All ok!
    return true;
  }

private:
  const double cc_nphe_min_;
};

// =============================================================================
//  EC total energy cut
// =============================================================================
class ec_etot : public cut<ec_etot, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<ec_etot, h10_reader&, const unsigned>;

  ec_etot(double sampfrac, double threshold, const std::string& path = "",
          const std::string& context = "", std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "ec_etot"),
                    make_title(context, "EC Etot Cut")}
      , sampfrac_{sampfrac}
      , threshold_{threshold} {
    // plot the other track PID variables
    if (hfile) {
      // Etot
      add_histo(hfile, "ec_etot", "E_{tot}",
                {"E_{tot} [GeV]", [](h10_reader& r, const unsigned iEVNT) {
                  const int iEC = index::ec(r, iEVNT);
                  return fmax(r.ec.etot[iEC],
                              r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC]);
                }, 200, 0., 2.0});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // get EC index
    const int iEC = index::ec(r, iEVNT);
    // calculate Etot and mom
    const double ece{fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC])};
    if (ece / sampfrac_ < threshold_) {
      return false;
    }
    // all OK
    return true;
  }

private:
  const double sampfrac_;
  const double threshold_;
};
// =============================================================================
//  beta cut
// =============================================================================
class beta : public cut<beta, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<beta, h10_reader&, const unsigned>;

  beta(min_max_pair beta_cut, const std::string& path = "",
       const std::string& context = "", std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "beta"), make_title(context, "Beta Cut")}
      , beta_cut_{beta_cut} {
    // plot the other track PID variables
    if (hfile) {
      // beta
      add_histo(hfile, "beta", "Beta",
                {"beta", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.b[iEVNT];
                }, 200, 0., 1.1});
    }
  }
  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    const double beta{r.evnt.b[iEVNT]};
    if (beta < beta_cut_.min || beta > beta_cut_.max) {
      return false;
    }
    // all ok
    return true;
  }

private:
  const min_max_pair beta_cut_;
};
// =============================================================================
//  delta_beta cut
// =============================================================================
class delta_beta
    : public cut<delta_beta, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<delta_beta, h10_reader&, const unsigned>;

  delta_beta(lund_id pid, double delta_beta_max, const std::string& path = "",
             const std::string& context = "",
             std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "delta_beta"),
                    make_title(context, "Beta Cut")}
      , mass_{pdg_mass_GeV(pid)}
      , delta_beta_max_{delta_beta_max} {
    // plot the other track PID variables
    if (hfile) {
      // delta_beta
      add_histo(
          hfile, "delta_beta", "#Delta#beta",
          {"p [GeV]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; },
           200, 0., 3.},
          {"#Delta#beta", [=](h10_reader& r, const unsigned iEVNT) {
            return calc_delta_beta(r, iEVNT);
          }, 200, -.7, .7});
    }
  }
  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    if (fabs(calc_delta_beta(r, iEVNT)) > delta_beta_max_) {
      return false;
    }
    // all ok
    return true;
  }

  double calc_delta_beta(h10_reader& r, const unsigned iEVNT) const {
    const unsigned iSC{index::sc(r, iEVNT)};
    const double b1 = r.sc.sc_r[iSC] / (r.sc.sc_t[iSC] - *r.head.tr_time) /
                      constants::c_cm_ns;
    const double b2 =
        r.evnt.p[iEVNT] / sqrt(pow(r.evnt.p[iEVNT], 2) + pow(mass_, 2));
    return b1 - b2;
  }
private:
  const double mass_;
  const double delta_beta_max_;
};
// =============================================================================
// ic etc energy cut
// =============================================================================
class ic_etc : public cut<ic_etc, h10_reader&, const unsigned /* iIC */> {
public:
  using parent_type = cut<ic_etc, h10_reader&, const unsigned>;

  ic_etc(min_max_pair etc_cut, const std::string& path,
         const std::string& context, std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "ic_etc"),
                    make_title(context, "IC ETC Cut")}
      , etc_cut_{etc_cut} {
    if (hfile) {
      // Etot
      add_histo(hfile, "ic_etc", "IC ETC",
                {"E [GeV]", [](h10_reader& r, const unsigned iIC) {
                  return r.icpb.etc[iIC];
                }, 200, 0., 3.0});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iIC) {
    if (r.icpb.etc[iIC] < etc_cut_.min || r.icpb.etc[iIC] > etc_cut_.max) {
      return false;
    }
    // all ok
    return true;
  }

private:
  min_max_pair etc_cut_;
};

// =============================================================================
// electron_id: applies the ec_e_over_p, ec_ei and cc_nnphe
// =============================================================================
class electron_id
    : public cut<electron_id, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<electron_id, h10_reader&, const unsigned>;

  electron_id(double mean, double width, double nphot, double nsigma,
              min_max_pair ei_cut, const std::string& path = "",
              const std::string& context = "",
              std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "electron_id"),
                    make_title(context, "Electron PID Cuts")}
      , ec_e_over_p_{mean, width, nsigma, histo_path(), "(electrons)", hfile}
      , ec_ei_{ei_cut, histo_path(), "(electrons)", hfile}
      , cc_nphe_{nphot, histo_path(), "(electrons)", hfile} {
    if (hfile) {
      // electron EC_eo vs EC_ei
      add_histo(hfile, "e_eo_vs_ei", "Electron EC_eo vs EC_ei",
                {"E_{in} [GeV]",
                 [](h10_reader& r, const unsigned iEVNT) {
                   return r.ec.ec_ei[index::ec(r, iEVNT)];
                 },
                 200, 0., 0.6},
                {"E_{out} [GeV]", [](h10_reader& r, const unsigned iEVNT) {
                  return double{r.ec.ec_eo[index::ec(r, iEVNT)]};
                }, 200, 0., 0.4});
      // electron (e_tot/p vs p)
      add_histo(
          hfile, "e_etot_p_vs_p", "Electron E_{tot}/p vs p",
          {"p_{e} [GeV]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; },
           200, 0.5, 5.},
          {"E_{tot}/p", [](h10_reader& r, const unsigned iEVNT) {
            const unsigned iEC = index::ec(r, iEVNT);
            const double p{r.evnt.p[iEVNT]};
            const double etot{
                fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC])};
            return (p > 0) ? etot / p : -1;
          }, 200, 0., 1.});
      // number of photo-electrons
      add_histo(hfile, "e_nphe", "Electron N_{phe}",
                {"#", [](h10_reader& r, const unsigned iEVNT) {
                  const unsigned iCC = index::cc(r, iEVNT);
                  return r.cc.nphe[iCC];
                }, 50, 0., 350.});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip right ahead to the three actual
    // electron ID cuts:
    bool cut_status{true};
    cut_status &= ec_e_over_p_(r, iEVNT);
    cut_status &= ec_ei_(r, iEVNT);
    cut_status &= cc_nphe_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::ec_e_over_p ec_e_over_p_;
  cuts::ec_ei ec_ei_;
  cuts::cc_nphe cc_nphe_;
};
// =============================================================================
// proton_id: delta_beta cut
// =============================================================================
class proton_id
    : public cut<proton_id, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<proton_id, h10_reader&, const unsigned>;

  proton_id(double delta_beta_max, const std::string& path = "",
            const std::string& context = "", std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "proton_id"),
                    make_title(context, "Proton PID Cuts")}
      , delta_beta_{lund_id::p, delta_beta_max, histo_path(), "(protons)",
                    hfile} {
    if (hfile) {
      // delta_beta
      add_histo(
          hfile, "delta_beta", "#Delta#beta",
          {"p [GeV]",
           [](h10_reader& r, const unsigned iEVNT) { return r.evnt.p[iEVNT]; },
           200, 0., 3.},
          {"#Delta#beta", [=](h10_reader& r, const unsigned iEVNT) {
            return delta_beta_.calc_delta_beta(r, iEVNT);
          }, 200, -.7, .7});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip right ahead to the actual
    // proton ID cuts:
    bool cut_status{true};
    cut_status &= delta_beta_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::delta_beta delta_beta_;
};

// =============================================================================
// photon_ec_id: etot and beta cuts
// =============================================================================
class photon_ec_id
    : public cut<photon_ec_id, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<photon_ec_id, h10_reader&, const unsigned>;

  photon_ec_id(double sampfrac, double threshold, min_max_pair beta_cut,
               const std::string& path = "", const std::string& context = "",
               std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "photon_ec_id"),
                    make_title(context, "EC Photon PID Cuts")}
      , ec_etot_{sampfrac, threshold, histo_path(), "(EC photons)", hfile}
      , beta_{beta_cut, histo_path(), "(EC photons)", hfile} {
    if (hfile) {
      // Etot
      add_histo(hfile, "ec_etot", "EC Photon E_{tot}",
                {"E_{tot} [GeV]", [](h10_reader& r, const unsigned iEVNT) {
                  const int iEC = index::ec(r, iEVNT);
                  return fmax(r.ec.etot[iEC],
                              r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC]);
                }, 200, 0., 2.0});
      // beta
      add_histo(hfile, "beta", "EC Photon Beta",
                {"beta", [](h10_reader& r, const unsigned iEVNT) {
                  return r.evnt.b[iEVNT];
                }, 200, 0., 1.1});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip right ahead to the actual
    // photon_ec ID cuts:
    bool cut_status{true};
    cut_status &= ec_etot_(r, iEVNT);
    cut_status &= beta_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::ec_etot ec_etot_;
  cuts::beta beta_;
};
// =============================================================================
// photon_ic_id: just the ic_etc cut
// =============================================================================
class photon_ic_id
    : public cut<photon_ic_id, h10_reader&, const unsigned /* iEVNT */> {
public:
  using parent_type = cut<photon_ic_id, h10_reader&, const unsigned>;

  photon_ic_id(min_max_pair etc_cut, const std::string& path,
               const std::string& context, std::shared_ptr<TFile> hfile = 0x0)
      : parent_type{make_path(path, "photon_ic_id"),
                    make_title(context, "IC Photon PID Cuts")}
      , ic_etc_{etc_cut, histo_path(), "(IC photons)", hfile} {
    if (hfile) {
      // Etc
      add_histo(hfile, "ic_etc", "IC Photon ETC",
                {"E [GeV]", [](h10_reader& r, const unsigned iIC) {
                  return r.icpb.etc[iIC];
                }, 200, 0., 2.0});
    }
  }

  bool cut_impl(h10_reader& r, const unsigned iEVNT) {
    // assume a minimal good track, skip right ahead to the actual
    // photon_ec ID cuts:
    bool cut_status{true};
    cut_status &= ic_etc_(r, iEVNT);
    // done!
    return cut_status;
  }

private:
  cuts::ic_etc ic_etc_;
};

} // ns cuts

} // eg6

#endif
