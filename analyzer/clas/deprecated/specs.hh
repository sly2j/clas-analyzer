#ifndef EG6_SPECS_LOADED
#define EG6_SPECS_LOADED

#include <cmath>

#include <TVector3.h>
#include <TLorentzVector.h>

#include "util/constants.hh"
#include "eg6/h10_reader.hh"

namespace eg6 {

// =============================================================================
// "safe" indexing functions always return a valid index
// =============================================================================
namespace index {
inline unsigned nEVNT(h10_reader& r) {
  return *r.evnt.gpart > 0 ? static_cast<unsigned>(*r.evnt.gpart) : 0;
}
inline unsigned sc(h10_reader& r, const unsigned iEVNT) {
  const int sc{r.evnt.sc[iEVNT] - 1};
  return (sc > 0 && sc < *r.sc.sc_part) ? sc : 0;
}
inline unsigned ec(h10_reader& r, const unsigned iEVNT) {
  const int ec{r.evnt.ec[iEVNT] - 1};
  return (ec > 0 && ec < *r.ec.ec_part) ? ec : 0;
}
inline unsigned dc(h10_reader& r, const unsigned iEVNT) {
  const int dc{r.evnt.dc[iEVNT] - 1};
  return (dc > 0 && dc < *r.dc.dc_part) ? dc : 0;
}
inline unsigned cc(h10_reader& r, const unsigned iEVNT) {
  const int cc{r.evnt.cc[iEVNT] - 1};
  return (cc > 0 && cc < *r.cc.cc_part) ? cc : 0;
}
inline unsigned nIC(h10_reader& r) {
  return *r.icpb.icpart > 0 ? static_cast<unsigned>(*r.icpb.icpart) : 0;
}
inline unsigned ichb(h10_reader& r, const unsigned iIC) {
  int ichb = (r.icpb.statc[iIC] - (r.icpb.statc[iIC]%10000))/10000 - 1;
  return (ichb > 0) ? static_cast<unsigned>(ichb) : 0;
}
} // ns index

// =============================================================================
// run info
// =============================================================================
inline lund_id target_type(const int run) {
  // hydrogen target:
  if ((run >= 61963 && run <= 61966) || (run >= 61432 && run <= 61446)) {
    return lund_id::p;
  }
  // default to helium-4
  return lund_id::He4;
}
inline double target_mass(const int run) {
  return pdg_mass_GeV(target_type(run));
}
} // ns run_info

// =============================================================================


} // ns eg6

#endif
