#ifndef ANALYZER_CLAS_EG1DVCS_PARTICLE_LOADED
#define ANALYZER_CLAS_EG1DVCS_PARTICLE_LOADED

#include <cstdint>
#include <memory>
#include <tuple>

#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// =============================================================================
// candidate: base type for candidate particles
// =============================================================================
template <class Reader> struct candidate {
  // reference to the current entry
  Reader& r;
  const size_t index;
  const TParticlePDG& pdg;

  candidate(Reader& r, const size_t index, const TParticlePDG& pdg)
      : r{r}, index{index}, pdg{pdg} {}
};


// =============================================================================
// charged_candidate: base type for charged particle candidates
//
// Performs the vertex correction
// =============================================================================
template <class Reader> struct charged_candidate : clas_particle, candidate<Reader> {
  using candidate_base_type = candidate<Reader>;

  // detector info
  double dbeta;         // difference between beta SC and tracking beta
  TVector3 c_xyz_cor;   // vertex corrected direction cosines
  size_t sector;        // sector in clas
  double phi_sector;    // phi position relative to sector
  double phi_dc_sector; // phi position (relative to sector) in DC
  TVector3 ic_coords;   // coordinates at the IC plane
  charged_candidate(Reader& r, const size_t iEVNT, const TParticlePDG& pdg,
                    const double leading_vz = 0, const double leading_t = 0)
      : candidate_base_type{r, iEVNT, pdg}
      , phi_dc_sector{clas::angle_in_sector(
            r.dc.dc_sect[iEVNT] - 1, r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT])}
      , ic_coords{clas::project_dc1_to_ic(
            {r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT], r.dc.tl1_z[iEVNT]},
            {r.dc.tl1_cx[iEVNT], r.dc.tl1_cy[iEVNT],
             sqrt(1 - pow(r.dc.tl1_cx[iEVNT], 2) -
                  pow(r.dc.tl1_cy[iEVNT], 2))})} {
    // get beam x and y position (at the nominal vertex)
    TVector3 beam_pos{r.slow.beam_pos(*r.head.rastr1, *r.head.rastr2)};
    // get corrected c_xyz and vz info
    double vz;
    std::tie(c_xyz_cor, vz) = r.slow.reconstruct_vertex(
        beam_pos, {r.dc.tl1_cx[iEVNT], r.dc.tl1_cy[iEVNT], -999},
        {r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT], r.dc.tl1_z[iEVNT]},
        r.evnt.p[iEVNT], r.evnt.q[iEVNT]);
    // properly initialize the underlying clas particle
    static_cast<clas_particle&>(*this) =
        clas_particle{*r.head.runnb,
                      *r.head.evntid,
                      {pdg, r.evnt.p[iEVNT], acos(c_xyz_cor.Z()),
                       atan2_pos(c_xyz_cor.Y(), c_xyz_cor.X())},
                      {beam_pos.X(), beam_pos.Y(), vz},
                      0};
    // timing and phi in sector info
    dbeta = r.evnt.b[iEVNT] - beta;
    t = r.sc.sc_t[iEVNT] - r.sc.sc_r[iEVNT] / (constants::c_cm_ns * beta);
    sector = clas::sector(phi);
    phi_sector = clas::angle_in_sector(sector, phi);
    // get timing and vertex difference; special particles without primaries
    // (leading_vz and leading_t set to zero)
    if (leading_vz && leading_t) {
      dvz = vertex.Z() - leading_vz;
      dt = t - leading_t;
    } else {
      dvz = 0;
      dt = 0;
    }
  }
  // constructor for leading particles
  charged_candidate(Reader& r, const size_t iEVNT, const TParticlePDG& pdg)
      : charged_candidate(r, iEVNT, pdg) {}
  // constructor for secondary particles
  charged_candidate(Reader& r, const size_t iEVNT, const TParticlePDG& pdg,
                    const clas_particle& leading)
      : charged_candidate(r, iEVNT, pdg, leading.vertex.Z(), leading.t) {}
};

// =============================================================================
// electron particle candidate
//
// Adds in computed EC info
// =============================================================================
template <class Reader> struct electron_candidate : charged_candidate<Reader> {
  using candidate_base_type = charged_candidate<Reader>;
  // detector info
  double ec_etot;  // total energy deposit in the EC
  TVector3 ec_uvw; // U,V,W coordinates at the EC surface

  electron_candidate(Reader& r, const size_t iEVNT,
                     const clas_particle& leading = {})
      : candidate_base_type(r, iEVNT, PDG_ELECTRON, leading)
      , ec_etot{fmax(r.ec.etot[iEVNT], r.ec.ec_ei[iEVNT] + r.ec.ec_eo[iEVNT])}
      , ec_uvw{clas::ec_xyz2uvw(
            {r.ec.ech_x[iEVNT], r.ec.ech_y[iEVNT], r.ec.ech_z[iEVNT]})} {}
};
// =============================================================================
// various hadron particle candidates
// =============================================================================
template <class Reader> struct pion_candidate : charged_candidate<Reader> {
  using candidate_base_type = charged_candidate<Reader>;
  pion_candidate(Reader& r, const size_t iEVNT,
                 const clas_particle& leading = {})
      : candidate_base_type{r, iEVNT, (r.evnt.q[iEVNT] > 0 ? PDG_PI_PLUS : PDG_PI_MINUS),
                  leading} {}
};
template <class Reader> struct kaon_candidate : charged_candidate<Reader> {
  using candidate_base_type = charged_candidate<Reader>;
  kaon_candidate(Reader& r, const size_t iEVNT,
                 const clas_particle& leading = {})
      : candidate_base_type{r, iEVNT, (r.evnt.q[iEVNT] > 0 ? PDG_K_PLUS : PDG_K_MINUS),
                  leading} {}
};
template <class Reader> struct proton_candidate : charged_candidate<Reader> {
  using candidate_base_type = charged_candidate<Reader>;
  proton_candidate(Reader& r, const size_t iEVNT,
                   const clas_particle& leading = {})
      : candidate_base_type{r, iEVNT, (r.evnt.q[iEVNT] > 0 ? PDG_PROTON : PDG_ANTIPROTON),
                  leading} {}
};

// =============================================================================
// IC Photon candidate
// =============================================================================
template <class Reader>
struct ic_photon_candidate : clas_particle, candidate<Reader> {
  using candidate_base_type = candidate<Reader>;

  ic_photon_candidate(Reader& r, const size_t iIC, const clas_particle& leading)
      : candidate_base_type(r, iIC, PDG_ELECTRON) {
    // calculate the photon direction
    TVector3 dir{r.ic.xc[iIC], r.ic.yc[iIC], 0};
    dir -= leading.vertex;
    // properly initialize the underlying clas_particle
    // note: getting dt relative to electron dt (time)
    static_cast<clas_particle&>(*this) =
        clas_particle{leading.run,
                      leading.event,
                      {PDG_PHOTON, r.ic.etc[iIC] * dir.Unit()},
                      leading.vertex,
                      r.ic.tc[iIC] - dir.Mag() / constants::c_cm_ns,
                      true};
    dt = t - leading.t;
  }
};

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
