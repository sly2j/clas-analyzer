#include "eg1dvcs/specs/fiducial.hh"

#include <analyzer/core/assert.hh>
#include <analyzer/core/math.hh>
#include <string>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

namespace {
//==============================================================================
// Implementation of the EG1-DVCS fiducial cuts; mostly copy-paste from Yelena's
// fiducial.C
//==============================================================================
namespace fiducial_impl {
//==============================================================================
// necessary constants
//==============================================================================
constexpr const size_t PTBINS{6};
constexpr const size_t SECTORS{6};
constexpr const size_t PLBINS{6};
constexpr const size_t POBINS{6};
enum class param_set { SET1, SET2 };
//==============================================================================
// Cut parameters from site/www/html/Hall-B/secure/eg1-dvcs/yprok/fiducial/
//
// Hardcoded from now, should get their own configuration file TODO
//==============================================================================
struct tight_params {
  float atpar, btpar, ctpar, dtpar, etpar, ftpar;
  float t1tpar, t2tpar;
  float mt, yint, x1t, x2t;
};
struct loose_params {
  float alpar, blpar, clpar, dlpar, elpar, flpar;
  float t1lpar, t2lpar;
};
struct outbending_params {
  float aopar, bopar, copar, dopar, eopar, fopar;
  float gupar, hupar;
  float glpar, hlpar;
  float jpar;
};
constexpr const tight_params tight_params_set1[PTBINS][SECTORS]{
    {{28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 7.5, 16.72, 0.06, 34., 40., 0., 0., 0., 0.}},
    {{30., 0.30, 0.30, 10.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 10.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 10.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 10.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 11.2, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 11.2, 16.72, 0.06, 32., 40., 0., 0., 0., 0.}},
    {{35., 0.29, 0.30, 11.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.29, 0.30, 12., 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.29, 0.30, 12., 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.29, 0.30, 11.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.29, 0.30, 12., 16.72, 0.06, 32., 40., 7.62, -119., 18.5094, 21.1319},
     {35., 0.29, 0.30, 12., 16.72, 0.06, 32., 40., -10.2, 226., 20.2905, 18.4487}},
    {{35., 0.28, 0.30, 12.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 12., 16.72, 0.06, 32., 40., 28.3, -481., 17.8373, 18.3227},
     {35., 0.28, 0.30, 12., 16.72, 0.06, 32., 40., -14.6, 296., 18.8065, 17.802},
     {35., 0.28, 0.30, 12.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 12., 16.72, 0.06, 32., 40., 10.5, -177., 18.8835, 20.8241},
     {35., 0.28, 0.30, 12., 16.72, 0.06, 32., 40., -8.5, 197, 21.0504, 18.6705}},
    {{38., 0.28, 0.30, 13., 16.72, 0.06, 30., 40., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 12., 16.72, 0.06, 30., 40., 12.5, -198., 17.7631, 18.9476},
     {38., 0.28, 0.30, 12., 16.72, 0.06, 30., 40., -17.5, 350., 18.7718, 17.917},
     {38., 0.28, 0.30, 13., 16.72, 0.06, 30., 40., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 12., 16.72, 0.06, 30., 40., 9.44, -154, 18.6227, 20.711},
     {38., 0.28, 0.30, 12., 16.72, 0.06, 30., 40., -9.33, 215., 21.1152, 18.8918}},
    {{40., 0.25, 0.35, 13.8, 16.72, 0.06, 26., 40., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 13., 16.72, 0.06, 26., 40., 17.1, -278., 17.6801, 18.4723},
     {40., 0.25, 0.35, 13., 16.72, 0.06, 26., 40., -23., 443., 18.2795, 17.706},
     {40., 0.25, 0.35, 13.8, 16.72, 0.06, 26., 40., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 13., 16.72, 0.06, 26., 40., 10., -163., 18.5051, 20.4127},
     {40., 0.25, 0.35, 13., 16.72, 0.06, 26., 40., -10.7, 241., 20.8067, 18.9012}}};
constexpr const tight_params tight_params_set2[PTBINS][SECTORS]{
    {{30., 0.30, 0.30, 6.5, 16.72, 0.06, 34., 45., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 6.5, 16.72, 0.06, 34., 45., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 6.5, 16.72, 0.06, 34., 45., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 6.5, 16.72, 0.06, 34., 45., 0., 0., 0., 0.},
     {28., 0.30, 0.30, 6.2, 16.72, 0.06, 34., 45., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 6.2, 16.72, 0.06, 34., 45., 0., 0., 0., 0.}},
    {{30., 0.30, 0.30, 8.5, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 8.5, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 8.5, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 8.5, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 8.8, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.30, 0.30, 8.8, 16.72, 0.06, 32., 44., 0., 0., 0., 0.}},
    {{30., 0.29, 0.30, 9.0, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {32., 0.29, 0.30, 9.0, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {32., 0.29, 0.30, 9.0, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {32., 0.29, 0.30, 9.5, 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {30., 0.25, 0.25, 9.5, 16.72, 0.06, 32., 40., 0., 0., 0., 0.},
     {30., 0.25, 0.25, 9.5, 16.72, 0.06, 32., 42., 0., 0., 0., 0.}},
    {{35., 0.28, 0.30, 11., 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 11., 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 11., 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 11., 16.72, 0.06, 32., 44., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 11., 16.72, 0.06, 32., 42., 0., 0., 0., 0.},
     {35., 0.28, 0.30, 11., 16.72, 0.06, 32., 42., 0., 0., 0., 0.}},
    {{38., 0.28, 0.30, 11., 16.72, 0.06, 30., 44., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 11., 16.72, 0.06, 30., 44., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 11., 16.72, 0.06, 30., 44., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 11., 16.72, 0.06, 30., 44., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 11.3, 16.72, 0.06, 30., 44., 0., 0., 0., 0.},
     {38., 0.28, 0.30, 11.3, 16.72, 0.06, 30., 40., 0., 0., 0., 0.}},
    {{40., 0.25, 0.35, 12., 16.72, 0.06, 26., 48., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 12., 16.72, 0.06, 26., 48., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 11.5, 16.72, 0.06, 26., 48., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 11.5, 16.72, 0.06, 26., 48., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 12., 16.72, 0.06, 26., 48., 0., 0., 0., 0.},
     {40., 0.25, 0.35, 12.3, 16.72, 0.06, 26., 44., 0., 0., 0., 0.}}};
constexpr const loose_params loose_params_set1[PLBINS]{
    {35., 0.32, 0.35, 6., 16.72, 0.06, 34., 40.},
    {36., 0.28, 0.30, 9., 16.72, 0.06, 32., 40.},
    {36., 0.28, 0.30, 10., 16.72, 0.06, 32., 40.},
    {38., 0.25, 0.30, 11., 16.72, 0.06, 32., 40.},
    {44.0, 0.25, 0.35, 11.5, 16.72, 0.06, 28.0, 40.0},
    {45., 0.25, 0.35, 12., 16.72, 0.06, 28., 40.}};
constexpr const loose_params loose_params_set2[PLBINS]{
    {37., 0.35, 0.30, 5., 16.72, 0.06, 34., 45.},
    {36., 0.26, 0.26, 8., 16.72, 0.06, 32., 45.},
    {36., 0.28, 0.30, 9., 16.72, 0.06, 32., 45.},
    {36., 0.25, 0.30, 10., 16.72, 0.06, 32., 45.},
    {44.0, 0.25, 0.35, 10., 16.72, 0.06, 28.0, 45.0},
    {45., 0.24, 0.35, 11., 16.72, 0.06, 28., 45.}};
constexpr const outbending_params outbending_params_set1[POBINS][SECTORS]{
    {{25., 0.35, 0.80, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 12., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 37.},
     {25., 0.20, 0.22, 12., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 37.}},
    {{25., 0.35, 0.85, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.},
     {25., 0.25, 0.22, 12., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.},
     {28., 0.28, 0.22, 12., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.},
     {25., 0.20, 0.22, 12., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 37.}},
    {{25., 0.35, 0.85, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 36.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {25., 0.20, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 36.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 34.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {28., 0.28, 0.22, 16., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 16., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.}}};
constexpr const outbending_params outbending_params_set2[POBINS][SECTORS]{
    {{25., 0.35, 0.80, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 37.},
     {25., 0.20, 0.22, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -5., 0.25, -1., 37.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 37.}},
    {{25., 0.35, 0.85, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.},
     {25., 0.25, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.},
     {25., 0.20, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.25, -0.09, 0.25, -0.09, 35.}},
    {{25., 0.35, 0.85, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {25., 0.20, 0.22, 13., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 35.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 34.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 34.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 33.},
     {28., 0.28, 0.22, 16., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 32.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 32.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 16., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.}},
    {{25., 0.35, 0.85, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {25., 0.20, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 14., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.},
     {28., 0.28, 0.22, 15., 3., 1.46, 0.15, -0.09, 0.15, -0.09, 30.}}};

//==============================================================================
// calc_pbin(float pel):
//    get the momentum bin number
// INPUT:
//  * pel: electron momentum
// RETURNS
//  * momentum bin number from 1->8, or 0 (out-of-range)
//==============================================================================
int calc_pbin(float pel) {
  // returns the bin number from 1 to 8
  int ip = 0; // ip=0 is when p<0.7 or p>6GeV

  constexpr static const float pmin[PTBINS] = {0.7, 1.0, 1.5, 2.0, 2.5, 3.0};
  constexpr static const float pmax[PTBINS] = {1.0, 1.5, 2.0, 2.5, 3.0, 6.0};
  // float pmin[PTBINS]= {0.7, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0};
  // float pmax[PTBINS]=  {     1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 6.0};
  for (int i = 0; i < PTBINS; i++) {
    if (pel >= pmin[i] && pel < pmax[i]) {
      ip = i + 1;
      break;
    }
  }
  return ip;
}
int tightfcut(float th_dc1, float phi_dc1, float pel, int ip, float itorus,
              int nsec, param_set set) {
  int wtight = 0;
  float ntpel;
  float phi_dc2;
  float dphi, th, thcut, thcutr, base, expon;
  float line[PTBINS][SECTORS];
  // for pi use M_PI from standard cmath
  //
  if (ip == 0) { // momentum is less than 0.7
    wtight = 0;
    return wtight;
  }
  ntpel = pel;
  if (ip == 1)
    ntpel = 0.8;
  if (ip > 6)
    ip = 6; // should not be greater than 6

  // set the parameterization pointers
  const tight_params& params{(set == param_set::SET1)
                                 ? tight_params_set1[ip - 1][nsec - 1]
                                 : tight_params_set2[ip - 1][nsec - 1]};

  thcut = params.dtpar +
          params.etpar / ((ntpel + params.ftpar) * 3375. / itorus);
  // cout<<nsec<<" "<<ip<<" "<<th_dc1<<" "<<phi_dc1<<" "<<pel<<" "<<thcut<<" ";
  thcutr = thcut / 360. * 2. * M_PI;
  if (th_dc1 > params.t1tpar && th_dc1 < params.t2tpar)
    th = params.t1tpar / 360. * 2. * M_PI;
  else
    th = th_dc1 / 360. * 2. * M_PI;

  expon = (params.btpar) *
          (pow((double)(ntpel * 3375. / itorus), (double)params.ctpar));
  base = fabs(sin(th - thcutr));
  dphi = params.atpar * (pow((double)base, (double)expon));
  line[ip - 1][nsec - 1] = params.mt * (th * 180. / M_PI) + params.yint;
  // cout<<th<<" "<<expon<<" "<<base<<" "<<dphi<<" ";

  // Yelena is a different phi definition which is +30 bigger and it is between
  // 0 and 360..
  phi_dc2 = phi_dc1 +
            30.; // shifting at 0 the lower limit phi of sector 1 (which is -30)
  if (phi_dc2 < 0)
    phi_dc2 = phi_dc2 + 360.; // making phi range from 0 to 360
  phi_dc2 =
      phi_dc2 -
      (nsec - 1) *
          60.; // converting absolute phi to the sector-phi  (from 0 to 60)

  if (nsec == 1 || nsec == 4) {
    if (th_dc1 <= thcut || th_dc1 >= params.t2tpar)
      wtight = 0;
    else if (th_dc1 > thcut && th_dc1 < params.t2tpar) {
      if (phi_dc2 < (dphi + 30.) && phi_dc2 > (30. - dphi))
        wtight = 1;
      else
        wtight = 0;
    } else {
      dphi = 0.;
      wtight = 0;
    }
  }
  if (nsec == 2 || nsec == 5) {
    if (th_dc1 < thcut || th_dc1 >= params.t2tpar)
      wtight = 0;
    else if (th_dc1 > params.x1t && th_dc1 < params.t2tpar) {
      if (phi_dc2 < (dphi + 30.) && phi_dc2 > (30. - dphi))
        wtight = 1;
      else
        wtight = 0;
    }
  }
  if (nsec == 3 || nsec == 6) {
    if (th_dc1 < thcut || th_dc1 >= params.t2tpar)
      wtight = 0;
    else if (th_dc1 > params.x2t && th_dc1 < params.t2tpar) {
      if (phi_dc2 < (dphi + 30.) && phi_dc2 > (30. - dphi))
        wtight = 1;
      else
        wtight = 0;
    }
  }
  // cout<<phi_dc2<<" "<<wtight<<endl;;
  return wtight;
}
int loosefcut(float th_dc1, float phi_dc1, float pel, int ip, float itorus,
              int nsec, param_set set) {
  int wloose = 0;
  float ntpel;
  float phi_dc2;
  float dphi, th, thcut, thcutr, base, expon;
  // for pi use M_PI from standard cmath
  //
  if (ip == 0) { // momentum is less than 0.7
    wloose = 0;
    return wloose;
  }
  ntpel = pel;
  if (ip > 6)
    ip = 6;
  // if(ip==0)ntpel=0.8;//is this needed????

  // set the parameterization pointers
  const loose_params* const params{(set == param_set::SET1)
                                       ? &loose_params_set1[ip - 1]
                                       : &loose_params_set2[ip - 1]};
  thcut = params->dlpar +
          params->elpar / ((ntpel + params->flpar) * 3375. / itorus);
  // cout<<nsec<<" "<<ip<<" "<<th_dc1<<" "<<phi_dc1<<" "<<pel<<" "<<thcut<<" ";
  thcutr = thcut / 360. * 2. * M_PI;
  if (th_dc1 > params->t1lpar && th_dc1 < params->t2lpar)
    th = params->t1lpar / 360. * 2. * M_PI;
  else
    th = th_dc1 / 360. * 2. * M_PI;

  expon = (params->blpar) *
          (pow((double)(ntpel * 3375. / itorus), (double)params->clpar));
  base = fabs(sin(th - thcutr));
  dphi = params->alpar * (pow((double)base, (double)expon));
  // cout<<th<<" "<<expon<<" "<<base<<" "<<dphi<<" ";

  // Yelena is a different phi definition which is +30 bigger and it is between
  // 0 and 360..
  phi_dc2 = phi_dc1 +
            30.; // shifting at 0 the lower limit phi of sector 1 (which is -30)
  if (phi_dc2 < 0)
    phi_dc2 = phi_dc2 + 360.; // making phi range from 0 to 360
  phi_dc2 =
      phi_dc2 -
      (nsec - 1) *
          60.; // converting absolute phi to the sector-phi  (from 0 to 60)

  if (th_dc1 <= thcut || th_dc1 >= params->t2lpar)
    wloose = 0;
  else if (th_dc1 > thcut && th_dc1 < params->t2lpar) {
    if (phi_dc2 < (dphi + 30.) && phi_dc2 > (30. - dphi))
      wloose = 1;
    else
      wloose = 0;
  } else {
    dphi = 0.;
    wloose = 0;
  }
  //std::cout << nsec << " " << phi_dc2 << " " << wloose << std::endl;
  return wloose;
}
int outbendingfcut(float th_dc1, float phi_dc1, float pel, int ip, float itorus,
                   int nsec, param_set set) {
  int woutbending = 0;
  float ntpel;
  float thhigh, thnom, pscale, phi_dc2;
  float dphi, th, thcut, thcutr, base, expon;
  float phicl, phich;
  // for pi use M_PI from standard cmath
  //
  ntpel = pel;
  if (ip > 6)
    ip = 6; // should not be greater than 6

  // set the parameterization pointers
  const outbending_params& params{
      (set == param_set::SET1) ? outbending_params_set1[ip - 1][nsec - 1]
                               : outbending_params_set2[ip - 1][nsec - 1]};

  thnom =
      params.jpar * pow((0.2 * (ntpel * 3375. / fabs(itorus) + 2.5)), 0.33333);
  //     min theta (degrees)
  pscale = ntpel * 1500. / fabs(itorus);
  thcut = params.dopar + params.eopar * (1 - 0.25 * pscale) * params.fopar;
  //     max theta (degrees)
  thhigh = fmin(40., thnom);

  thcutr = 6.5 / 360. * 2. * M_PI;
  th = th_dc1 / 360. * 2. * M_PI;
  expon =
      (params.bopar) * (pow((double)(ntpel * 0.25), (double)params.copar));
  base = fabs(sin(th - thcutr));
  dphi = params.aopar * (pow((double)base, (double)expon));
  // cout<<th<<" "<<expon<<" "<<base<<" "<<dphi<<" ";
  // cout<<params.aopar<<" "<<ip<<" "<<nsec<<" "<<base<<" "<<expon<<endl;

  phicl = params.glpar / (sin((th_dc1 + params.hlpar) / 180. * M_PI));
  phich = params.gupar / (sin((th_dc1 + params.hupar) / 180. * M_PI));

  // Yelena is a different phi definition which is +30 bigger and it is between
  // 0 and 360..
  phi_dc2 = phi_dc1 +
            30.; // shifting at 0 the lower limit phi of sector 1 (which is -30)
  if (phi_dc2 < 0)
    phi_dc2 = phi_dc2 + 360.; // making phi range from 0 to 360
  phi_dc2 =
      phi_dc2 -
      (nsec - 1) *
          60.; // converting absolute phi to the sector-phi  (from 0 to 60)
  // cout<<phich<<" "<<phicl<<" "<<phi_dc2<<" "<<dphi<<endl;
  if (th_dc1 >= thcut and th_dc1 < thhigh) {
    if (phi_dc2 < (dphi + 30.) && phi_dc2 > (phich + 30.))
      woutbending = 1;
    else if (phi_dc2 < (30. - phicl) && phi_dc2 > (30. - dphi))
      woutbending = 1;
    else
      woutbending = 0;
  } else {
    woutbending = 0;
  }

  // cout<<phi_dc2<<" "<<woutbending<<endl;;
  return woutbending;
}
} // ns fiducial_impl
} // unnamed namespace

bool apply_fiducial_cut(float th_dc1, float phi_dc1, float pel, float ctorus,
                        int nsec, int fiducial_group, bool loose) {
  th_dc1 = radian_to_degree(th_dc1);
  phi_dc1 = radian_to_degree(phi_dc1);
  if (phi_dc1 > 180.) {
    phi_dc1 -= 360.;
  }
  // std::cout << phi_dc1 << " " << th_dc1 << " " nsec << " " << fiducial_group
  // << " " << ctorus << " " << loose
  //          << std::endl;

  // get the momentum bin
  int ip = fiducial_impl::calc_pbin(pel);
  tassert(ip > 0 && ip <= 6,
          "Invalid electron momentum bin: " + std::to_string(ip) + " (" +
              std::to_string(pel) + "GeV)" +
              " ensure the electron momentum lies between 0.7GeV and 6GeV");
  // select inbending our outbending cuts depending on the fiducial group
  tassert(fiducial_group > 0 && fiducial_group <= 7,
          "Invalid number fiducial group number: " +
              std::to_string(fiducial_group));
  fiducial_impl::param_set set{
      (fiducial_group == 1 || fiducial_group == 2 || fiducial_group == 4)
          ? fiducial_impl::param_set::SET1
          : fiducial_impl::param_set::SET2};
  // use 1->6 sector index
  nsec += 1;
  tassert(nsec > 0 && nsec <= 6,
          "Invalid sector number: " + std::to_string(nsec));
  // do the actual cuts depending on the fiducial group and if we want loose
  // cuts
  if (fiducial_group == 4 || fiducial_group == 7) {
    return fiducial_impl::outbendingfcut(th_dc1, phi_dc1, pel, ip, ctorus, nsec,
                                         set);
  } else if (!loose) {
    return fiducial_impl::tightfcut(th_dc1, phi_dc1, pel, ip, ctorus, nsec,
                                    set);
  } else {
    return fiducial_impl::loosefcut(th_dc1, phi_dc1, pel, ip, ctorus, nsec,
                                    set);
  }
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer
