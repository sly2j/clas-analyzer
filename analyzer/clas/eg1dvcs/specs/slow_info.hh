#ifndef ANALYZER_CLAS_EG1DVCS_SLOW_INFO_LOADED
#define ANALYZER_CLAS_EG1DVCS_SLOW_INFO_LOADED

#include <tuple>
#include <TVector3.h>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// eg1dvcs target types
enum class target_type { EMPTY = 0, NH3, ND3, C12, OTHER };
enum class pol_info { NONE = 0, PLUS = 1, MINUS = -1 };

// basic run-level information
struct basic_slow_info {
  size_t runnb;                // run number
  pol_info target_pol;         // sign of the target polarization
  bool hwplate;                // Half-wave plate status (0: out, 1: in)
  target_type target_material; // target material

  constexpr basic_slow_info()
      : runnb{0}
      , target_pol{pol_info::NONE}
      , hwplate{0}
      , target_material{target_type::OTHER} {}
  constexpr basic_slow_info(size_t runnb, pol_info target_pol, bool hwplate,
                            target_type target_material)
      : runnb{runnb}
      , target_pol{target_pol}
      , hwplate{hwplate}
      , target_material{target_material} {}
};

// full hard-coded run-level information, initialized from just the run number
struct slow_info : basic_slow_info {
  bool good_run;        // is this a good run? includes raster status
  float beam_energy;    // beam energy in GeV
  float nominal_vertex; // nominal vetex position - center of target in cm
  int fiducial_group;   // group number for fiducial cuts
  float torus_current;  // torus current
  float beam_pol;       // beam polarization

  slow_info();
  explicit slow_info(size_t runnb);

  // Convert raw raster ADC readings to beam position relative to CLAS center
  // (in cm).
  // INPUT:
  //  * rastr1: raw x raster ADC reading
  //  * rastr2: raw y raster ADC reading
  // OUTPUT:
  //  A vector containing the current beam {x, y} position.
  //  The z-component of the vector is set to the nominal vertex position
  TVector3 beam_pos(int rastr1, int rastr2) const;

  // Find direction consines and z-vertex location using DC1 angles, taking into
  // account the raster position.
  // INPUT:
  //  * beam_pos: raster position vector (or rastr1, rastr2 to calculate
  //  beam_pos)
  //  * dc_dir: {cx, cy, cz} at DC1
  //  * dc_pos: {x, y, z} at DC1
  //  * p: particle momentum in GeV
  //  * q: particle charge (-1 or 1)
  std::pair<TVector3, float> reconstruct_vertex(const TVector3& beam_pos,
                                                const TVector3& dc_dir,
                                                const TVector3& dc_pos, float p,
                                                int q) const;
  std::pair<TVector3, float> reconstruct_vertex(int rastr1, int rastr2,
                                                const TVector3& dc_dir,
                                                const TVector3& dc_pos, float p,
                                                int q) const {
    return reconstruct_vertex(beam_pos(rastr1, rastr2), dc_dir, dc_pos, p, q);
  }
};

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
