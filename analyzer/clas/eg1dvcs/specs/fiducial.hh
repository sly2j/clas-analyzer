#ifndef ANALYZER_CLAS_EG1DVCS_SPECS_FIDUCIAL_LOADED
#define ANALYZER_CLAS_EG1DVCS_SPECS_FIDUCIAL_LOADED

namespace analyzer {
namespace clas {
namespace eg1dvcs {

//==============================================================================
// Standard EG1-DVCS electron fiducial cuts, with auto-selection between
// inbending and outbending settings
// PARAMETERS:
//  * th_dc1: theta at the drift chamber in radians
//  * phi_dc1: phi at the drift chamber in radians
//  * pel: electron momentum
//  * ctorus: torus current
//  * nsec: sector (1->6)
//  * fiducial_group: the fiducial group of the run we are examining
//  * loose: use loose instead of tight cuts for inbending tracks
// RETURNS:
//  * true if it passes the cuts, false otherwise
//==============================================================================
bool apply_fiducial_cut(float th_dc1, float phi_dc1, float pel, float ctorus,
                        int nsec, int fiducial_group, bool loose);

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
