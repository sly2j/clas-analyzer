#include "eg1dvcs/specs/slow_info.hh"

#include <analyzer/core/logger.hh>

#include <algorithm>
#include <array>
#include <string>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

namespace {
namespace slow_info_impl {
// full EG1DVCS run info based on /u/home/yprok/eg1c/runlists/runinfo.txt
constexpr const std::array<basic_slow_info, 1037> SLOW_INFO{
    {{58794, pol_info::PLUS, 0, target_type::NH3},
     {58795, pol_info::PLUS, 0, target_type::NH3},
     {58799, pol_info::PLUS, 0, target_type::NH3},
     {58800, pol_info::PLUS, 0, target_type::NH3},
     {58801, pol_info::PLUS, 0, target_type::NH3},
     {58802, pol_info::PLUS, 0, target_type::NH3},
     {58803, pol_info::PLUS, 0, target_type::NH3},
     {58805, pol_info::PLUS, 0, target_type::NH3},
     {58806, pol_info::PLUS, 0, target_type::NH3},
     {58807, pol_info::PLUS, 0, target_type::NH3},
     {58808, pol_info::PLUS, 0, target_type::NH3},
     {58809, pol_info::PLUS, 0, target_type::NH3},
     {58810, pol_info::PLUS, 0, target_type::NH3},
     {58813, pol_info::NONE, 0, target_type::C12},
     {58814, pol_info::NONE, 0, target_type::C12},
     {58830, pol_info::PLUS, 0, target_type::NH3},
     {58831, pol_info::PLUS, 0, target_type::NH3},
     {58832, pol_info::PLUS, 0, target_type::NH3},
     {58832, pol_info::PLUS, 0, target_type::NH3},
     {58833, pol_info::PLUS, 0, target_type::NH3},
     {58834, pol_info::PLUS, 0, target_type::NH3},
     {58835, pol_info::PLUS, 0, target_type::NH3},
     {58836, pol_info::PLUS, 0, target_type::NH3},
     {58837, pol_info::PLUS, 0, target_type::NH3},
     {58838, pol_info::PLUS, 0, target_type::NH3},
     {58839, pol_info::PLUS, 0, target_type::NH3},
     {58840, pol_info::PLUS, 0, target_type::NH3},
     {58904, pol_info::PLUS, 0, target_type::NH3},
     {58905, pol_info::PLUS, 0, target_type::NH3},
     {58906, pol_info::PLUS, 0, target_type::NH3},
     {58911, pol_info::PLUS, 0, target_type::NH3},
     {58916, pol_info::PLUS, 0, target_type::NH3},
     {58917, pol_info::PLUS, 0, target_type::NH3},
     {58918, pol_info::PLUS, 0, target_type::NH3},
     {58919, pol_info::PLUS, 0, target_type::NH3},
     {58920, pol_info::PLUS, 0, target_type::NH3},
     {58921, pol_info::PLUS, 0, target_type::NH3},
     {58922, pol_info::PLUS, 0, target_type::NH3},
     {58923, pol_info::PLUS, 0, target_type::NH3},
     {58924, pol_info::PLUS, 0, target_type::NH3},
     {58925, pol_info::PLUS, 0, target_type::NH3},
     {58926, pol_info::MINUS, 0, target_type::NH3},
     {58927, pol_info::MINUS, 0, target_type::NH3},
     {58928, pol_info::MINUS, 0, target_type::NH3},
     {58929, pol_info::MINUS, 0, target_type::NH3},
     {58936, pol_info::MINUS, 0, target_type::NH3},
     {58937, pol_info::MINUS, 0, target_type::NH3},
     {58938, pol_info::MINUS, 0, target_type::NH3},
     {58940, pol_info::MINUS, 0, target_type::NH3},
     {58941, pol_info::MINUS, 0, target_type::NH3},
     {58942, pol_info::MINUS, 0, target_type::NH3},
     {58943, pol_info::MINUS, 0, target_type::NH3},
     {58944, pol_info::MINUS, 0, target_type::NH3},
     {58945, pol_info::MINUS, 0, target_type::NH3},
     {58946, pol_info::MINUS, 0, target_type::NH3},
     {58947, pol_info::MINUS, 0, target_type::NH3},
     {58948, pol_info::MINUS, 0, target_type::NH3},
     {58951, pol_info::MINUS, 0, target_type::NH3},
     {58952, pol_info::MINUS, 0, target_type::NH3},
     {58953, pol_info::MINUS, 0, target_type::NH3},
     {58954, pol_info::MINUS, 0, target_type::NH3},
     {58955, pol_info::MINUS, 0, target_type::NH3},
     {58956, pol_info::MINUS, 0, target_type::NH3},
     {58958, pol_info::NONE, 0, target_type::C12},
     {58959, pol_info::NONE, 0, target_type::C12},
     {58968, pol_info::MINUS, 0, target_type::NH3},
     {58969, pol_info::MINUS, 0, target_type::NH3},
     {58970, pol_info::MINUS, 0, target_type::NH3},
     {58971, pol_info::MINUS, 0, target_type::NH3},
     {58972, pol_info::MINUS, 1, target_type::NH3},
     {58973, pol_info::MINUS, 1, target_type::NH3},
     {58974, pol_info::MINUS, 0, target_type::NH3},
     {58975, pol_info::MINUS, 0, target_type::NH3},
     {58976, pol_info::MINUS, 1, target_type::NH3},
     {58977, pol_info::MINUS, 1, target_type::NH3},
     {58978, pol_info::MINUS, 1, target_type::NH3},
     {58979, pol_info::MINUS, 1, target_type::NH3},
     {58981, pol_info::MINUS, 1, target_type::NH3},
     {58983, pol_info::MINUS, 1, target_type::NH3},
     {58984, pol_info::MINUS, 1, target_type::NH3},
     {58985, pol_info::MINUS, 1, target_type::NH3},
     {58986, pol_info::MINUS, 1, target_type::NH3},
     {58987, pol_info::MINUS, 1, target_type::NH3},
     {58988, pol_info::MINUS, 1, target_type::NH3},
     {58989, pol_info::NONE, 1, target_type::C12},
     {58990, pol_info::PLUS, 1, target_type::NH3},
     {58991, pol_info::PLUS, 1, target_type::NH3},
     {58992, pol_info::PLUS, 1, target_type::NH3},
     {58997, pol_info::PLUS, 1, target_type::NH3},
     {58998, pol_info::PLUS, 1, target_type::NH3},
     {58999, pol_info::PLUS, 1, target_type::NH3},
     {59000, pol_info::PLUS, 1, target_type::NH3},
     {59001, pol_info::PLUS, 1, target_type::NH3},
     {59002, pol_info::PLUS, 1, target_type::NH3},
     {59003, pol_info::PLUS, 1, target_type::NH3},
     {59004, pol_info::PLUS, 1, target_type::NH3},
     {59005, pol_info::PLUS, 1, target_type::NH3},
     {59006, pol_info::PLUS, 1, target_type::NH3},
     {59007, pol_info::PLUS, 1, target_type::NH3},
     {59008, pol_info::PLUS, 1, target_type::NH3},
     {59009, pol_info::PLUS, 1, target_type::NH3},
     {59010, pol_info::PLUS, 1, target_type::NH3},
     {59011, pol_info::PLUS, 1, target_type::NH3},
     {59012, pol_info::PLUS, 1, target_type::NH3},
     {59013, pol_info::PLUS, 1, target_type::NH3},
     {59014, pol_info::PLUS, 1, target_type::NH3},
     {59015, pol_info::PLUS, 1, target_type::NH3},
     {59016, pol_info::PLUS, 1, target_type::NH3},
     {59019, pol_info::PLUS, 1, target_type::NH3},
     {59020, pol_info::PLUS, 1, target_type::NH3},
     {59021, pol_info::PLUS, 1, target_type::NH3},
     {59022, pol_info::PLUS, 1, target_type::NH3},
     {59024, pol_info::PLUS, 1, target_type::NH3},
     {59025, pol_info::PLUS, 1, target_type::NH3},
     {59026, pol_info::PLUS, 1, target_type::NH3},
     {59027, pol_info::PLUS, 1, target_type::NH3},
     {59028, pol_info::PLUS, 1, target_type::NH3},
     {59029, pol_info::PLUS, 0, target_type::NH3},
     {59030, pol_info::PLUS, 0, target_type::NH3},
     {59032, pol_info::PLUS, 0, target_type::NH3},
     {59038, pol_info::PLUS, 0, target_type::NH3},
     {59039, pol_info::PLUS, 0, target_type::NH3},
     {59040, pol_info::PLUS, 0, target_type::NH3},
     {59041, pol_info::PLUS, 0, target_type::NH3},
     {59042, pol_info::PLUS, 0, target_type::NH3},
     {59043, pol_info::PLUS, 0, target_type::NH3},
     {59044, pol_info::NONE, 0, target_type::C12},
     {59045, pol_info::PLUS, 0, target_type::NH3},
     {59046, pol_info::PLUS, 0, target_type::NH3},
     {59047, pol_info::PLUS, 0, target_type::NH3},
     {59048, pol_info::PLUS, 0, target_type::NH3},
     {59049, pol_info::PLUS, 0, target_type::NH3},
     {59050, pol_info::PLUS, 0, target_type::NH3},
     {59051, pol_info::PLUS, 0, target_type::NH3},
     {59052, pol_info::PLUS, 0, target_type::NH3},
     {59053, pol_info::MINUS, 0, target_type::NH3},
     {59054, pol_info::MINUS, 0, target_type::NH3},
     {59055, pol_info::MINUS, 0, target_type::NH3},
     {59056, pol_info::MINUS, 0, target_type::NH3},
     {59057, pol_info::MINUS, 0, target_type::NH3},
     {59058, pol_info::MINUS, 0, target_type::NH3},
     {59059, pol_info::MINUS, 0, target_type::NH3},
     {59060, pol_info::MINUS, 0, target_type::NH3},
     {59071, pol_info::NONE, 0, target_type::C12},
     {59072, pol_info::MINUS, 0, target_type::NH3},
     {59073, pol_info::MINUS, 0, target_type::NH3},
     {59074, pol_info::MINUS, 0, target_type::NH3},
     {59075, pol_info::MINUS, 0, target_type::NH3},
     {59076, pol_info::MINUS, 0, target_type::NH3},
     {59077, pol_info::MINUS, 0, target_type::NH3},
     {59078, pol_info::MINUS, 0, target_type::NH3},
     {59079, pol_info::MINUS, 0, target_type::NH3},
     {59080, pol_info::MINUS, 0, target_type::NH3},
     {59081, pol_info::MINUS, 0, target_type::NH3},
     {59082, pol_info::MINUS, 0, target_type::NH3},
     {59083, pol_info::MINUS, 1, target_type::NH3},
     {59084, pol_info::MINUS, 1, target_type::NH3},
     {59085, pol_info::MINUS, 1, target_type::NH3},
     {59086, pol_info::MINUS, 1, target_type::NH3},
     {59087, pol_info::MINUS, 1, target_type::NH3},
     {59088, pol_info::MINUS, 1, target_type::NH3},
     {59089, pol_info::MINUS, 1, target_type::NH3},
     {59090, pol_info::MINUS, 1, target_type::NH3},
     {59092, pol_info::NONE, 1, target_type::C12},
     {59093, pol_info::MINUS, 1, target_type::NH3},
     {59095, pol_info::MINUS, 1, target_type::NH3},
     {59098, pol_info::MINUS, 1, target_type::NH3},
     {59099, pol_info::MINUS, 1, target_type::NH3},
     {59100, pol_info::MINUS, 1, target_type::NH3},
     {59101, pol_info::MINUS, 1, target_type::NH3},
     {59102, pol_info::MINUS, 1, target_type::NH3},
     {59103, pol_info::MINUS, 1, target_type::NH3},
     {59104, pol_info::MINUS, 1, target_type::NH3},
     {59114, pol_info::MINUS, 1, target_type::NH3},
     {59116, pol_info::MINUS, 1, target_type::NH3},
     {59117, pol_info::MINUS, 1, target_type::NH3},
     {59118, pol_info::MINUS, 1, target_type::NH3},
     {59119, pol_info::MINUS, 1, target_type::NH3},
     {59121, pol_info::MINUS, 1, target_type::NH3},
     {59122, pol_info::MINUS, 0, target_type::NH3},
     {59123, pol_info::MINUS, 0, target_type::NH3},
     {59125, pol_info::MINUS, 0, target_type::NH3},
     {59126, pol_info::MINUS, 0, target_type::NH3},
     {59127, pol_info::MINUS, 0, target_type::NH3},
     {59128, pol_info::PLUS, 0, target_type::NH3},
     {59130, pol_info::PLUS, 0, target_type::NH3},
     {59131, pol_info::NONE, 0, target_type::C12},
     {59132, pol_info::NONE, 0, target_type::C12},
     {59133, pol_info::NONE, 0, target_type::C12},
     {59134, pol_info::NONE, 0, target_type::C12},
     {59135, pol_info::NONE, 0, target_type::C12},
     {59136, pol_info::NONE, 0, target_type::C12},
     {59137, pol_info::PLUS, 1, target_type::NH3},
     {59138, pol_info::PLUS, 1, target_type::NH3},
     {59139, pol_info::PLUS, 1, target_type::NH3},
     {59140, pol_info::PLUS, 1, target_type::NH3},
     {59141, pol_info::PLUS, 1, target_type::NH3},
     {59142, pol_info::NONE, 0, target_type::EMPTY},
     {59143, pol_info::NONE, 0, target_type::C12},
     {59144, pol_info::NONE, 0, target_type::C12},
     {59145, pol_info::NONE, 0, target_type::C12},
     {59146, pol_info::NONE, 0, target_type::C12},
     {59147, pol_info::PLUS, 0, target_type::NH3},
     {59149, pol_info::PLUS, 0, target_type::NH3},
     {59152, pol_info::NONE, 0, target_type::C12},
     {59154, pol_info::NONE, 0, target_type::C12},
     {59155, pol_info::NONE, 0, target_type::C12},
     {59166, pol_info::PLUS, 0, target_type::NH3},
     {59167, pol_info::PLUS, 0, target_type::NH3},
     {59168, pol_info::PLUS, 0, target_type::NH3},
     {59169, pol_info::NONE, 0, target_type::C12},
     {59171, pol_info::NONE, 0, target_type::EMPTY},
     {59172, pol_info::NONE, 0, target_type::EMPTY},
     {59173, pol_info::NONE, 0, target_type::OTHER},
     {59174, pol_info::NONE, 0, target_type::OTHER},
     {59175, pol_info::NONE, 0, target_type::OTHER},
     {59176, pol_info::NONE, 0, target_type::OTHER},
     {59177, pol_info::NONE, 0, target_type::OTHER},
     {59178, pol_info::NONE, 0, target_type::C12},
     {59179, pol_info::NONE, 0, target_type::C12},
     {59180, pol_info::PLUS, 0, target_type::NH3},
     {59181, pol_info::PLUS, 0, target_type::NH3},
     {59182, pol_info::PLUS, 0, target_type::NH3},
     {59183, pol_info::PLUS, 0, target_type::NH3},
     {59184, pol_info::PLUS, 0, target_type::NH3},
     {59185, pol_info::PLUS, 0, target_type::NH3},
     {59186, pol_info::PLUS, 0, target_type::NH3},
     {59188, pol_info::PLUS, 0, target_type::NH3},
     {59189, pol_info::PLUS, 0, target_type::NH3},
     {59190, pol_info::NONE, 0, target_type::C12},
     {59191, pol_info::MINUS, 0, target_type::NH3},
     {59192, pol_info::MINUS, 0, target_type::NH3},
     {59193, pol_info::MINUS, 1, target_type::NH3},
     {59194, pol_info::MINUS, 1, target_type::NH3},
     {59195, pol_info::MINUS, 1, target_type::NH3},
     {59196, pol_info::MINUS, 1, target_type::NH3},
     {59197, pol_info::MINUS, 1, target_type::NH3},
     {59198, pol_info::MINUS, 1, target_type::NH3},
     {59199, pol_info::MINUS, 1, target_type::NH3},
     {59200, pol_info::MINUS, 1, target_type::NH3},
     {59201, pol_info::MINUS, 1, target_type::NH3},
     {59202, pol_info::MINUS, 1, target_type::NH3},
     {59203, pol_info::MINUS, 1, target_type::NH3},
     {59204, pol_info::MINUS, 1, target_type::NH3},
     {59205, pol_info::NONE, 1, target_type::C12},
     {59206, pol_info::NONE, 1, target_type::C12},
     {59212, pol_info::PLUS, 1, target_type::NH3},
     {59213, pol_info::PLUS, 1, target_type::NH3},
     {59214, pol_info::PLUS, 1, target_type::NH3},
     {59216, pol_info::PLUS, 1, target_type::NH3},
     {59217, pol_info::PLUS, 1, target_type::NH3},
     {59218, pol_info::PLUS, 1, target_type::NH3},
     {59219, pol_info::PLUS, 1, target_type::NH3},
     {59220, pol_info::NONE, 1, target_type::C12},
     {59424, pol_info::PLUS, 1, target_type::NH3},
     {59425, pol_info::PLUS, 1, target_type::NH3},
     {59429, pol_info::PLUS, 1, target_type::NH3},
     {59432, pol_info::PLUS, 1, target_type::NH3},
     {59445, pol_info::PLUS, 1, target_type::NH3},
     {59447, pol_info::PLUS, 1, target_type::NH3},
     {59456, pol_info::PLUS, 1, target_type::NH3},
     {59457, pol_info::PLUS, 1, target_type::NH3},
     {59458, pol_info::PLUS, 1, target_type::NH3},
     {59459, pol_info::PLUS, 1, target_type::NH3},
     {59461, pol_info::PLUS, 1, target_type::NH3},
     {59462, pol_info::PLUS, 1, target_type::NH3},
     {59463, pol_info::PLUS, 1, target_type::NH3},
     {59464, pol_info::PLUS, 1, target_type::NH3},
     {59465, pol_info::PLUS, 1, target_type::NH3},
     {59466, pol_info::PLUS, 1, target_type::NH3},
     {59468, pol_info::PLUS, 1, target_type::NH3},
     {59471, pol_info::PLUS, 1, target_type::NH3},
     {59472, pol_info::PLUS, 1, target_type::NH3},
     {59474, pol_info::PLUS, 1, target_type::NH3},
     {59475, pol_info::PLUS, 1, target_type::NH3},
     {59477, pol_info::PLUS, 1, target_type::NH3},
     {59481, pol_info::PLUS, 1, target_type::NH3},
     {59483, pol_info::PLUS, 1, target_type::NH3},
     {59484, pol_info::PLUS, 1, target_type::NH3},
     {59486, pol_info::PLUS, 1, target_type::NH3},
     {59487, pol_info::PLUS, 1, target_type::NH3},
     {59488, pol_info::PLUS, 1, target_type::NH3},
     {59492, pol_info::PLUS, 1, target_type::NH3},
     {59494, pol_info::NONE, 1, target_type::C12},
     {59497, pol_info::NONE, 1, target_type::EMPTY},
     {59498, pol_info::MINUS, 1, target_type::NH3},
     {59500, pol_info::MINUS, 0, target_type::NH3},
     {59501, pol_info::MINUS, 0, target_type::NH3},
     {59502, pol_info::MINUS, 0, target_type::NH3},
     {59504, pol_info::MINUS, 0, target_type::NH3},
     {59505, pol_info::MINUS, 0, target_type::NH3},
     {59506, pol_info::MINUS, 0, target_type::NH3},
     {59507, pol_info::MINUS, 0, target_type::NH3},
     {59510, pol_info::MINUS, 0, target_type::NH3},
     {59511, pol_info::MINUS, 0, target_type::NH3},
     {59512, pol_info::MINUS, 0, target_type::NH3},
     {59513, pol_info::MINUS, 0, target_type::NH3},
     {59514, pol_info::MINUS, 0, target_type::NH3},
     {59515, pol_info::MINUS, 0, target_type::NH3},
     {59516, pol_info::MINUS, 0, target_type::NH3},
     {59517, pol_info::MINUS, 0, target_type::NH3},
     {59518, pol_info::MINUS, 0, target_type::NH3},
     {59519, pol_info::MINUS, 0, target_type::NH3},
     {59520, pol_info::MINUS, 0, target_type::NH3},
     {59522, pol_info::MINUS, 1, target_type::NH3},
     {59523, pol_info::PLUS, 1, target_type::NH3},
     {59524, pol_info::PLUS, 1, target_type::NH3},
     {59525, pol_info::PLUS, 1, target_type::NH3},
     {59526, pol_info::PLUS, 1, target_type::NH3},
     {59527, pol_info::PLUS, 1, target_type::NH3},
     {59528, pol_info::PLUS, 1, target_type::NH3},
     {59529, pol_info::PLUS, 1, target_type::NH3},
     {59533, pol_info::NONE, 1, target_type::C12},
     {59534, pol_info::NONE, 1, target_type::C12},
     {59537, pol_info::MINUS, 1, target_type::NH3},
     {59538, pol_info::MINUS, 0, target_type::NH3},
     {59539, pol_info::MINUS, 0, target_type::NH3},
     {59541, pol_info::MINUS, 0, target_type::NH3},
     {59542, pol_info::MINUS, 0, target_type::NH3},
     {59543, pol_info::MINUS, 0, target_type::NH3},
     {59549, pol_info::NONE, 0, target_type::C12},
     {59551, pol_info::NONE, 0, target_type::C12},
     {59552, pol_info::PLUS, 0, target_type::NH3},
     {59553, pol_info::PLUS, 0, target_type::NH3},
     {59554, pol_info::PLUS, 0, target_type::NH3},
     {59555, pol_info::PLUS, 0, target_type::NH3},
     {59557, pol_info::PLUS, 0, target_type::NH3},
     {59558, pol_info::PLUS, 0, target_type::NH3},
     {59559, pol_info::PLUS, 0, target_type::NH3},
     {59561, pol_info::PLUS, 0, target_type::NH3},
     {59562, pol_info::PLUS, 0, target_type::NH3},
     {59563, pol_info::PLUS, 0, target_type::NH3},
     {59564, pol_info::PLUS, 0, target_type::NH3},
     {59565, pol_info::PLUS, 0, target_type::NH3},
     {59566, pol_info::PLUS, 0, target_type::NH3},
     {59567, pol_info::PLUS, 0, target_type::NH3},
     {59572, pol_info::PLUS, 0, target_type::NH3},
     {59573, pol_info::PLUS, 0, target_type::NH3},
     {59574, pol_info::PLUS, 0, target_type::NH3},
     {59576, pol_info::PLUS, 0, target_type::NH3},
     {59578, pol_info::PLUS, 0, target_type::NH3},
     {59579, pol_info::NONE, 0, target_type::C12},
     {59580, pol_info::NONE, 0, target_type::C12},
     {59581, pol_info::PLUS, 0, target_type::NH3},
     {59582, pol_info::PLUS, 0, target_type::NH3},
     {59584, pol_info::PLUS, 0, target_type::NH3},
     {59585, pol_info::PLUS, 0, target_type::NH3},
     {59586, pol_info::PLUS, 0, target_type::NH3},
     {59587, pol_info::PLUS, 0, target_type::NH3},
     {59588, pol_info::PLUS, 0, target_type::NH3},
     {59591, pol_info::PLUS, 0, target_type::NH3},
     {59592, pol_info::PLUS, 0, target_type::NH3},
     {59593, pol_info::PLUS, 0, target_type::NH3},
     {59594, pol_info::PLUS, 0, target_type::NH3},
     {59595, pol_info::PLUS, 0, target_type::NH3},
     {59596, pol_info::PLUS, 0, target_type::NH3},
     {59597, pol_info::PLUS, 0, target_type::NH3},
     {59598, pol_info::PLUS, 0, target_type::NH3},
     {59599, pol_info::PLUS, 0, target_type::NH3},
     {59600, pol_info::PLUS, 0, target_type::NH3},
     {59602, pol_info::PLUS, 0, target_type::NH3},
     {59603, pol_info::PLUS, 0, target_type::NH3},
     {59604, pol_info::PLUS, 0, target_type::NH3},
     {59609, pol_info::PLUS, 0, target_type::NH3},
     {59610, pol_info::PLUS, 0, target_type::NH3},
     {59611, pol_info::PLUS, 0, target_type::NH3},
     {59612, pol_info::PLUS, 0, target_type::NH3},
     {59613, pol_info::PLUS, 0, target_type::NH3},
     {59614, pol_info::PLUS, 0, target_type::NH3},
     {59617, pol_info::PLUS, 0, target_type::NH3},
     {59618, pol_info::PLUS, 0, target_type::NH3},
     {59619, pol_info::PLUS, 0, target_type::NH3},
     {59621, pol_info::PLUS, 0, target_type::NH3},
     {59622, pol_info::PLUS, 0, target_type::NH3},
     {59623, pol_info::PLUS, 0, target_type::NH3},
     {59624, pol_info::NONE, 0, target_type::C12},
     {59637, pol_info::MINUS, 0, target_type::NH3},
     {59638, pol_info::MINUS, 0, target_type::NH3},
     {59639, pol_info::MINUS, 0, target_type::NH3},
     {59640, pol_info::MINUS, 0, target_type::NH3},
     {59641, pol_info::MINUS, 0, target_type::NH3},
     {59642, pol_info::MINUS, 0, target_type::NH3},
     {59644, pol_info::MINUS, 0, target_type::NH3},
     {59645, pol_info::MINUS, 0, target_type::NH3},
     {59646, pol_info::MINUS, 0, target_type::NH3},
     {59647, pol_info::MINUS, 0, target_type::NH3},
     {59648, pol_info::MINUS, 0, target_type::NH3},
     {59650, pol_info::MINUS, 0, target_type::NH3},
     {59651, pol_info::MINUS, 0, target_type::NH3},
     {59653, pol_info::MINUS, 0, target_type::NH3},
     {59655, pol_info::MINUS, 0, target_type::NH3},
     {59656, pol_info::MINUS, 0, target_type::NH3},
     {59657, pol_info::MINUS, 0, target_type::NH3},
     {59659, pol_info::MINUS, 0, target_type::NH3},
     {59660, pol_info::MINUS, 0, target_type::NH3},
     {59662, pol_info::MINUS, 0, target_type::NH3},
     {59664, pol_info::MINUS, 0, target_type::NH3},
     {59666, pol_info::MINUS, 0, target_type::NH3},
     {59687, pol_info::NONE, 0, target_type::EMPTY},
     {59688, pol_info::NONE, 0, target_type::C12},
     {59692, pol_info::NONE, 0, target_type::C12},
     {59693, pol_info::MINUS, 0, target_type::NH3},
     {59694, pol_info::MINUS, 0, target_type::NH3},
     {59695, pol_info::MINUS, 0, target_type::NH3},
     {59696, pol_info::MINUS, 0, target_type::NH3},
     {59697, pol_info::MINUS, 0, target_type::NH3},
     {59698, pol_info::MINUS, 0, target_type::NH3},
     {59702, pol_info::MINUS, 0, target_type::NH3},
     {59703, pol_info::MINUS, 0, target_type::NH3},
     {59704, pol_info::MINUS, 0, target_type::NH3},
     {59705, pol_info::MINUS, 0, target_type::NH3},
     {59707, pol_info::MINUS, 1, target_type::NH3},
     {59709, pol_info::MINUS, 1, target_type::NH3},
     {59710, pol_info::MINUS, 1, target_type::NH3},
     {59711, pol_info::MINUS, 1, target_type::NH3},
     {59712, pol_info::MINUS, 1, target_type::NH3},
     {59714, pol_info::MINUS, 1, target_type::NH3},
     {59715, pol_info::MINUS, 1, target_type::NH3},
     {59716, pol_info::MINUS, 1, target_type::NH3},
     {59717, pol_info::MINUS, 1, target_type::NH3},
     {59718, pol_info::MINUS, 1, target_type::NH3},
     {59719, pol_info::MINUS, 1, target_type::NH3},
     {59720, pol_info::MINUS, 1, target_type::NH3},
     {59723, pol_info::MINUS, 1, target_type::NH3},
     {59724, pol_info::MINUS, 1, target_type::NH3},
     {59725, pol_info::MINUS, 1, target_type::NH3},
     {59726, pol_info::MINUS, 1, target_type::NH3},
     {59728, pol_info::MINUS, 1, target_type::NH3},
     {59729, pol_info::MINUS, 1, target_type::NH3},
     {59730, pol_info::MINUS, 1, target_type::NH3},
     {59732, pol_info::MINUS, 1, target_type::NH3},
     {59733, pol_info::MINUS, 1, target_type::NH3},
     {59734, pol_info::MINUS, 1, target_type::NH3},
     {59735, pol_info::MINUS, 1, target_type::NH3},
     {59736, pol_info::MINUS, 1, target_type::NH3},
     {59737, pol_info::MINUS, 1, target_type::NH3},
     {59738, pol_info::NONE, 1, target_type::C12},
     {59739, pol_info::NONE, 1, target_type::C12},
     {59740, pol_info::NONE, 1, target_type::C12},
     {59742, pol_info::NONE, 1, target_type::C12},
     {59743, pol_info::NONE, 1, target_type::C12},
     {59744, pol_info::NONE, 1, target_type::C12},
     {59745, pol_info::NONE, 1, target_type::C12},
     {59746, pol_info::NONE, 1, target_type::C12},
     {59747, pol_info::NONE, 1, target_type::C12},
     {59748, pol_info::NONE, 1, target_type::C12},
     {59749, pol_info::NONE, 1, target_type::C12},
     {59751, pol_info::PLUS, 1, target_type::NH3},
     {59752, pol_info::PLUS, 1, target_type::NH3},
     {59753, pol_info::PLUS, 1, target_type::NH3},
     {59754, pol_info::PLUS, 0, target_type::NH3},
     {59755, pol_info::PLUS, 0, target_type::NH3},
     {59756, pol_info::PLUS, 0, target_type::NH3},
     {59757, pol_info::PLUS, 0, target_type::NH3},
     {59758, pol_info::PLUS, 0, target_type::NH3},
     {59760, pol_info::PLUS, 0, target_type::NH3},
     {59761, pol_info::PLUS, 0, target_type::NH3},
     {59762, pol_info::PLUS, 0, target_type::NH3},
     {59763, pol_info::PLUS, 0, target_type::NH3},
     {59765, pol_info::PLUS, 0, target_type::NH3},
     {59768, pol_info::NONE, 0, target_type::EMPTY},
     {59769, pol_info::NONE, 0, target_type::C12},
     {59771, pol_info::NONE, 0, target_type::C12},
     {59772, pol_info::PLUS, 0, target_type::NH3},
     {59773, pol_info::PLUS, 0, target_type::NH3},
     {59774, pol_info::PLUS, 0, target_type::NH3},
     {59777, pol_info::PLUS, 0, target_type::NH3},
     {59778, pol_info::PLUS, 0, target_type::NH3},
     {59779, pol_info::PLUS, 0, target_type::NH3},
     {59781, pol_info::PLUS, 0, target_type::NH3},
     {59782, pol_info::PLUS, 0, target_type::NH3},
     {59783, pol_info::PLUS, 0, target_type::NH3},
     {59784, pol_info::PLUS, 0, target_type::NH3},
     {59786, pol_info::PLUS, 0, target_type::NH3},
     {59787, pol_info::PLUS, 0, target_type::NH3},
     {59788, pol_info::PLUS, 0, target_type::NH3},
     {59793, pol_info::MINUS, 0, target_type::NH3},
     {59794, pol_info::MINUS, 0, target_type::NH3},
     {59795, pol_info::MINUS, 0, target_type::NH3},
     {59797, pol_info::MINUS, 0, target_type::NH3},
     {59798, pol_info::MINUS, 0, target_type::NH3},
     {59799, pol_info::MINUS, 0, target_type::NH3},
     {59800, pol_info::MINUS, 0, target_type::NH3},
     {59802, pol_info::MINUS, 0, target_type::NH3},
     {59803, pol_info::MINUS, 0, target_type::NH3},
     {59804, pol_info::MINUS, 0, target_type::NH3},
     {59805, pol_info::MINUS, 0, target_type::NH3},
     {59807, pol_info::NONE, 0, target_type::C12},
     {59808, pol_info::NONE, 0, target_type::C12},
     {59809, pol_info::MINUS, 0, target_type::NH3},
     {59810, pol_info::MINUS, 0, target_type::NH3},
     {59811, pol_info::MINUS, 0, target_type::NH3},
     {59812, pol_info::MINUS, 0, target_type::NH3},
     {59814, pol_info::MINUS, 0, target_type::NH3},
     {59815, pol_info::MINUS, 0, target_type::NH3},
     {59816, pol_info::MINUS, 0, target_type::NH3},
     {59817, pol_info::MINUS, 0, target_type::NH3},
     {59819, pol_info::MINUS, 1, target_type::NH3},
     {59820, pol_info::MINUS, 1, target_type::NH3},
     {59821, pol_info::MINUS, 1, target_type::NH3},
     {59822, pol_info::MINUS, 1, target_type::NH3},
     {59824, pol_info::MINUS, 1, target_type::NH3},
     {59825, pol_info::MINUS, 1, target_type::NH3},
     {59826, pol_info::MINUS, 1, target_type::NH3},
     {59827, pol_info::MINUS, 1, target_type::NH3},
     {59829, pol_info::MINUS, 1, target_type::NH3},
     {59830, pol_info::MINUS, 1, target_type::NH3},
     {59831, pol_info::MINUS, 1, target_type::NH3},
     {59832, pol_info::MINUS, 1, target_type::NH3},
     {59834, pol_info::NONE, 1, target_type::C12},
     {59835, pol_info::NONE, 1, target_type::C12},
     {59836, pol_info::NONE, 1, target_type::C12},
     {59837, pol_info::MINUS, 1, target_type::NH3},
     {59839, pol_info::MINUS, 1, target_type::NH3},
     {59840, pol_info::MINUS, 1, target_type::NH3},
     {59841, pol_info::MINUS, 1, target_type::NH3},
     {59842, pol_info::MINUS, 1, target_type::NH3},
     {59844, pol_info::MINUS, 1, target_type::NH3},
     {59845, pol_info::MINUS, 1, target_type::NH3},
     {59846, pol_info::MINUS, 1, target_type::NH3},
     {59847, pol_info::MINUS, 1, target_type::NH3},
     {59849, pol_info::MINUS, 1, target_type::NH3},
     {59850, pol_info::MINUS, 1, target_type::NH3},
     {59851, pol_info::MINUS, 1, target_type::NH3},
     {59852, pol_info::MINUS, 1, target_type::NH3},
     {59853, pol_info::MINUS, 1, target_type::NH3},
     {59854, pol_info::MINUS, 1, target_type::NH3},
     {59855, pol_info::MINUS, 1, target_type::NH3},
     {59856, pol_info::MINUS, 1, target_type::NH3},
     {59857, pol_info::MINUS, 1, target_type::NH3},
     {59858, pol_info::MINUS, 1, target_type::NH3},
     {59859, pol_info::NONE, 1, target_type::EMPTY},
     {59861, pol_info::NONE, 1, target_type::C12},
     {59862, pol_info::NONE, 1, target_type::C12},
     {59863, pol_info::NONE, 1, target_type::C12},
     {59864, pol_info::NONE, 1, target_type::C12},
     {59866, pol_info::PLUS, 1, target_type::NH3},
     {59867, pol_info::PLUS, 1, target_type::NH3},
     {59868, pol_info::PLUS, 1, target_type::NH3},
     {59869, pol_info::PLUS, 1, target_type::NH3},
     {59870, pol_info::PLUS, 1, target_type::NH3},
     {59871, pol_info::PLUS, 1, target_type::NH3},
     {59873, pol_info::PLUS, 1, target_type::NH3},
     {59875, pol_info::PLUS, 1, target_type::NH3},
     {59876, pol_info::PLUS, 1, target_type::NH3},
     {59878, pol_info::PLUS, 1, target_type::NH3},
     {59879, pol_info::PLUS, 1, target_type::NH3},
     {59880, pol_info::PLUS, 1, target_type::NH3},
     {59881, pol_info::PLUS, 1, target_type::NH3},
     {59883, pol_info::PLUS, 1, target_type::NH3},
     {59884, pol_info::PLUS, 0, target_type::NH3},
     {59885, pol_info::PLUS, 0, target_type::NH3},
     {59886, pol_info::PLUS, 0, target_type::NH3},
     {59888, pol_info::PLUS, 0, target_type::NH3},
     {59889, pol_info::PLUS, 0, target_type::NH3},
     {59890, pol_info::PLUS, 0, target_type::NH3},
     {59891, pol_info::PLUS, 0, target_type::NH3},
     {59893, pol_info::PLUS, 0, target_type::NH3},
     {59896, pol_info::NONE, 0, target_type::C12},
     {59899, pol_info::PLUS, 0, target_type::NH3},
     {59900, pol_info::PLUS, 0, target_type::NH3},
     {59901, pol_info::PLUS, 0, target_type::NH3},
     {59904, pol_info::PLUS, 0, target_type::NH3},
     {59905, pol_info::PLUS, 0, target_type::NH3},
     {59906, pol_info::PLUS, 0, target_type::NH3},
     {59907, pol_info::PLUS, 0, target_type::NH3},
     {59909, pol_info::PLUS, 0, target_type::NH3},
     {59910, pol_info::PLUS, 0, target_type::NH3},
     {59911, pol_info::PLUS, 0, target_type::NH3},
     {59912, pol_info::PLUS, 1, target_type::NH3},
     {59914, pol_info::PLUS, 1, target_type::NH3},
     {59915, pol_info::PLUS, 1, target_type::NH3},
     {59916, pol_info::PLUS, 1, target_type::NH3},
     {59917, pol_info::PLUS, 1, target_type::NH3},
     {59919, pol_info::PLUS, 1, target_type::NH3},
     {59920, pol_info::PLUS, 1, target_type::NH3},
     {59921, pol_info::PLUS, 1, target_type::NH3},
     {59922, pol_info::PLUS, 1, target_type::NH3},
     {59923, pol_info::PLUS, 1, target_type::NH3},
     {59924, pol_info::PLUS, 1, target_type::NH3},
     {59925, pol_info::PLUS, 1, target_type::NH3},
     {59926, pol_info::PLUS, 1, target_type::NH3},
     {59927, pol_info::PLUS, 1, target_type::NH3},
     {59929, pol_info::PLUS, 1, target_type::NH3},
     {59930, pol_info::PLUS, 1, target_type::NH3},
     {59931, pol_info::PLUS, 1, target_type::NH3},
     {59932, pol_info::PLUS, 0, target_type::NH3},
     {59934, pol_info::PLUS, 0, target_type::NH3},
     {59935, pol_info::PLUS, 0, target_type::NH3},
     {59936, pol_info::PLUS, 0, target_type::NH3},
     {59937, pol_info::PLUS, 0, target_type::NH3},
     {59939, pol_info::PLUS, 0, target_type::NH3},
     {59940, pol_info::PLUS, 0, target_type::NH3},
     {59941, pol_info::PLUS, 0, target_type::NH3},
     {59942, pol_info::PLUS, 0, target_type::NH3},
     {59944, pol_info::NONE, 0, target_type::C12},
     {59947, pol_info::NONE, 0, target_type::C12},
     {59948, pol_info::NONE, 0, target_type::C12},
     {59949, pol_info::MINUS, 0, target_type::NH3},
     {59950, pol_info::MINUS, 0, target_type::NH3},
     {59951, pol_info::MINUS, 0, target_type::NH3},
     {59953, pol_info::MINUS, 0, target_type::NH3},
     {59954, pol_info::MINUS, 0, target_type::NH3},
     {59955, pol_info::MINUS, 1, target_type::NH3},
     {59956, pol_info::MINUS, 1, target_type::NH3},
     {59958, pol_info::MINUS, 1, target_type::NH3},
     {59959, pol_info::MINUS, 1, target_type::NH3},
     {59960, pol_info::MINUS, 1, target_type::NH3},
     {59961, pol_info::MINUS, 1, target_type::NH3},
     {59963, pol_info::MINUS, 1, target_type::NH3},
     {59964, pol_info::MINUS, 1, target_type::NH3},
     {59965, pol_info::MINUS, 1, target_type::NH3},
     {59966, pol_info::MINUS, 1, target_type::NH3},
     {59967, pol_info::MINUS, 1, target_type::NH3},
     {59968, pol_info::MINUS, 1, target_type::NH3},
     {59969, pol_info::MINUS, 1, target_type::NH3},
     {59970, pol_info::MINUS, 1, target_type::NH3},
     {59971, pol_info::MINUS, 1, target_type::NH3},
     {59972, pol_info::MINUS, 1, target_type::NH3},
     {59973, pol_info::MINUS, 1, target_type::NH3},
     {59974, pol_info::MINUS, 1, target_type::NH3},
     {59976, pol_info::NONE, 1, target_type::C12},
     {59985, pol_info::MINUS, 1, target_type::NH3},
     {59986, pol_info::MINUS, 1, target_type::NH3},
     {59987, pol_info::MINUS, 1, target_type::NH3},
     {59989, pol_info::MINUS, 1, target_type::NH3},
     {59990, pol_info::MINUS, 1, target_type::NH3},
     {59991, pol_info::MINUS, 1, target_type::NH3},
     {59992, pol_info::MINUS, 1, target_type::NH3},
     {59993, pol_info::MINUS, 1, target_type::NH3},
     {59995, pol_info::MINUS, 1, target_type::NH3},
     {59996, pol_info::NONE, 1, target_type::C12},
     {59997, pol_info::NONE, 1, target_type::C12},
     {59998, pol_info::PLUS, 1, target_type::NH3},
     {60000, pol_info::PLUS, 1, target_type::NH3},
     {60001, pol_info::PLUS, 1, target_type::NH3},
     {60002, pol_info::PLUS, 1, target_type::NH3},
     {60003, pol_info::PLUS, 1, target_type::NH3},
     {60004, pol_info::PLUS, 1, target_type::NH3},
     {60005, pol_info::PLUS, 1, target_type::NH3},
     {60008, pol_info::PLUS, 0, target_type::NH3},
     {60009, pol_info::PLUS, 0, target_type::NH3},
     {60010, pol_info::PLUS, 0, target_type::NH3},
     {60011, pol_info::PLUS, 0, target_type::NH3},
     {60013, pol_info::PLUS, 0, target_type::NH3},
     {60014, pol_info::PLUS, 0, target_type::NH3},
     {60015, pol_info::PLUS, 0, target_type::NH3},
     {60016, pol_info::PLUS, 0, target_type::NH3},
     {60018, pol_info::PLUS, 0, target_type::NH3},
     {60019, pol_info::PLUS, 0, target_type::NH3},
     {60020, pol_info::NONE, 0, target_type::EMPTY},
     {60021, pol_info::NONE, 0, target_type::OTHER},
     {60023, pol_info::NONE, 0, target_type::OTHER},
     {60024, pol_info::NONE, 0, target_type::OTHER},
     {60025, pol_info::NONE, 0, target_type::C12},
     {60026, pol_info::NONE, 0, target_type::C12},
     {60028, pol_info::NONE, 0, target_type::C12},
     {60029, pol_info::MINUS, 0, target_type::NH3},
     {60030, pol_info::MINUS, 0, target_type::NH3},
     {60031, pol_info::MINUS, 0, target_type::NH3},
     {60032, pol_info::MINUS, 0, target_type::NH3},
     {60033, pol_info::MINUS, 0, target_type::NH3},
     {60034, pol_info::MINUS, 0, target_type::NH3},
     {60035, pol_info::MINUS, 0, target_type::NH3},
     {60038, pol_info::MINUS, 0, target_type::NH3},
     {60039, pol_info::MINUS, 0, target_type::NH3},
     {60041, pol_info::NONE, 0, target_type::C12},
     {60042, pol_info::MINUS, 0, target_type::NH3},
     {60043, pol_info::MINUS, 0, target_type::NH3},
     {60044, pol_info::MINUS, 0, target_type::NH3},
     {60047, pol_info::MINUS, 0, target_type::NH3},
     {60048, pol_info::MINUS, 0, target_type::NH3},
     {60049, pol_info::MINUS, 0, target_type::NH3},
     {60050, pol_info::MINUS, 0, target_type::NH3},
     {60052, pol_info::MINUS, 0, target_type::NH3},
     {60053, pol_info::MINUS, 0, target_type::NH3},
     {60054, pol_info::MINUS, 0, target_type::NH3},
     {60055, pol_info::MINUS, 0, target_type::NH3},
     {60057, pol_info::NONE, 0, target_type::C12},
     {60058, pol_info::NONE, 0, target_type::C12},
     {60059, pol_info::PLUS, 0, target_type::NH3},
     {60060, pol_info::PLUS, 0, target_type::NH3},
     {60062, pol_info::PLUS, 1, target_type::NH3},
     {60063, pol_info::PLUS, 1, target_type::NH3},
     {60064, pol_info::PLUS, 1, target_type::NH3},
     {60065, pol_info::PLUS, 1, target_type::NH3},
     {60069, pol_info::PLUS, 1, target_type::NH3},
     {60070, pol_info::PLUS, 1, target_type::NH3},
     {60072, pol_info::PLUS, 1, target_type::NH3},
     {60073, pol_info::PLUS, 1, target_type::NH3},
     {60074, pol_info::PLUS, 1, target_type::NH3},
     {60075, pol_info::PLUS, 1, target_type::NH3},
     {60076, pol_info::PLUS, 1, target_type::NH3},
     {60078, pol_info::PLUS, 1, target_type::NH3},
     {60079, pol_info::PLUS, 1, target_type::NH3},
     {60080, pol_info::NONE, 1, target_type::C12},
     {60081, pol_info::NONE, 1, target_type::C12},
     {60084, pol_info::MINUS, 1, target_type::NH3},
     {60085, pol_info::MINUS, 1, target_type::NH3},
     {60086, pol_info::MINUS, 1, target_type::NH3},
     {60087, pol_info::MINUS, 1, target_type::NH3},
     {60089, pol_info::MINUS, 1, target_type::NH3},
     {60090, pol_info::MINUS, 1, target_type::NH3},
     {60091, pol_info::NONE, 1, target_type::C12},
     {60092, pol_info::NONE, 1, target_type::C12},
     {60093, pol_info::MINUS, 0, target_type::NH3},
     {60094, pol_info::MINUS, 0, target_type::NH3},
     {60097, pol_info::MINUS, 0, target_type::NH3},
     {60098, pol_info::MINUS, 0, target_type::NH3},
     {60099, pol_info::MINUS, 1, target_type::NH3},
     {60100, pol_info::MINUS, 1, target_type::NH3},
     {60101, pol_info::MINUS, 1, target_type::NH3},
     {60103, pol_info::MINUS, 1, target_type::NH3},
     {60104, pol_info::MINUS, 1, target_type::NH3},
     {60105, pol_info::MINUS, 1, target_type::NH3},
     {60106, pol_info::MINUS, 1, target_type::NH3},
     {60110, pol_info::NONE, 1, target_type::EMPTY},
     {60111, pol_info::NONE, 1, target_type::EMPTY},
     {60112, pol_info::PLUS, 1, target_type::NH3},
     {60113, pol_info::PLUS, 1, target_type::NH3},
     {60115, pol_info::PLUS, 1, target_type::NH3},
     {60116, pol_info::PLUS, 1, target_type::NH3},
     {60117, pol_info::PLUS, 1, target_type::NH3},
     {60118, pol_info::PLUS, 1, target_type::NH3},
     {60119, pol_info::PLUS, 1, target_type::NH3},
     {60124, pol_info::MINUS, 1, target_type::NH3},
     {60125, pol_info::MINUS, 1, target_type::NH3},
     {60127, pol_info::MINUS, 1, target_type::NH3},
     {60129, pol_info::MINUS, 1, target_type::NH3},
     {60130, pol_info::MINUS, 1, target_type::NH3},
     {60131, pol_info::MINUS, 1, target_type::NH3},
     {60132, pol_info::NONE, 1, target_type::C12},
     {60133, pol_info::NONE, 1, target_type::C12},
     {60134, pol_info::NONE, 1, target_type::C12},
     {60135, pol_info::MINUS, 1, target_type::NH3},
     {60136, pol_info::MINUS, 1, target_type::NH3},
     {60137, pol_info::MINUS, 1, target_type::NH3},
     {60138, pol_info::MINUS, 1, target_type::NH3},
     {60141, pol_info::MINUS, 0, target_type::NH3},
     {60142, pol_info::MINUS, 0, target_type::NH3},
     {60143, pol_info::MINUS, 0, target_type::NH3},
     {60144, pol_info::MINUS, 0, target_type::NH3},
     {60146, pol_info::MINUS, 0, target_type::NH3},
     {60147, pol_info::MINUS, 0, target_type::NH3},
     {60153, pol_info::MINUS, 0, target_type::NH3},
     {60154, pol_info::NONE, 0, target_type::C12},
     {60155, pol_info::NONE, 0, target_type::C12},
     {60156, pol_info::NONE, 0, target_type::EMPTY},
     {60158, pol_info::PLUS, 0, target_type::NH3},
     {60159, pol_info::PLUS, 0, target_type::NH3},
     {60160, pol_info::PLUS, 0, target_type::NH3},
     {60161, pol_info::PLUS, 1, target_type::NH3},
     {60163, pol_info::PLUS, 1, target_type::NH3},
     {60164, pol_info::PLUS, 1, target_type::NH3},
     {60165, pol_info::PLUS, 1, target_type::NH3},
     {60166, pol_info::PLUS, 1, target_type::NH3},
     {60168, pol_info::PLUS, 1, target_type::NH3},
     {60169, pol_info::PLUS, 1, target_type::NH3},
     {60170, pol_info::PLUS, 1, target_type::NH3},
     {60171, pol_info::PLUS, 1, target_type::NH3},
     {60173, pol_info::PLUS, 1, target_type::NH3},
     {60174, pol_info::PLUS, 1, target_type::NH3},
     {60175, pol_info::PLUS, 1, target_type::NH3},
     {60176, pol_info::PLUS, 1, target_type::NH3},
     {60177, pol_info::PLUS, 1, target_type::NH3},
     {60179, pol_info::NONE, 1, target_type::C12},
     {60180, pol_info::NONE, 1, target_type::C12},
     {60181, pol_info::NONE, 1, target_type::C12},
     {60182, pol_info::NONE, 1, target_type::C12},
     {60184, pol_info::NONE, 1, target_type::C12},
     {60247, pol_info::NONE, 1, target_type::C12},
     {60248, pol_info::PLUS, 1, target_type::NH3},
     {60250, pol_info::PLUS, 1, target_type::NH3},
     {60300, pol_info::PLUS, 1, target_type::ND3},
     {60301, pol_info::PLUS, 1, target_type::ND3},
     {60302, pol_info::PLUS, 1, target_type::ND3},
     {60304, pol_info::PLUS, 1, target_type::ND3},
     {60305, pol_info::PLUS, 1, target_type::ND3},
     {60306, pol_info::PLUS, 1, target_type::ND3},
     {60307, pol_info::PLUS, 1, target_type::ND3},
     {60309, pol_info::PLUS, 1, target_type::ND3},
     {60311, pol_info::PLUS, 1, target_type::ND3},
     {60314, pol_info::PLUS, 1, target_type::ND3},
     {60316, pol_info::PLUS, 1, target_type::ND3},
     {60317, pol_info::PLUS, 1, target_type::ND3},
     {60319, pol_info::PLUS, 1, target_type::ND3},
     {60320, pol_info::PLUS, 1, target_type::ND3},
     {60322, pol_info::PLUS, 1, target_type::ND3},
     {60324, pol_info::PLUS, 1, target_type::ND3},
     {60325, pol_info::PLUS, 1, target_type::ND3},
     {60326, pol_info::PLUS, 1, target_type::ND3},
     {60327, pol_info::PLUS, 1, target_type::ND3},
     {60330, pol_info::PLUS, 1, target_type::ND3},
     {60331, pol_info::PLUS, 1, target_type::ND3},
     {60332, pol_info::PLUS, 1, target_type::ND3},
     {60333, pol_info::PLUS, 1, target_type::ND3},
     {60337, pol_info::NONE, 1, target_type::EMPTY},
     {60338, pol_info::NONE, 1, target_type::C12},
     {60340, pol_info::NONE, 1, target_type::C12},
     {60342, pol_info::NONE, 1, target_type::C12},
     {60343, pol_info::PLUS, 1, target_type::ND3},
     {60344, pol_info::PLUS, 1, target_type::ND3},
     {60345, pol_info::PLUS, 1, target_type::ND3},
     {60346, pol_info::PLUS, 1, target_type::ND3},
     {60347, pol_info::PLUS, 1, target_type::ND3},
     {60348, pol_info::PLUS, 1, target_type::ND3},
     {60349, pol_info::PLUS, 1, target_type::ND3},
     {60350, pol_info::PLUS, 1, target_type::ND3},
     {60351, pol_info::PLUS, 1, target_type::ND3},
     {60354, pol_info::PLUS, 1, target_type::ND3},
     {60355, pol_info::PLUS, 1, target_type::ND3},
     {60356, pol_info::PLUS, 1, target_type::ND3},
     {60357, pol_info::PLUS, 1, target_type::ND3},
     {60359, pol_info::PLUS, 1, target_type::ND3},
     {60360, pol_info::PLUS, 1, target_type::ND3},
     {60361, pol_info::PLUS, 1, target_type::ND3},
     {60363, pol_info::MINUS, 1, target_type::ND3},
     {60364, pol_info::MINUS, 1, target_type::ND3},
     {60366, pol_info::MINUS, 1, target_type::ND3},
     {60367, pol_info::MINUS, 1, target_type::ND3},
     {60369, pol_info::MINUS, 1, target_type::ND3},
     {60371, pol_info::MINUS, 1, target_type::ND3},
     {60372, pol_info::MINUS, 1, target_type::ND3},
     {60373, pol_info::MINUS, 1, target_type::ND3},
     {60374, pol_info::MINUS, 1, target_type::ND3},
     {60376, pol_info::MINUS, 1, target_type::ND3},
     {60377, pol_info::MINUS, 1, target_type::ND3},
     {60378, pol_info::MINUS, 1, target_type::ND3},
     {60379, pol_info::MINUS, 1, target_type::ND3},
     {60381, pol_info::MINUS, 1, target_type::ND3},
     {60382, pol_info::MINUS, 1, target_type::ND3},
     {60383, pol_info::MINUS, 1, target_type::ND3},
     {60384, pol_info::MINUS, 1, target_type::ND3},
     {60385, pol_info::MINUS, 1, target_type::ND3},
     {60387, pol_info::MINUS, 1, target_type::ND3},
     {60388, pol_info::MINUS, 1, target_type::ND3},
     {60389, pol_info::MINUS, 1, target_type::ND3},
     {60390, pol_info::MINUS, 1, target_type::ND3},
     {60392, pol_info::MINUS, 1, target_type::ND3},
     {60393, pol_info::MINUS, 1, target_type::NH3},
     {60394, pol_info::MINUS, 1, target_type::NH3},
     {60395, pol_info::MINUS, 1, target_type::NH3},
     {60396, pol_info::MINUS, 1, target_type::NH3},
     {60397, pol_info::MINUS, 1, target_type::NH3},
     {60399, pol_info::NONE, 0, target_type::EMPTY},
     {60400, pol_info::NONE, 0, target_type::C12},
     {60401, pol_info::NONE, 0, target_type::C12},
     {60402, pol_info::NONE, 0, target_type::C12},
     {60404, pol_info::NONE, 0, target_type::C12},
     {60405, pol_info::NONE, 0, target_type::OTHER},
     {60407, pol_info::NONE, 0, target_type::OTHER},
     {60408, pol_info::NONE, 0, target_type::OTHER},
     {60411, pol_info::PLUS, 0, target_type::ND3},
     {60412, pol_info::PLUS, 0, target_type::ND3},
     {60413, pol_info::PLUS, 0, target_type::ND3},
     {60414, pol_info::PLUS, 0, target_type::ND3},
     {60416, pol_info::PLUS, 0, target_type::ND3},
     {60417, pol_info::PLUS, 0, target_type::ND3},
     {60418, pol_info::PLUS, 0, target_type::ND3},
     {60419, pol_info::PLUS, 0, target_type::ND3},
     {60421, pol_info::PLUS, 0, target_type::ND3},
     {60422, pol_info::PLUS, 0, target_type::ND3},
     {60423, pol_info::PLUS, 0, target_type::ND3},
     {60424, pol_info::PLUS, 0, target_type::ND3},
     {60425, pol_info::PLUS, 0, target_type::ND3},
     {60427, pol_info::PLUS, 0, target_type::ND3},
     {60428, pol_info::PLUS, 0, target_type::ND3},
     {60429, pol_info::PLUS, 0, target_type::ND3},
     {60430, pol_info::PLUS, 0, target_type::ND3},
     {60431, pol_info::PLUS, 0, target_type::ND3},
     {60432, pol_info::PLUS, 1, target_type::ND3},
     {60434, pol_info::NONE, 1, target_type::C12},
     {60435, pol_info::NONE, 1, target_type::C12},
     {60436, pol_info::PLUS, 1, target_type::ND3},
     {60437, pol_info::PLUS, 1, target_type::ND3},
     {60439, pol_info::PLUS, 1, target_type::ND3},
     {60440, pol_info::PLUS, 1, target_type::ND3},
     {60441, pol_info::PLUS, 1, target_type::ND3},
     {60442, pol_info::PLUS, 1, target_type::ND3},
     {60444, pol_info::PLUS, 0, target_type::ND3},
     {60445, pol_info::PLUS, 0, target_type::ND3},
     {60446, pol_info::PLUS, 0, target_type::ND3},
     {60447, pol_info::PLUS, 0, target_type::ND3},
     {60449, pol_info::PLUS, 0, target_type::ND3},
     {60450, pol_info::PLUS, 0, target_type::ND3},
     {60451, pol_info::PLUS, 0, target_type::ND3},
     {60452, pol_info::PLUS, 0, target_type::ND3},
     {60453, pol_info::PLUS, 0, target_type::ND3},
     {60454, pol_info::PLUS, 0, target_type::ND3},
     {60455, pol_info::NONE, 0, target_type::C12},
     {60456, pol_info::NONE, 0, target_type::C12},
     {60457, pol_info::NONE, 1, target_type::C12},
     {60458, pol_info::MINUS, 1, target_type::ND3},
     {60459, pol_info::MINUS, 1, target_type::ND3},
     {60460, pol_info::MINUS, 1, target_type::ND3},
     {60462, pol_info::MINUS, 1, target_type::ND3},
     {60463, pol_info::MINUS, 1, target_type::ND3},
     {60464, pol_info::MINUS, 1, target_type::ND3},
     {60466, pol_info::MINUS, 1, target_type::ND3},
     {60468, pol_info::MINUS, 1, target_type::ND3},
     {60469, pol_info::MINUS, 1, target_type::ND3},
     {60470, pol_info::MINUS, 1, target_type::ND3},
     {60471, pol_info::MINUS, 1, target_type::ND3},
     {60473, pol_info::MINUS, 1, target_type::ND3},
     {60474, pol_info::MINUS, 1, target_type::ND3},
     {60475, pol_info::MINUS, 1, target_type::ND3},
     {60476, pol_info::MINUS, 1, target_type::ND3},
     {60478, pol_info::MINUS, 1, target_type::ND3},
     {60479, pol_info::MINUS, 0, target_type::ND3},
     {60480, pol_info::MINUS, 0, target_type::ND3},
     {60481, pol_info::MINUS, 0, target_type::ND3},
     {60483, pol_info::MINUS, 0, target_type::ND3},
     {60484, pol_info::MINUS, 0, target_type::ND3},
     {60485, pol_info::MINUS, 0, target_type::ND3},
     {60486, pol_info::MINUS, 0, target_type::ND3},
     {60488, pol_info::NONE, 0, target_type::C12},
     {60489, pol_info::NONE, 0, target_type::C12},
     {60491, pol_info::NONE, 0, target_type::C12},
     {60492, pol_info::NONE, 0, target_type::C12},
     {60496, pol_info::MINUS, 0, target_type::ND3},
     {60497, pol_info::MINUS, 0, target_type::ND3},
     {60498, pol_info::MINUS, 0, target_type::ND3},
     {60499, pol_info::MINUS, 0, target_type::ND3},
     {60500, pol_info::MINUS, 0, target_type::ND3},
     {60501, pol_info::MINUS, 0, target_type::ND3},
     {60502, pol_info::MINUS, 0, target_type::ND3},
     {60503, pol_info::MINUS, 0, target_type::ND3},
     {60504, pol_info::MINUS, 0, target_type::ND3},
     {60505, pol_info::MINUS, 0, target_type::ND3},
     {60507, pol_info::MINUS, 0, target_type::ND3},
     {60508, pol_info::MINUS, 0, target_type::ND3},
     {60509, pol_info::MINUS, 0, target_type::ND3},
     {60510, pol_info::NONE, 0, target_type::C12},
     {60512, pol_info::NONE, 0, target_type::C12},
     {60513, pol_info::NONE, 0, target_type::C12},
     {60514, pol_info::PLUS, 0, target_type::ND3},
     {60515, pol_info::PLUS, 0, target_type::ND3},
     {60517, pol_info::PLUS, 0, target_type::ND3},
     {60518, pol_info::PLUS, 0, target_type::ND3},
     {60519, pol_info::PLUS, 1, target_type::ND3},
     {60520, pol_info::PLUS, 1, target_type::ND3},
     {60522, pol_info::PLUS, 1, target_type::ND3},
     {60523, pol_info::PLUS, 1, target_type::ND3},
     {60524, pol_info::PLUS, 1, target_type::ND3},
     {60525, pol_info::PLUS, 1, target_type::ND3},
     {60526, pol_info::PLUS, 1, target_type::ND3},
     {60527, pol_info::PLUS, 1, target_type::ND3},
     {60528, pol_info::PLUS, 1, target_type::ND3},
     {60530, pol_info::PLUS, 1, target_type::ND3},
     {60531, pol_info::PLUS, 1, target_type::ND3},
     {60532, pol_info::PLUS, 1, target_type::ND3},
     {60533, pol_info::PLUS, 1, target_type::ND3},
     {60534, pol_info::PLUS, 1, target_type::ND3},
     {60535, pol_info::PLUS, 1, target_type::ND3},
     {60537, pol_info::PLUS, 1, target_type::ND3},
     {60538, pol_info::PLUS, 1, target_type::ND3},
     {60539, pol_info::PLUS, 1, target_type::ND3},
     {60540, pol_info::PLUS, 1, target_type::NH3},
     {60541, pol_info::PLUS, 0, target_type::NH3},
     {60542, pol_info::NONE, 0, target_type::C12},
     {60544, pol_info::NONE, 0, target_type::C12},
     {60547, pol_info::NONE, 0, target_type::C12},
     {60553, pol_info::MINUS, 0, target_type::ND3},
     {60554, pol_info::MINUS, 0, target_type::ND3},
     {60557, pol_info::MINUS, 0, target_type::ND3},
     {60558, pol_info::MINUS, 0, target_type::ND3},
     {60559, pol_info::MINUS, 0, target_type::ND3},
     {60560, pol_info::MINUS, 0, target_type::ND3},
     {60561, pol_info::MINUS, 0, target_type::ND3},
     {60562, pol_info::MINUS, 0, target_type::ND3},
     {60563, pol_info::MINUS, 0, target_type::ND3},
     {60564, pol_info::MINUS, 0, target_type::ND3},
     {60565, pol_info::NONE, 0, target_type::C12},
     {60566, pol_info::MINUS, 0, target_type::ND3},
     {60567, pol_info::MINUS, 0, target_type::ND3},
     {60568, pol_info::MINUS, 1, target_type::ND3},
     {60569, pol_info::MINUS, 1, target_type::ND3},
     {60570, pol_info::MINUS, 1, target_type::ND3},
     {60572, pol_info::MINUS, 1, target_type::ND3},
     {60573, pol_info::MINUS, 1, target_type::ND3},
     {60574, pol_info::MINUS, 1, target_type::ND3},
     {60575, pol_info::MINUS, 1, target_type::NH3},
     {60577, pol_info::PLUS, 1, target_type::ND3},
     {60578, pol_info::PLUS, 1, target_type::ND3},
     {60579, pol_info::PLUS, 1, target_type::ND3},
     {60580, pol_info::PLUS, 1, target_type::ND3},
     {60582, pol_info::PLUS, 1, target_type::ND3},
     {60584, pol_info::PLUS, 1, target_type::ND3},
     {60585, pol_info::PLUS, 1, target_type::ND3},
     {60586, pol_info::PLUS, 0, target_type::ND3},
     {60588, pol_info::PLUS, 0, target_type::ND3},
     {60589, pol_info::PLUS, 0, target_type::ND3},
     {60590, pol_info::PLUS, 0, target_type::ND3},
     {60591, pol_info::PLUS, 0, target_type::ND3},
     {60592, pol_info::PLUS, 0, target_type::ND3},
     {60594, pol_info::PLUS, 0, target_type::ND3},
     {60595, pol_info::PLUS, 0, target_type::NH3},
     {60597, pol_info::PLUS, 0, target_type::NH3},
     {60598, pol_info::PLUS, 0, target_type::NH3},
     {60599, pol_info::PLUS, 0, target_type::NH3},
     {60600, pol_info::NONE, 0, target_type::C12},
     {60602, pol_info::NONE, 0, target_type::C12},
     {60603, pol_info::PLUS, 1, target_type::ND3},
     {60604, pol_info::PLUS, 1, target_type::ND3},
     {60605, pol_info::PLUS, 1, target_type::ND3},
     {60607, pol_info::PLUS, 1, target_type::ND3},
     {60608, pol_info::PLUS, 1, target_type::ND3},
     {60609, pol_info::PLUS, 1, target_type::ND3},
     {60610, pol_info::PLUS, 1, target_type::ND3},
     {60612, pol_info::PLUS, 1, target_type::ND3},
     {60613, pol_info::PLUS, 1, target_type::ND3},
     {60614, pol_info::PLUS, 1, target_type::ND3},
     {60615, pol_info::PLUS, 1, target_type::ND3},
     {60617, pol_info::NONE, 1, target_type::C12},
     {60618, pol_info::NONE, 1, target_type::C12},
     {60619, pol_info::MINUS, 1, target_type::ND3},
     {60620, pol_info::MINUS, 1, target_type::ND3},
     {60622, pol_info::MINUS, 0, target_type::ND3},
     {60623, pol_info::MINUS, 0, target_type::ND3},
     {60624, pol_info::MINUS, 0, target_type::ND3},
     {60625, pol_info::MINUS, 0, target_type::ND3},
     {60627, pol_info::MINUS, 0, target_type::ND3},
     {60628, pol_info::MINUS, 0, target_type::ND3},
     {60629, pol_info::MINUS, 0, target_type::ND3},
     {60633, pol_info::NONE, 0, target_type::C12},
     {60634, pol_info::NONE, 0, target_type::EMPTY},
     {60635, pol_info::NONE, 0, target_type::EMPTY},
     {60637, pol_info::NONE, 0, target_type::OTHER},
     {60638, pol_info::NONE, 0, target_type::OTHER},
     {60639, pol_info::NONE, 0, target_type::OTHER},
     {60640, pol_info::NONE, 0, target_type::OTHER},
     {60642, pol_info::MINUS, 0, target_type::ND3},
     {60643, pol_info::MINUS, 0, target_type::ND3},
     {60644, pol_info::MINUS, 0, target_type::ND3},
     {60645, pol_info::MINUS, 0, target_type::ND3},
     {60647, pol_info::MINUS, 1, target_type::ND3},
     {60648, pol_info::MINUS, 1, target_type::ND3}}};

const constexpr std::array<int, 54> BAD_RASTER{
    {58794, 58795, 58796, 58808, 58809, 58810, 58813, 58814, 58815, 58816,
     59418, 59445, 59481, 59495, 59498, 59500, 59580, 59873, 59504, 59910,
     60242, 60247, 60265, 60266, 60269, 60270, 60271, 60272, 60273, 60274,
     60275, 60276, 60277, 60278, 60279, 60280, 60281, 60282, 60283, 60284,
     60297, 60326, 60377, 60378, 60379, 60380, 60381, 60382, 60383, 60384,
     60630, 60632, 60638, 60462}};
// copy-paste from slow_info.cxx
float vertex(size_t run) {
  float vertex = -57.95;
  if (run >= 59161 && run < 59221)
    vertex = -57.95;
  else if (run >= 59221 && run < 60223)
    vertex = -67.97;
  else if (run >= 60223)
    vertex = -68.18;
  else
    vertex = -57.95;
  return vertex;
}
float ebeam(size_t run) {
  float ebeam;
  if (run < 59161)
    ebeam = 5.887;
  else if (run >= 59161 && run < 59221)
    ebeam = 4.730;
  else if (run >= 59221 && run < 60223)
    ebeam = 5.954;
  else if (run >= 60223)
    ebeam = 5.752;
  else
    ebeam = 0.;
  return ebeam;
}
int fcgr_assign(size_t run) {
  constexpr static const size_t N_FC_GROUPS{7};
  int fcgr = 0;
  size_t runmin[N_FC_GROUPS] = {58708, 59162, 59401, 59978, 60006, 60221, 60566};
  size_t runmax[N_FC_GROUPS] = {59161, 59220, 59977, 60005, 60184, 60565, 60648};
  // eliminate the group 59221 59400

  for (int ir = 0; ir < N_FC_GROUPS; ir++) {
    if (run >= runmin[ir] && run <= runmax[ir]) {
      fcgr = ir + 1; // Groups run from 1 to N_FC_GROUPS to be consisten with
                     // the fortran version
      break;
    } else
      fcgr = 0;
  }
  return fcgr;
}
float ctorus(size_t run) {
  float ctorus = 2250.;
  if (run >= 59978 && run <= 60005)
    ctorus = -2200.;
  return ctorus;
}
float beam_pola(size_t run) {
  //      get the absolute value of average beam polarization based on Hall B
  //      Moeller measurements
  float beam_pola = 0;
  constexpr static const size_t N_BP_GROUPS{19};
  size_t runmin[N_BP_GROUPS] = {58739, 58825, 58977, 59036, 59077, 59127, 59164,
                             59443, 59537, 59565, 59705, 59780, 59792, 59894,
                             59909, 59965, 60006, 60111, 60121};
  size_t runmax[N_BP_GROUPS] = {58825, 58977, 59036, 59077, 59127, 59164, 59443,
                             59537, 59565, 59705, 59780, 59792, 59894, 59909,
                             59965, 60006, 60111, 60121, 60184};
  float polplus[N_BP_GROUPS] = {88.70, 90.41, 90.82, 89.64, 90.60, 75.19, 90.60,
                                87.53, 81.43, 86.13, 89.93, 91.97, 81.55, 85.72,
                                84.57, 82.87, 88.53, 85.15, 85.85};
  float polminus[N_BP_GROUPS] = {
      80.49, 82.76, 87.04, 84.14, 79.09, 68.00, 84.25, 81.43, 82.14, 84.71,
      80.11, 86.25, 82.25, 80.59, 82.68, 87.54, 74.38, 83.99, 85.28};
  for (int ir = 0; ir < N_BP_GROUPS; ir++) {
    if (run >= runmin[ir] && run <= runmax[ir]) {
      beam_pola = (polplus[ir] + polminus[ir]) / 2.;
      break;
    } else
      beam_pola = 0;
  }
  return beam_pola;
}
// modified copy_paste from target.C
// EG1-DVCS experiment
// Function: find direction cosines at target using DC1
//           anlges, taking into account raster position
//           also finds vertex z position using DC1 info
//           and beam position from raster information
// Usage: First call beam_info routine to get xr and yr
//        Can be used to over-write cx,cy,cz,vz in ntuples
// Inputs: irun: run number (integer)
//         xr: beam position in x from raster in cm (real)
//         yr: beam position in y from raster in cm (real)
//         cxdc: cx at DC1 (trl_cx in nt10 or nt22) (real)
//         cydc: cy at DC1 (trl_cy in nt10 or nt22) (real)
//         xdc: x at DC1 (cm) (trl_x in nt10/nt22) (real)
//         ydc: y at DC1 (cm) (trl_y in nt10/nt22) (real)
//         zdc: z at DC1 (cm) (trl_z in nt10/nt22) (real)
//         p: particle momentum in GeV (real)
//         q: particle charge (-1 or 1) (integer)
// Outputs: cxc: cx at target (real)
//          cyc: cy at target (real)
//          czc: cz at target (real)
//           vz: vz at target in cm (real)
// Documentation: EG1-DVCS TN-004 (April, 2010)
// Author: Peter Bosted
// Date: April 20, 2010
////////////////////////////////////////////////////////////
// C++ translation by Erin Seder
// Define in main program:
//    #include "target.C"
//    float *target;
// during event loop get xy[0], xy[1] from raster correction subroutine and
// define the run_number
// then call during particle loop from nt22/root22 variables as:
//    target =
//    target_info(run_number,xy[0],xy[1],tl1_cx[jj],tl1_cy[jj],tl1_x[jj],tl1_y[jj]
//    ,tl1_z[jj] ,p[jj],q[jj]);
// this will return an array of 4 variables:
//    target[0] = new cx,
//    target[1] = new cy,
//    target[2] = new cz,
//    target[3] = new vertex
// Please note this is only for CHARGED PARTICLES

std::array<float, 4> target_info(int irun, float xr, float yr, float cxdc,
                                 float cydc, float xdc, float ydc, float zdc,
                                 float p, int q) {
  // float th;
  // float phi;
  float cxc;
  float cyc;
  float czc;
  float thf;
  float dth;
  // float vznom;
  float phif;
  float xp;
  float yp;
  float th0;
  float phi0;
  float targsign;
  float rdc;
  float fc;
  float vz;
  float czdc;
  float temp;
  float temp2;
  float temp3;
  // float* target = new float[4];
  // SJJ: MEMORY leak protection:
  std::array<float, 4> target{{0, 0, 0, 0}};

  // protect against garbage in, garbage out This is needed because some tracks
  // seem to have p=0
  cxc = 0;
  cyc = 0;
  czc = 1;
  vz = 0;
  // target[0] = 0;
  // target[1] = 0; // SJJ unnecessary for array
  // target[2] = 0;
  // target[3] = 0;
  if (p >= 0.001 && (q == 1 || q == -1) && (pow(cxdc, 2) + pow(cydc, 2)) < 1) {

    // overall integral B*dl correction factor
    // We ran 4.97 T, so "field correction" is 0.995
    fc = 0.995;

    // sign of target field compared to simulations
    targsign = -1;
    if (irun > 60222)
      targsign = 1;

    // Get nominal th and phi at DC1 in radians
    temp = 1. - pow(cxdc, 2) - pow(cydc, 2);
    if (temp > 0)
      czdc = sqrt(temp);
    else
      czdc = 0.;
    thf = acos(czdc);
    phif = atan2(cydc, cxdc);

    // Get phi0 without raster correction yet
    phi0 = phif +
           targsign * fc * (0.186 + 0.045 * pow(thf, 2) + 0.008 * pow(thf, 3) +
                            0.0032 * pow(thf, 3) / pow(p, 2)) *
               q / p;

    // correction to polar angle from focusing effect
    // first, get focusing term for beam (x,y)=0.
    dth = fc * (0.90 * thf + 1.2 * pow(thf, 3)) / (100 * pow(p, 2));

    // displacement of beam along trajectory (xp) and
    // perpendicular to it (yp)
    xp = xr * cos(phi0) + yr * sin(phi0);
    yp = -xr * sin(phi0) + yr * cos(phi0);

    // correction to dth from radial target field, which
    // only depends on raster x and y but not vertex z
    // Also, no effect on peak at zero!
    dth = dth * (1. + targsign * q * p * (0.5 / thf) * (yp / 0.75));

    // Now can get cz
    th0 = thf + dth;
    czc = cos(th0);

    // Now phi0 again, this time including raster correction
    phi0 = phif +
           targsign * fc * (0.186 + 0.045 * pow(thf, 2) + 0.008 * pow(thf, 3) +
                            0.0032 * pow(thf, 3) / pow(p, 2)) *
               q / p * (1. - 0.09 * (0.35 / thf) * xp);

    // Get cx and cy using this cz
    cxc = sin(th0) * cos(phi0);
    cyc = sin(th0) * sin(phi0);

    // renomalize czc
    temp2 = 1 - pow(cxc, 2) - pow(cyc, 2);
    if (temp2 > 0)
      czc = sqrt(temp2);
    else
      czc = 0;
    // Apply target field rotation correction
    // do I need targsing here????
    cxc = cxc - targsign * q * czc * 0.0007 / p;
    cyc = cyc + targsign * q * czc * 0.0022 / p;

    // renomalize czc

    temp3 = 1 - pow(cxc, 2) - pow(cyc, 2);
    if (temp3 > 0)
      czc = sqrt(temp3);
    else
      czc = 0;
    // Get vertex z in cm
    rdc = sqrt(pow(xdc - xr, 2) + pow(ydc - yr, 2));
    vz = zdc - (rdc - 22. * cos(th0) * (tan(th0) - tan(thf))) / tan(thf);

    target[0] = cxc;
    target[1] = cyc;
    target[2] = czc;
    target[3] = vz;
  }
  return target;
}
// modified copy-paste from raster.C
// EG1-DVCS experiment
// Function: convert raw raster ADC readings to beam
//           position relative to CLAS center (in cm)
// Inputs: run: run number (integer)
//         rastr1: raw x raster ADC readings (integer)
//         rastr2: raw y raster ADC readings (integer)
// Outputs: xr: beam position in x in cm (real)
//          yr: beam position in y in cm (real)
//          vznom: center of target is cm (real)
//          e0: beam energy at center of target in GeV (real)
// Documentation: EG1-DVCS TN-002 (for x,y,z, April, 2010)
//                EG1-DVCS TN-003 (for e0, April, 2010)
// Author: Peter Bosted
// Date: April 20, 2010
//----------------------------------------------------
// C++ translation by Erin Seder
/////////////////////////////////
// #include "raster.C"
/////////////////////////////////
// beam_info
// Define in main program:
//   float *xy;
// set the run_number
// then call as:
//   xy = beam_info(run_number,rastr1, rastr2);
// this will return an array of 2 variables:
//   xy[0] = beam x position in cm
//   xy[1] = beam y postions in cm
////////////////////////////////////
std::array<float, 2> beam_info(size_t run, int rastr1, int rastr2) {
  float x_off;
  float y_off;
  float x_gain;
  float y_gain;
  // float* xy = new float[2];
  // SJJ: MEMORY leak protection:
  std::array<float, 2> xy{{0, 0}};

  // x and y offsets seem to change from time to time for no
  // apparant reason. Nominal zero of the ADC is 4000, ! so differences from
  // this indicates where the beam ! centroid physically was relative to the
  // magnetic ! center of CLAS. Note: 400 ADC counts is about 1 mm.
  x_off = 3660;
  if (run >= 59153)
    x_off = 3730;
  if (run >= 59221)
    x_off = 3580;
  if (run >= 59601)
    x_off = 3590;
  if (run >= 59910)
    x_off = 3780;
  if (run >= 59945)
    x_off = 3620;
  if (run >= 60223)
    x_off = 3940;
  if (run >= 60557)
    x_off = 3980;

  y_off = 4520;
  if (run >= 58821)
    y_off = 4330;
  if (run >= 59221)
    y_off = 4530;
  if (run >= 59551)
    y_off = 4410;
  if (run >= 59621)
    y_off = 4350;
  if (run >= 59977)
    y_off = 3950;
  if (run >= 60006)
    y_off = 4350;
  if (run >= 60223)
    y_off = 3900;
  if (run >= 60396)
    y_off = 3650;
  if (run >= 60566)
    y_off = 3450;

  // get gain factors that convert ADC counts to cm
  // Note: these are inversely proportion to beam energy
  // also, get eo and vz at center of target

  // start with 5.9 gev
  x_gain = 0.000229;
  y_gain = -0.000250;

  // switch to 4.8 gev
  if (run >= 59161) {
    x_gain = 0.000284;
    y_gain = -0.000313;
  }

  // switch to 5.95 gev
  if (run >= 59221) {
    x_gain = 0.000225;
    y_gain = -0.000243;
  }

  // switch to 5.7 GeV
  if (run >= 60223) {
    x_gain = 0.000239;
    y_gain = -0.000258;
  }

  // default
  // xy[0] = 0.; // SJJ unnecessary for array
  // xy[1] = 0.;

  // values out of range
  if ((rastr1 >= 1500 && rastr1 <= 10000) ||
      (rastr2 >= 1500 && rastr2 <= 10000)) { // actually get xr,yr

    xy[0] = (rastr1 - x_off) * x_gain;
    xy[1] = (rastr2 - y_off) * y_gain;
    //      cout<<"sub rater1: "<<xy[0]<<endl;
  }
  return xy;
}
} // ns slow_info_impl
} // unnamed namespace

// slow_info constructor
slow_info::slow_info()
    : basic_slow_info{}
    , good_run{false}
    , beam_energy{-999}
    , nominal_vertex{-999}
    , fiducial_group{-999}
    , torus_current{-999}
    , beam_pol{-999} {}

slow_info::slow_info(size_t runnb) : slow_info{} {
  auto basic_info = std::find_if(
      slow_info_impl::SLOW_INFO.begin(), slow_info_impl::SLOW_INFO.end(),
      [=](const basic_slow_info& r) { return r.runnb == runnb; });
  if (basic_info == slow_info_impl::SLOW_INFO.end()) {
    LOG_DEBUG("slow_info", "No slow control info found for run: " + std::to_string(runnb));
    good_run = false;
    return;
  }
  // set the basic info
  static_cast<basic_slow_info&>(*this) = *basic_info;
  // check if the raster info is good
  if (std::binary_search(slow_info_impl::BAD_RASTER.begin(),
                         slow_info_impl::BAD_RASTER.end(), runnb)) {
    LOG_DEBUG("slow_info", "Bad raster for run: " + std::to_string(runnb));
    good_run = false;
    return;
  }
  // get the other variables
  beam_energy = slow_info_impl::ebeam(runnb);
  nominal_vertex = slow_info_impl::vertex(runnb);
  fiducial_group = slow_info_impl::fcgr_assign(runnb);
  torus_current = slow_info_impl::ctorus(runnb);
  beam_pol = slow_info_impl::beam_pola(runnb);
  good_run = true;
}

TVector3 slow_info::beam_pos(int rastr1, int rastr2) const {
  auto xy = slow_info_impl::beam_info(runnb, rastr1, rastr2);
  return {xy[0], xy[1], nominal_vertex};
}
std::pair<TVector3, float>
slow_info::reconstruct_vertex(const TVector3& beam_pos, const TVector3& dc_dir,
                              const TVector3& dc_pos, float p, int q) const {
  auto cxyz_vz = slow_info_impl::target_info(runnb, beam_pos.X(), beam_pos.Y(),
                                             dc_dir.X(), dc_dir.Y(), dc_pos.X(),
                                             dc_pos.Y(), dc_pos.Z(), p, q);
  return {{cxyz_vz[0], cxyz_vz[1], cxyz_vz[2]}, cxyz_vz[3]};
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer
