#ifndef ANALYZER_CLAS_EG1DVCS_ANACONF_LOADED
#define ANALYZER_CLAS_EG1DVCS_ANACONF_LOADED

#include <string>

#include <boost/lexical_cast.hpp>

#include <analyzer/clas/eg1dvcs/specs.hh>
#include <analyzer/core/configuration.hh>
#include <analyzer/physics/pdg.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// simple struct to hold the generic analysis configuration info useful for
// analyzing eg1-dvcs data.
struct anaconf {
  std::string target_name;     // target name
  target_type target_material; // target material
  const TParticlePDG* nuclear_pdg;   // nuclear PDG info in case of a nuclear target
  bool enable_dq;  // do we want to enable or disable DQ?

  anaconf(const ptree& settings, const string_path& path);

private:
  void update_target();
};


} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
