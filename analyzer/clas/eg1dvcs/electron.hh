#ifndef ANALYZER_CLAS_EG1DVCS_ELECTRON_LOADED
#define ANALYZER_CLAS_EG1DVCS_ELECTRON_LOADED

#include <cstdint>
#include <memory>

#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/clas/detector.hh>

#include <analyzer/clas/eg1dvcs/cuts.hh>
#include <analyzer/clas/eg1dvcs/particle.hh>
#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// diagnostic histos (see below)
template <class Reader, class Cut>
void electron_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

// =============================================================================
// Electron fiducial cuts
// =============================================================================
template <class Reader>
class electron_fiducial
    : public cut<electron_fiducial<Reader>, electron_candidate<Reader>&> {
public:
  using base_type = cut<electron_fiducial<Reader>, electron_candidate<Reader>&>;
  using candidate_type = electron_candidate<Reader>;

  electron_fiducial(const ptree& settings, const string_path& path,
                    const std::string& title,
                    std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , delta_vertex_cut_{settings,
                          path / "delta_nominal_vertex",
                          "#Delta nominal vertex Cut",
                          {"dvz",
                           [](candidate_type& c) {
                             return c.vertex.Z() - c.r.slow.nominal_vertex;
                           }}}
      , standard_cut_{settings, path / "standard", "Standard Fiducial Cut"}
      , ec_cut_{settings,
                path / "EC",
                "EC UVW Cut",
                {{"U", [](candidate_type& c) { return c.ec_uvw.X(); }},
                 {"V", [](candidate_type& c) { return c.ec_uvw.Y(); }},
                 {"W", [](candidate_type& c) { return c.ec_uvw.Z(); }}}}
      , dc_cut_{settings,
                path / "DC",
                "DC Cut",
                {"angle", [](candidate_type& c) { return c.phi_dc_sector; }}}
      , ic_cut_{settings,
                path / "IC",
                "IC Shadow Cut",
                {"coords", [](candidate_type& c) { return c.ic_coords.X(); },
                 [](candidate_type& c) { return c.ic_coords.Y(); }}} {
    electron_histos<Reader>(delta_vertex_cut_, hfile);
    electron_histos<Reader>(standard_cut_, hfile);
    electron_histos<Reader>(ec_cut_, hfile);
    electron_histos<Reader>(dc_cut_, hfile);
    electron_histos<Reader>(ic_cut_, hfile);
    electron_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= delta_vertex_cut_(c);
    ok &= standard_cut_(c);
    ok &= ec_cut_(c);
    ok &= dc_cut_(c);
    ok &= ic_cut_(c);
    return ok;
  }

private:
  gaus_cut<double, candidate_type&> delta_vertex_cut_;
  standard_fiducial<candidate_type> standard_cut_;
  range_cut<double, candidate_type&> ec_cut_;
  range_cut<double, candidate_type&> dc_cut_;
  shape_cut<double, candidate_type&> ic_cut_;
};

// =============================================================================
// Electron PID cuts
// =============================================================================
template <class Reader>
class electron_pid
    : public cut<electron_pid<Reader>, electron_candidate<Reader>&> {
public:
  using base_type = cut<electron_pid<Reader>, electron_candidate<Reader>&>;
  using candidate_type = electron_candidate<Reader>;

  electron_pid(const ptree& settings, const string_path& path,
               const std::string& title, std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ec_edep_cut_{settings,
                     path / "EC_E",
                     "EC Energy-Deposition Cut",
                     {{"ec_ei",
                       [](candidate_type& c) { return c.r.ec.ec_ei[c.index]; }},
                      {"ec_eo",
                       [](candidate_type& c) { return c.r.ec.ec_eo[c.index]; }}}}
      , cc_nphe_cut_{settings,
                     path / "CC",
                     "CC Photo-electron Cut",
                     {"nphe",
                      [](candidate_type& c) { return c.r.cc.nphe[c.index]; }}}
      , e_over_p_cut_{settings,
                      path / "EC_E_over_p",
                      "EC E/p Cut",
                      {"param", [](candidate_type& c) { return c.mom; },
                       [](candidate_type& c) { return c.ec_etot / c.mom; }}} {
    electron_histos<Reader>(ec_edep_cut_, hfile);
    electron_histos<Reader>(cc_nphe_cut_, hfile);
    electron_histos<Reader>(e_over_p_cut_, hfile);
    electron_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ec_edep_cut_(c);
    ok &= cc_nphe_cut_(c);
    ok &= e_over_p_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> ec_edep_cut_;
  range_cut<double, candidate_type&> cc_nphe_cut_;
  polygaus_cut<double, candidate_type&> e_over_p_cut_;
};

// =============================================================================
// Primary Electron Detector
// =============================================================================
template <class Reader>
class leading_electron_detector
    : public detector_base<leading_electron_detector<Reader>, Reader,
                           electron_candidate<Reader>> {
public:
  using base_type = detector_base<leading_electron_detector<Reader>, Reader,
                                  electron_candidate<Reader>>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  leading_electron_detector(const ptree& settings, const string_path& path,
                            std::shared_ptr<TFile> hfile = nullptr)
      : momentum_cut_{settings,
                      path / "momentum",
                      "Momentum Cut",
                      {"p", [](const double p) { return p; }}}
      , fiducial_cuts_{settings, path / "fiducial", "Electron Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "Electron PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iEVNT) {
    // charge check
    if (r.evnt.q[iEVNT] >= 0 || !momentum_cut_(r.evnt.p[iEVNT])) {
      return false;
    }
    // make sure we have a good EC reading
    if (fabs(r.ec.ech_x[iEVNT]) < 0.1 && fabs(r.ec.ech_y[iEVNT]) < 0.1) {
      return false;
    }
    // check if the Cherenkov sector and the CLAS sector match
    if (clas::sector(r.evnt.cx[iEVNT], r.evnt.cy[iEVNT]) + 1 !=
        r.cc.cc_sect[iEVNT]) {
      return false;
    }
    return true;
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  range_cut<double, const double> momentum_cut_;
  electron_fiducial<reader_type> fiducial_cuts_;
  electron_pid<reader_type> pid_cuts_;
};

// =======================================================================================
// Implementation: electron diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void electron_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = electron_candidate<Reader>;
  // z-vertex
  c.add_histo(
      hfile, "vz", "z-vertex",
      {"z [cm]", [](candidate_type& c) { return c.vertex.Z(); }, 78, {-99., -21.}});
  // XY position at the EC
  c.add_histo(hfile, "ec_y_vs_x", "Track position at the EC",
              {"x [cm]",
               [](candidate_type& c) { return c.r.ec.ech_x[c.index]; },
               200,
               {-400., 400.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.ec.ech_y[c.index]; },
               200,
               {-400., 400.}});
  // XY position at the IC
  c.add_histo(hfile, "icpos_y_vs_x", "Track position at IC",
              {"x [cm]",
               [](candidate_type& c) { return c.ic_coords.X(); },
               200,
               {-50., 50.}},
              {"y [cm]",
               [](candidate_type& c) { return c.ic_coords.Y(); },
               200,
               {-50., 50.}});
  // XY position at the DC
  c.add_histo(hfile, "dcpos_y_vs_x", "Track position at DC1",
              {"x [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_x[c.index]; },
               200,
               {-60., 60.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_y[c.index]; },
               200,
               {-60., 60.}});
  // Phi vs Theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0., 50.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               200,
               {0, 360.}});
  // electron EC_eo vs EC_ei
  c.add_histo(hfile, "e_eo_vs_ei", "EC_eo vs EC_ei",
              {"E_{in} [GeV]",
               [](candidate_type& c) { return c.r.ec.ec_ei[c.index]; },
               200,
               {0.001, 0.6}},
              {"E_{out} [GeV]",
               [](candidate_type& c) { return double{c.r.ec.ec_eo[c.index]}; },
               200,
               {0.001, 0.4}});
  // electron (e_tot/p vs p)
  c.add_histo(
      hfile, "e_etot_p_vs_p", "E_{tot}/p vs p",
      {"p_{e} [GeV]",
       [](candidate_type& c) { return c.r.evnt.p[c.index]; },
       200,
       {0.5, 5.}},
      {"E_{tot}/p",
       [](candidate_type& c) { return (c.mom > 0) ? c.ec_etot / c.mom : -1; },
       200,
       {0., 1.}});
  // number of photo-electrons
  c.add_histo(hfile, "e_nphe", "N_{phe}",
              {"#",
               [](candidate_type& c) { return c.r.cc.nphe[c.index]; },
               50,
               {0., 350.}});
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
