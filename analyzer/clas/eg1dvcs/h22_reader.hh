#ifndef ANALYZER_CLAS_EG1DVCS_H22_READER_LOADED
#define ANALYZER_CLAS_EG1DVCS_H22_READER_LOADED

#include <string>
#include <memory>

#include <analyzer/clas/eg1dvcs/specs.hh>

#include <analyzer/core/tree.hh>
#include <analyzer/core/logger.hh>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

#include <boost/lexical_cast.hpp>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

constexpr const char* const H22_NAME{"h22"};

// the different h22 data banks
namespace h22_banks {
struct head {
  TTreeReaderValue<UInt_t> evntid;
  std::shared_ptr<UInt_t> runnb;
  TTreeReaderValue<UChar_t> ihel;
  TTreeReaderValue<UShort_t> rastr1;
  TTreeReaderValue<UShort_t> rastr2;

  head(TTreeReader& r, std::shared_ptr<UInt_t> run_buf);
};
struct evnt {
  TTreeReaderValue<Int_t> gpart;
  TTreeReaderArray<Int_t> q;
  TTreeReaderArray<Float_t> p;
  TTreeReaderArray<Float_t> b;
  TTreeReaderArray<Float_t> cx;
  TTreeReaderArray<Float_t> cy;
  TTreeReaderArray<Float_t> vz;
  TTreeReaderArray<Int_t> id;
  TTreeReaderArray<Float_t> vx;
  TTreeReaderArray<Float_t> vy;

  evnt(TTreeReader& r);
};

struct dc {
  TTreeReaderArray<UChar_t> dc_sect;
  TTreeReaderArray<Float_t> tl1_cx;
  TTreeReaderArray<Float_t> tl1_cy;
  TTreeReaderArray<Float_t> tl1_x;
  TTreeReaderArray<Float_t> tl1_y;
  TTreeReaderArray<Float_t> tl1_z;
  TTreeReaderArray<Float_t> tl3_x;
  TTreeReaderArray<Float_t> tl3_y;
  TTreeReaderArray<Float_t> tl3_z;
  TTreeReaderArray<Float_t> tl3_cx;
  TTreeReaderArray<Float_t> tl3_cy;
  TTreeReaderArray<Float_t> tl3_cz;

  dc(TTreeReader& r);
};

struct ec {
  // EC
  TTreeReaderArray<UChar_t> ec_sect;
  TTreeReaderArray<Float_t> ec_r;
  TTreeReaderArray<Float_t> ec_t;
  TTreeReaderArray<Float_t> ec_ei;
  TTreeReaderArray<Float_t> ec_eo;
  TTreeReaderArray<Float_t> etot;
  TTreeReaderArray<Float_t> ech_x;
  TTreeReaderArray<Float_t> ech_y;
  TTreeReaderArray<Float_t> ech_z;

  ec(TTreeReader& r);
};

struct sc {
  TTreeReaderArray<UChar_t> sc_sect;
  TTreeReaderArray<Float_t> sc_r;
  TTreeReaderArray<Float_t> sc_t;
  TTreeReaderArray<Float_t> edep;
  TTreeReaderArray<UChar_t> sc_pd;

  sc(TTreeReader& r);
};

struct cc {
  TTreeReaderArray<UChar_t> cc_sect;
  TTreeReaderArray<Float_t> cc_r;
  TTreeReaderArray<Float_t> cc_t;
  TTreeReaderArray<UShort_t> nphe;
  TTreeReaderArray<Float_t> cc_c2;
  TTreeReaderArray<UChar_t> cc_pmt;

  cc(TTreeReader& r);
};

struct ic {
  TTreeReaderValue<Int_t> svicpart;
  TTreeReaderArray<Float_t> xc;
  TTreeReaderArray<Float_t> yc;
  TTreeReaderArray<Float_t> etc;
  TTreeReaderArray<Float_t> ecc;
  TTreeReaderArray<Float_t> tc;
  TTreeReaderArray<UChar_t> nblk;
  TTreeReaderArray<Float_t> ich_x;
  TTreeReaderArray<Float_t> ich_y;
  TTreeReaderArray<Float_t> et;
  TTreeReaderArray<Float_t> egl;
  TTreeReaderArray<Float_t> ich_xgl;
  TTreeReaderArray<Float_t> ich_ygl;

  ic(TTreeReader& r);
};
} // ns h22_banks

class h22_reader : public tree_reader {
public:
  using parent_type = tree_reader;

  h22_reader(const std::vector<std::string>& fnames,
             const std::string& h22_name = H22_NAME);
private: 
  std::shared_ptr<UInt_t> run;

public:
  h22_banks::head head;
  h22_banks::evnt evnt;
  h22_banks::dc dc;
  h22_banks::ec ec;
  h22_banks::sc sc;
  h22_banks::cc cc;
  h22_banks::ic ic;

  slow_info slow;

  // overwrite next and skip in order to keep the run number updated
  bool next() {
    update_run();
    return parent_type::next();
  }
  bool skip(size_t index) {
    update_run();
    return parent_type::skip(index);
  }

private:
  // update the run number from the file name if this is a new file
  void update_run() {
    if (tree_->GetFile() != file_cache) {
      file_cache = tree_->GetFile();
      std::string fname = file_cache->GetName();
      *run = boost::lexical_cast<UInt_t>(fname.substr(fname.size() - 14, 5));
      slow = slow_info{*run};
      LOG_DEBUG("h22_reader", "Processing new file: " + fname);
      LOG_DEBUG("h22_reader",
                (slow.good_run ? "Run number (good): " : "Run number (bad)") +
                    std::to_string(*run));
    }
  }
  mutable TFile* file_cache;
};

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
