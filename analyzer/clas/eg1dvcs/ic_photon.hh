#ifndef ANALYZER_CLAS_EG1DVCS_IC_PHOTON_LOADED
#define ANALYZER_CLAS_EG1DVCS_IC_PHOTON_LOADED

#include <cstdint>
#include <memory>

#include <TFile.h>
#include <TVector3.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/eg1dvcs/cuts.hh>
#include <analyzer/clas/eg1dvcs/particle.hh>
#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

template <class Reader, class Cut>
void ic_photon_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

// =============================================================================
// Fiducial cuts
// =============================================================================
template <class Reader>
class ic_photon_fiducial
    : public cut<ic_photon_fiducial<Reader>, ic_photon_candidate<Reader>&> {
public:
  using base_type =
      cut<ic_photon_fiducial<Reader>, ic_photon_candidate<Reader>&>;
  using candidate_type = ic_photon_candidate<Reader>;

  ic_photon_fiducial(const ptree& settings, const string_path& path,
                     const std::string& title,
                     std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , octagon_cut_{settings, path / "octagon", "IC Octagon Cut"} {
    ic_photon_histos<Reader>(*this, hfile);
    ic_photon_histos<Reader>(octagon_cut_, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok = true;
    ok &= octagon_cut_(c);
    return ok;
  }
private:
  octagon_cut<candidate_type> octagon_cut_;
};

// =============================================================================
// PID Cuts
// =============================================================================
template <class Reader>
class ic_photon_pid
    : public cut<ic_photon_pid<Reader>, ic_photon_candidate<Reader>&> {
public:
  using base_type = cut<ic_photon_pid<Reader>, ic_photon_candidate<Reader>&>;
  using candidate_type = ic_photon_candidate<Reader>;

  ic_photon_pid(const ptree& settings, const string_path& path,
                const std::string& title,
                std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ic_etc_cut_{
            settings,
            path / "IC_etc",
            "IC Energy Deposition Cut vs #theta",
            {{"flat",
              [](candidate_type& c) { return radian_to_degree(c.theta); },
              [](candidate_type& c) { return c.mom; }},
             {"slope",
              [](candidate_type& c) { return radian_to_degree(c.theta); },
              [](candidate_type& c) { return c.mom; }}}} {
    ic_photon_histos<Reader>(ic_etc_cut_, hfile);
    ic_photon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ic_etc_cut_(c);
    return ok;
  }

private:
  polyrange_cut<double, candidate_type&> ic_etc_cut_;
};

// =============================================================================
// IC Photon detector
// =============================================================================
template <class Reader>
class ic_photon_detector
    : public detector_base<ic_photon_detector<Reader>, Reader,
                           ic_photon_candidate<Reader>, clas_particle> {
public:
  using base_type = detector_base<ic_photon_detector<Reader>, Reader,
                                  ic_photon_candidate<Reader>, clas_particle>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  ic_photon_detector(const ptree& settings, const string_path& path,
                     std::shared_ptr<TFile> hfile = nullptr)
      : fiducial_cuts_{settings, path / "fiducial", "IC Photon Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "IC Photon PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iIC,
                 const clas_particle& leading) {
    const double dummy{leading.mom};
    return !nathan_is_hot_channel(r, iIC);
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  // ugly copy-paste from Mohammad
  // not sure which IC coordinate to use here, so for now using Nathan's
  // (unambiguous) code instead)
  // TODO: get the correct hot channels for eg1-DVCS!
  bool moh_is_hot_channel(Reader& r, const unsigned iIC) {
    double ic_x = r.ic.ich_xgl[iIC];
    double ic_y = r.ic.ich_ygl[iIC];
    return (-11.0 < ic_x && ic_x < -10.3 && -3.0 < ic_y && ic_y < -2.2) ||
           (-5.8 < ic_x && ic_x < -5.1 && -8.5 < ic_y && ic_y < -7.9) ||
           (-1.7 < ic_x && ic_x < -1.1 && -11.3 < ic_y && ic_y < -10.7) ||
           (-3.0 < ic_x && ic_x < -2.3 && -8.5 < ic_y && ic_y < -7.9) ||
           (-7.5 < ic_x && ic_x < -6.0 && 10.5 < ic_y && ic_y < 11.5) ||
           (-12.8 < ic_x && ic_x < -11.5 && -8.5 < ic_y && ic_y < -7.5) ||
           (3.9 < ic_x && ic_x < 4.5 && -14.1 < ic_y && ic_y < -13.5);
  }
  bool nathan_is_hot_channel(Reader& r, const unsigned iIC) {
    static constexpr const std::array<int, 9 * 2> GOOD_IC_BADPIX{
        0, 0, 3, 4, -8, -2, -4, -6, -2, -6, -1, -8, 3, -10, -5, 8, -9, -6};
    static constexpr const double DY{1.346};
    static constexpr const double DX{1.360};
    const int xpix{static_cast<int>(round(r.ic.ich_xgl[iIC] / DX))};
    const int ypix{static_cast<int>(round(r.ic.ich_ygl[iIC] / DY))};
    for (unsigned ii = 0; ii < GOOD_IC_BADPIX.size() / 2; ++ii) {
      if (xpix == GOOD_IC_BADPIX[ii * 2] &&
          ypix == GOOD_IC_BADPIX[ii * 2 + 1]) {
        return true;
      }
    }
    return false;
  }

  ic_photon_fiducial<reader_type> fiducial_cuts_;
  ic_photon_pid<reader_type> pid_cuts_;
};

// =======================================================================================
// Implementation: ic_photon diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void ic_photon_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = ic_photon_candidate<Reader>;
  // IC xy postion
  c.add_histo(hfile, "ic_y_vs_x", "IC y vs x",
              {"x [cm]",
               [](candidate_type& c) { return c.r.ic.xc[c.index]; },
               200,
               {-16., 16.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.ic.yc[c.index]; },
               200,
               {-16., 16.}});
  // IC phi vs theta
  c.add_histo(hfile, "ic_phi_vs_theta", "IC #phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0, 70.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               200,
               {0, 360.}});
  // IC energy vs theta
  c.add_histo(
      hfile, "ic_etc_vs_theta", "IC Energy vs #theta",
      {"E_{tc} [GeV]", [](candidate_type& c) { return c.mom; }, 200, {0, 3.}},
      {"#theta [deg.]",
       [](candidate_type& c) { return radian_to_degree(c.theta); },
       200,
       {0, 30.}});
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
