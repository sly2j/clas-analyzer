#include <analyzer/clas/eg1dvcs/h22_reader.hh>

#include <fstream>

namespace analyzer {
namespace clas {
namespace eg1dvcs {
namespace h22_banks {
head::head(TTreeReader& r, std::shared_ptr<UInt_t> run_buf)
    : evntid{r, "evntid"}
    , runnb{run_buf}
    , ihel{r, "ihel"}
    , rastr1{r, "rastr1"}
    , rastr2{r, "rastr2"} {}

evnt::evnt(TTreeReader& r)
    : gpart{r, "gpart"}
    , p{r, "p"}
    , q{r, "q"}
    , b{r, "b"}
    , cx{r, "cx"}
    , cy{r, "cy"}
    , vz{r, "vz"}
    , id{r, "id"}
    , vx{r, "vx"}
    , vy{r, "vy"} {}

dc::dc(TTreeReader& r)
    : dc_sect{r, "dc_sect"}
    , tl1_cx{r, "tl1_cx"}
    , tl1_cy{r, "tl1_cy"}
    , tl1_x{r, "tl1_x"}
    , tl1_y{r, "tl1_y"}
    , tl1_z{r, "tl1_z"}
    , tl3_x{r, "tl3_x"}
    , tl3_y{r, "tl3_y"}
    , tl3_z{r, "tl3_z"}
    , tl3_cx{r, "tl3_cx"}
    , tl3_cy{r, "tl3_cy"}
    , tl3_cz{r, "tl3_cz"} {}

ec::ec(TTreeReader& r)
    : ec_sect{r, "ec_sect"}
    , ec_r{r, "ec_r"}
    , ec_t{r, "ec_t"}
    , ec_ei{r, "ec_ei"}
    , ec_eo{r, "ec_eo"}
    , etot{r, "etot"}
    , ech_x{r, "ech_x"}
    , ech_y{r, "ech_y"}
    , ech_z{r, "ech_z"} {}

sc::sc(TTreeReader& r)
    : sc_sect{r, "sc_sect"}
    , sc_r{r, "sc_r"}
    , sc_t{r, "sc_t"}
    , edep{r, "edep"}
    , sc_pd{r, "sc_pd"} {}

cc::cc(TTreeReader& r)
    : cc_sect{r, "cc_sect"}
    , cc_r{r, "cc_r"}
    , cc_t{r, "cc_t"}
    , nphe{r, "nphe"}
    , cc_c2{r, "cc_c2"}
    , cc_pmt{r, "cc_pmt"} {}

ic::ic(TTreeReader& r)
    : svicpart{r, "svicpart"}
    , xc{r, "xc"}
    , yc{r, "yc"}
    , etc{r, "etc"}
    , ecc{r, "ecc"}
    , tc{r, "tc"}
    , nblk{r, "nblk"}
    , ich_x{r, "ich_x"}
    , ich_y{r, "ich_y"}
    , et{r, "et"}
    , egl{r, "egl"}
    , ich_xgl{r, "ich_xgl"}
    , ich_ygl{r, "ich_ygl"} {}
} // ns h22_banks

h22_reader::h22_reader(const std::vector<std::string>& fnames,
                       const std::string& h22_name)
    : parent_type{fnames, h22_name}
    , run{std::make_shared<UInt_t>(0)}
    , head{reader_, run}
    , evnt{reader_}
    , dc{reader_}
    , ec{reader_}
    , sc{reader_}
    , cc{reader_}
    , ic{reader_} {}

} // ns eg1dvcs
} // ns clas
} // ns analyzer
