#ifndef ANALYZER_CLAS_EG1DVCS_KAON_LOADED
#define ANALYZER_CLAS_EG1DVCS_KAON_LOADED

#include <cstdint>
#include <memory>
#include <tuple>

#include <TFile.h>
#include <TVector3.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/eg1dvcs/cuts.hh>
#include <analyzer/clas/eg1dvcs/particle.hh>
#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// diagnostic histos, see below
template <class Reader, class Cut>
void kaon_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

// =============================================================================
// Kaon fiducial cuts
// =============================================================================
template <class Reader>
class kaon_fiducial
    : public cut<kaon_fiducial<Reader>, kaon_candidate<Reader>&> {
public:
  using base_type = cut<kaon_fiducial<Reader>, kaon_candidate<Reader>&>;
  using candidate_type = kaon_candidate<Reader>;

  kaon_fiducial(const ptree& settings, const string_path& path,
                const std::string& title,
                std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , delta_vertex_cut_{settings,
                          path / "delta_vertex",
                          "#Delta Vertex Cut",
                          {"dvz", [](candidate_type& c) { return c.dvz; }}}
      , ic_cut_{settings,
                path / "IC",
                "IC Shadow Cut",
                {"coords", [](candidate_type& c) { return c.ic_coords.X(); },
                 [](candidate_type& c) { return c.ic_coords.Y(); }}} {
    kaon_histos<Reader>(delta_vertex_cut_, hfile);
    kaon_histos<Reader>(ic_cut_, hfile);
    kaon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= delta_vertex_cut_(c);
    ok &= ic_cut_(c);
    return ok;
  }

private:
  gaus_cut<double, candidate_type&> delta_vertex_cut_;
  shape_cut<double, candidate_type&> ic_cut_;
};

// =============================================================================
// Kaon fiducial cuts
// =============================================================================
template <class Reader>
class kaon_pid : public cut<kaon_pid<Reader>, kaon_candidate<Reader>&> {
public:
  using base_type = cut<kaon_pid<Reader>, kaon_candidate<Reader>&>;
  using candidate_type = kaon_candidate<Reader>;

  kaon_pid(const ptree& settings, const string_path& path,
           const std::string& title, std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , dbeta_cut_{settings,
                   path / "dbeta",
                   "#Delta#beta Cut",
                   {"range", [](candidate_type& c) { return c.dbeta; }}}
      , sc_edep_cut_{settings, path / "sc_edep", "SC Energy Deposition Cut"} {
    kaon_histos<Reader>(dbeta_cut_, hfile);
    kaon_histos<Reader>(sc_edep_cut_, hfile);
    kaon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= dbeta_cut_(c);
    ok &= sc_edep_cut_(c);
    return ok;
  }

private:
  gaus_cut<double, candidate_type&> dbeta_cut_;
  sc_edep<candidate_type> sc_edep_cut_;
};

// =============================================================================
// Kaon fiducial cuts
// =============================================================================
template <class Reader>
class kaon_detector
    : public detector_base<kaon_detector<Reader>, Reader,
                           kaon_candidate<Reader>, clas_particle> {
public:
  enum class flavor { PLUS, MINUS, ALL };
  using base_type =
      detector_base<kaon_detector<Reader>, Reader, kaon_candidate<Reader>>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  kaon_detector(const ptree& settings, const string_path& path,
                const flavor kaon_flavor,
                std::shared_ptr<TFile> hfile = nullptr)
      : mass_{PDG_K_PLUS.Mass()}
      , flavor_{kaon_flavor}
      , charge_{(flavor_ == flavor::PLUS) ? 1 : (flavor_ == flavor::MINUS) ? -1
                                                                           : 0}
      , momentum_cut_{settings,
                      path / "momentum",
                      "Momentum Cut",
                      {"p", [](const double p) { return p; }}}
      , timing_cut_{settings,
                    path / "timing",
                    "Timing Cut",
                    {"dt",
                     [](const double t, const double t_leading) {
                       return t - t_leading;
                     }}}
      , fiducial_cuts_{settings, path / "fiducial", "Kaon Fiducial Cuts", hfile}
      , pid_cuts_{settings, path / "pid", "Kaon PID Cuts", hfile} {
    timing_cut_.add_histo(
        hfile, "dt", "#Delta t between Kaon candidate and leading lepton",
        {"dt [ns]", [](const double t, const double t_leading) {
           return t - t_leading;
         }, 200, {-100, 100}});
    timing_cut_.add_histo(hfile, "t_k_vs_leading",
                          "t at vertex of Kaon candidate versus leading lepton",
                          {"dt_{leading} [ns]",
                           [](const double t, const double t_leading) {
                             return 0. * t + t_leading;
                           },
                           200,
                           {-100, 100}},
                          {"dt_{K} [ns]",
                           [](const double t, const double t_leading) {
                             return t + 0 * t_leading;
                           },
                           200,
                           {-100, 100}});
  }

  bool preselect(reader_type& r, const size_t iEVNT, const clas_particle& leading) {
    // charge check and momentum cut
    if (r.evnt.q[iEVNT] == 0 || !momentum_cut_(r.evnt.p[iEVNT])) {
      return false;
    }
    // flavor check
    if ((flavor_ == flavor::PLUS || flavor_ == flavor::MINUS) &&
        r.evnt.q[iEVNT] != charge_) {
      return false;
    }
    // timing cut
    if (!timing_cut_(t(r.sc.sc_t[iEVNT], r.sc.sc_r[iEVNT], r.evnt.p[iEVNT]),
                     leading.t)) {
      return false;
    }
    return true;
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  // calculate the time at the vertex using tracking information assuming the
  // particle was a kaon
  double t(const double sc_t, const double sc_r, const double p) {
    const double beta{p / sqrt(mass_ * mass_ + p * p)};
    return sc_t - sc_r / (constants::c_cm_ns * beta);
  }

  const double mass_;
  const flavor flavor_;
  const int charge_;
  range_cut<double, const double> momentum_cut_;
  gaus_cut<double, const double, const double> timing_cut_;
  kaon_fiducial<reader_type> fiducial_cuts_;
  kaon_pid<reader_type> pid_cuts_;
};

// =======================================================================================
// Implementation: kaon diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void kaon_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = kaon_candidate<Reader>;
  // z-vertex
  c.add_histo(hfile, "vz", "z-vertex",
              {"z [cm]",
               [](candidate_type& c) { return c.vertex.Z(); },
               78,
               {-99., -21.}});
  // XY position at the IC
  c.add_histo(hfile, "icpos_y_vs_x", "Track position at IC",
              {"x [cm]",
               [](candidate_type& c) { return c.ic_coords.X(); },
               200,
               {-50., 50.}},
              {"y [cm]",
               [](candidate_type& c) { return c.ic_coords.Y(); },
               200,
               {-50., 50.}});
  // XY position at the DC
  c.add_histo(hfile, "dcpos_y_vs_x", "Track position at DC1",
              {"x [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_x[c.index]; },
               200,
               {-60., 60.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_y[c.index]; },
               200,
               {-60., 60.}});
  // Phi vs Theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0., 50.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               200,
               {0, 360.}});
  // Phi_sector vs Theta
  c.add_histo(hfile, "theta_vs_phisec", "#theta vs #phi_{sec}",
              {"#phi_{sec} [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi_sector); },
               200,
               {-35, 35.}},
              {"#theta} [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0, 50.}});
  // delta_beta
  c.add_histo(hfile, "dbeta_vs_p", "#Delta#beta vs p",
              {"p [GeV]", [](candidate_type& c) { return c.mom; }, 200, {0, 2}},
              {"#Delta#beta",
               [](candidate_type& c) { return c.dbeta; },
               200,
               {-.7, .7}});
  // SC energy deposition
  c.add_histo(hfile, "sc_edep_vs_p", "#SC Energy Deposition vs p",
              {"p [GeV]", [](candidate_type& c) { return c.mom; }, 200, {0, 2}},
              {"E_{SC}",
               [](candidate_type& c) { return c.r.sc.edep[c.index]; },
               200,
               {0, 200}});
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
