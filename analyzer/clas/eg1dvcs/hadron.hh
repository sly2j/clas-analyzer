#ifndef ANALYZER_CLAS_EG1DVCS_HADRON_LOADED
#define ANALYZER_CLAS_EG1DVCS_HADRON_LOADED

#include <cstdint>
#include <memory>
#include <tuple>

#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {
// hadron candidate info
template <class Reader> struct hadron_candidate : clas_particle {
  // reference to the current entry
  Reader& r;
  const size_t iEVNT;
  // additional detector info
  double dbeta;
  TVector3 c_xyz_cor;
  size_t sector;
  double phi_sector;
  double phi_dc_sector; // phi position (relative to sector) in DC
  TVector3 ic_coords;   // coordinates at the IC plane

  hadron_candidate(Reader& r, const size_t iEVNT,
                   const TParticlePDG& hadron_pdg)
      : clas_particle{}
      , r{r}
      , iEVNT{iEVNT}
      , phi_dc_sector{clas::angle_in_sector(
            r.dc.dc_sect[iEVNT] - 1, r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT])}
      , ic_coords{clas::project_dc1_to_ic(
            {r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT], r.dc.tl1_z[iEVNT]},
            {r.dc.tl1_cx[iEVNT], r.dc.tl1_cy[iEVNT],
             sqrt(1 - pow(r.dc.tl1_cx[iEVNT], 2) -
                  pow(r.dc.tl1_cy[iEVNT], 2))})} {
    // get beam x and y position (at the nominal vertex)
    TVector3 beam_pos{r.slow.beam_pos(*r.head.rastr1, *r.head.rastr2)};
    // get corrected c_xyz and vz info
    double vz;
    std::tie(c_xyz_cor, vz) = r.slow.reconstruct_vertex(
        beam_pos, {r.dc.tl1_cx[iEVNT], r.dc.tl1_cy[iEVNT], -999},
        {r.dc.tl1_x[iEVNT], r.dc.tl1_y[iEVNT], r.dc.tl1_z[iEVNT]},
        r.evnt.p[iEVNT], r.evnt.q[iEVNT]);
    // properly initialize the underlying clas particle
    static_cast<clas_particle&>(*this) =
        clas_particle{*r.head.runnb,
                      *r.head.evntid,
                      {hadron_pdg, r.evnt.p[iEVNT], acos(c_xyz_cor.Z()),
                       atan2_pos(c_xyz_cor.Y(), c_xyz_cor.X())},
                      {beam_pos.X(), beam_pos.Y(), vz},
                      0};
    // timing and phi in sector info
    dbeta = r.evnt.b[iEVNT] - beta;
    dt = r.sc.sc_t[iEVNT] - r.sc.sc_r[iEVNT] / (constants::c_cm_ns * beta);
    sector = clas::sector(phi);
    phi_sector = clas::angle_in_sector(sector, phi);
  }
};

#endif
