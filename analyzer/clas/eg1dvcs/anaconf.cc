#include "eg1dvcs/anaconf.hh"
#include <analyzer/core/logger.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

anaconf::anaconf(const ptree& settings, const string_path& path) {
  configuration conf{settings, path};
  // target info
  target_name = conf.get<std::string>("target", "other");
  update_target();
  // data quality?
  enable_dq = conf.get<bool>("enable_dq", true);
    LOG_INFO("anaconf", (enable_dq ? "DQ enabled" : "DQ disabled"));
}

void anaconf::update_target() {
  if (target_name == "NH3" || target_name == "nh3") {
    target_name = "NH3";
    target_material = target_type::NH3;
    nuclear_pdg = &PDG_N14;
  } else if (target_name == "ND3" || target_name == "nd3") {
    target_name = "ND3";
    target_material = target_type::ND3;
    nuclear_pdg = &PDG_N14;
  } else if (target_name == "C12" || target_name == "c12") {
    target_name = "C12";
    target_material = target_type::C12;
    nuclear_pdg = &PDG_C12;
  } else if (target_name == "empty" || target_name == "EMPTY") {
    target_name = "empty";
    target_material = target_type::EMPTY;
    nuclear_pdg = nullptr;
  } else {
    target_name = "other";
    target_material = target_type::OTHER;
    nuclear_pdg = nullptr;
  }
  LOG_INFO("anaconf", "Target material: " + target_name);
}

} // ns eg1dvcs
} // ns clas
} // ns analyzer
