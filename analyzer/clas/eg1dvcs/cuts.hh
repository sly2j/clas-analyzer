#ifndef ANALYZER_CLAS_EG1DVCS_CUTS_LOADED
#define ANALYZER_CLAS_EG1DVCS_CUTS_LOADED

#include <cmath>
#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>
#include <analyzer/clas/eg1dvcs/specs.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

// =============================================================================
// Octagon cut for IC particles
// =============================================================================
template <class Candidate>
class octagon_cut
    : public cut<octagon_cut<Candidate>, Candidate&> {
public:
  using base_type =
      cut<octagon_cut<Candidate>, Candidate&>;
  using candidate_type = Candidate;

  octagon_cut(const ptree& settings, const string_path& path,
              const std::string& title)
      : base_type{settings, path, title}
      , step_x{configurable::conf().template get<double>("geometry/step_x")}
      , step_y{configurable::conf().template get<double>("geometry/step_y")}
      , n_in{configurable::conf().template get<double>("geometry/n_in")}
      , n_out{configurable::conf().template get<double>("geometry/n_out")}
      , sqrt2{sqrt(2)} {}

  bool cut_impl(candidate_type& c) {
    const double x{c.r.ic.xc[c.index]};
    const double y{c.r.ic.yc[c.index]};
    // outside inner octagon
    if (fabs(x / step_x) <= n_in && fabs(y / step_y) <= n_in &&
        fabs(x / step_x - y / step_y) <= sqrt2 * n_in &&
        fabs(x / step_x + y / step_y) <= sqrt2 * n_in) {
      return false;
    }
    // inside (not outside) outer octagon
    if (fabs(x / step_x) >= n_out || fabs(y / step_y) >= n_out ||
        fabs(x / step_x - y / step_y) >= sqrt2 * n_out ||
        fabs(x / step_x + y / step_y) >= sqrt2 * n_out) {
      return false;
    }
    // all OK
    return true;
  }

private:
  const double step_x;
  const double step_y;
  const double n_in;
  const double n_out;
  const double sqrt2;
};

// =============================================================================
// EG1-DVCS standard (CC) fiducial cut
//
// actual implementation in specs/fiducial
// =============================================================================
template <class Candidate>
class standard_fiducial : public cut<standard_fiducial<Candidate>, Candidate&> {
public:
  using base_type = cut<standard_fiducial<Candidate>, Candidate&>;
  using candidate_type = Candidate;

  standard_fiducial(const ptree& settings, const string_path& path,
                    const std::string& title)
      : base_type{settings, path, title}
      , loose_cut_{
            configurable::conf().template get<bool>("enable_loose_cuts")} {}

  bool cut_impl(candidate_type& c) {
    return apply_fiducial_cut(c.theta, c.phi, c.mom, c.r.slow.torus_current,
                              c.sector, c.r.slow.fiducial_group, loose_cut_);
  }

private:
  bool loose_cut_;
};

// =============================================================================
// SC energy deposition versus p cut
//
// Needed for pi-K separation
// =============================================================================
template <class Candidate>
class sc_edep : public cut<sc_edep<Candidate>, Candidate&> {
public:
  using base_type = cut<sc_edep<Candidate>, Candidate&>;
  using candidate_type = Candidate;

  sc_edep(const ptree& settings, const string_path& path,
          const std::string& title)
      : base_type{settings, path, title}
      , a_{configurable::conf().template get<double>("param/a")}
      , b_{configurable::conf().template get<double>("param/b")}
      , c_{configurable::conf().template get<double>("param/c")} {}

  bool cut_impl(candidate_type& c) {
    return c.r.sc.edep[c.index] > a_ + b_ / (c.mom - c_);
  }

private:
  double a_;
  double b_;
  double c_;
};



} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
