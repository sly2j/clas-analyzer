#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#endif

#include <analyzer/clas/particle.hh>
#include <analyzer/clas/event.hh>
#ifdef __CINT__
#pragma link C++ class analyzer::clas::clas_particle+;
#pragma link C++ class analyzer::clas::exclusive_event+;
#endif
