#ifndef ANALYZER_CLAS_EVENT_LOADED
#define ANALYZER_CLAS_EVENT_LOADED

#include <analyzer/physics/scattering.hh>
#include <analyzer/physics/pdg.hh>
#include <analyzer/clas/particle.hh>

namespace analyzer {
namespace clas {

struct exclusive_event {
  size_t run;
  size_t event;
  // initial state
  particle beam;
  particle target;
  // detected
  clas_particle scat;
  clas_particle produced;
  clas_particle recoil;
  // calculated
  particle vphoton;
  // scattering data
  inclusive_data incl;
  exclusive_data excl; // fully exclusive system

  exclusive_event() {}

  // standard constructor, using all involved particles
  exclusive_event(const particle& beam, const particle& target,
                  const clas_particle& electron, const clas_particle& produced,
                  const clas_particle& recoil)
      : run{electron.run}
      , event{electron.event}
      , beam{beam}
      , target{target}
      , scat{electron}
      , produced{produced}
      , recoil{recoil}
      , vphoton{PDG_PHOTON, beam.p - scat.p}
      , incl{beam, target, vphoton}
      , excl{incl, beam, target, vphoton, produced, recoil} {}
  // shorthand for electron beam in z-direction and target at rest
  exclusive_event(const double beam_energy, const TParticlePDG& target_pdg,
                  const clas_particle& electron, const clas_particle& produced,
                  const clas_particle& recoil)
      : exclusive_event{
            {PDG_ELECTRON,
             {0, 0, sqrt(beam_energy * beam_energy -
                         PDG_ELECTRON.Mass() * PDG_ELECTRON.Mass())}},
            {target_pdg, {0, 0, 0}},
            electron,
            produced,
            recoil} {}

  // recoil not detected, deduce from other particles
  exclusive_event(const particle& beam, const particle& target,
                  const clas_particle& electron, const clas_particle& produced,
                  const TParticlePDG& recoil_pdg)
      : exclusive_event{
            beam,
            target,
            electron,
            produced,
            {electron.run,
             electron.event,
             {recoil_pdg, beam.p + target.p - electron.p - produced.p},
             electron.vertex,
             electron.dt}} {}
  // shorthand for beam in z-direction and target at rest, recoil deduced
  exclusive_event(const double beam_energy, const TParticlePDG& target_pdg,
                  const clas_particle& electron, const clas_particle& produced,
                  const TParticlePDG& recoil_pdg)
      : exclusive_event{
            {PDG_ELECTRON,
             {0, 0, sqrt(beam_energy * beam_energy -
                         PDG_ELECTRON.Mass() * PDG_ELECTRON.Mass())}},
            {target_pdg, {0, 0, 0}},
            electron,
            produced,
            recoil_pdg} {}
};

} // ns clas
} // ns analyzer

#endif
