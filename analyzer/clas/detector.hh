#ifndef ANALYZER_CLAS_DETECTOR_LOADED
#define ANALYZER_CLAS_DETECTOR_LOADED

#include <algorithm>
#include <cstdint>
#include <vector>

#include <analyzer/clas/particle.hh>

namespace analyzer {
namespace clas {

// CRTP base class for our detectors
// Child Detector classes define at least a particle type, and 
// dq(), pid() and fiducial() cut member functions, as well as a particle() member
// function that creates a new particle
template <class Detector, class Reader, class Candidate, class... Extra>
class detector_base {
public:
  using child_type = Detector;
  using reader_type = Reader;
  using candidate_type = Candidate;
  using particle_type = clas_particle;

  // returns the detected particles in a vector sorted by momentum
  auto detect(reader_type& r, const size_t begin, const size_t end,
              const Extra&... e) {
    std::vector<particle_type> particles;
    // detection loop
    for (size_t i{begin}; i < end; ++i) {
      // pre-selection criteria
      if (!child().preselect(r, i, e...)) {
        continue;
      }
      // calculate candidate particle
      candidate_type c{r, i, e...};
      // fiducial and PID cuts
      if (child().fiducial(c) && child().pid(c)) {
        particles.push_back(c);
      }
    }
    std::sort(particles.begin(), particles.end(),
              [](const auto& a, const auto& b) { return (a.mom < b.mom); });
    return particles;
  }
  auto detect(reader_type& r, const size_t n, const Extra&... e) {
    return detect(r, 0, n, e...);
  }

private:
  // CRTP boilerplate
  child_type& child() { return static_cast<child_type&>(*this); }
  const child_type& child() const {
    return static_cast<const child_type&>(*this);
  }
};
} // ns clas
} // ns analyzer

#endif
