#ifndef ANALYZER_CLAS_PARTICLE_LOADED
#define ANALYZER_CLAS_PARTICLE_LOADED

#include <cstddef>

#include <TVector3.h>

#include <analyzer/physics/particle.hh>

namespace analyzer {
namespace clas {

struct clas_particle : particle {
  size_t run{0};            // run number
  size_t event{0};          // event number
  TVector3 vertex{0, 0, 0}; // full track vertex
  double dvz{0};            // difference in z-vertex with the primary
  double t{0};              // time at the vertex
  double dt{0};             // time difference with the primary at the vertex
  bool ic{false};
  clas_particle() = default;
  clas_particle(const clas_particle&) = default;
  clas_particle(size_t run, size_t event, const particle& part, TVector3 vertex,
                double t, bool ic = false)
      : particle{part}
      , run{run}
      , event{event}
      , vertex{vertex}
      , dvz{vertex.Z()}
      , t{t}
      , dt{t}
      , ic{ic} {}
  clas_particle& operator=(const clas_particle&) = default;
};

} // ns clas
} // ns analyzer

#endif
