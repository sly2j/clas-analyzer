#ifndef ANALYZER_CLAS_SPECS_LOADED
#define ANALYZER_CLAS_SPECS_LOADED

#include <cmath>
#include <analyzer/core/math.hh>
#include <analyzer/physics/pdg.hh>

namespace analyzer {
namespace clas {
namespace eg6 {

// copy-paste from Nathan
double correct_vz(const size_t runno, const double vz, const double theta,
                  const double phi) {
  // INPUT:
  // runno = EG6 Run Number
  // vz    = CLAS z-vertex (cm)
  // theta = CLAS theta (rad)
  // phi   = CLAS phi (rad)
  //
  // OUTPUT:
  // corrected z-vertex (cm)
  //
  static const int nr = 4;
  static const int runranges[nr] = {61483, 61580, 61850, 99999};
  static const double xx[nr] = {.155, .237, .27, .3};    // x beam position (cm)
  static const double yy[nr] = {.029, -.040, .04, -.04}; // y beam position (cm) 
  if (fabs(sin(theta)) < 1e-3) {
    return -9999;
  }
  int therange = 0;
  for (int ii = 0; ii < nr; ii++) {
    if (runno < runranges[ii]) {
      therange = ii;
      break;
    }
  }
  const double rr = sqrt(pow(xx[therange], 2) + pow(yy[therange], 2));
  const double phi0 = atan2(yy[therange], xx[therange]) + 3.141593;
  return vz - rr * cos(phi - phi0) / tan(theta);
}

// =============================================================================
// Beam Energy as a function of run number
// =============================================================================
inline double beam_energy(const int run) {
  if (run >= 61225 && run <= 61482) {
    return 1.206;
  } else if (run >= 61001 && run <= 61210) {
    return 5.776;
  } else if (run >= 61483 && run <= 61496) {
    return 5.776;
  } else if (run >= 61510 && run <= 61782) {
    return 6.064;
  } else if (run == 61789 || run == 61790) {
    return 1.269;
  } else if (run >= 61791 && run <= 61930) {
    return 6.064;
  } else if (run >= 61932 && run <= 61966) {
    return 1.269;
  }
  // if we're still here something is wrong
  std::cerr << "ERROR: Unknown beam energy for run number: " << run
            << std::endl;
  return -999.;
}
// =============================================================================
// Target type and mass as a function of run number
// =============================================================================
inline pdg_id target_type(const int run) {
  // hydrogen target:
  if ((run >= 61963 && run <= 61966) || (run >= 61432 && run <= 61446)) {
    return pdg_id::p;
  }
  // default to helium-4
  return pdg_id::He4;
}
inline double target_mass(const int run) {
  return (target_type(run) == pdg_id::He4) ? PDG_HE4.Mass() : PDG_PROTON.Mass();
}

} // ns eg6
} // ns clas
} // ns analyzer

#endif
