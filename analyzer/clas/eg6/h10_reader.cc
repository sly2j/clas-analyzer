#include <analyzer/clas/eg6/h10_reader.hh>

#include <fstream>

namespace analyzer {
namespace clas {
namespace eg6 {
namespace h10_banks {
head::head(TTreeReader& r)
    : evntid{r, "evntid"}
    , runnb{r, "runnb"}
    , tr_time{r, "tr_time"}
    , q_l{r, "q_l"}
    , helicity_cor{r, "helicity_cor"} {}

evnt::evnt(TTreeReader& r)
    : gpart{r, "gpart"}
    , id{r, "id"}
    , stat{r, "stat"}
    , dc{r, "dc"}
    , cc{r, "cc"}
    , sc{r, "sc"}
    , ec{r, "ec"}
    , lec{r, "lec"}
    , st{r, "st"}
    , p{r, "p"}
    , m{r, "m"}
    , q{r, "q"}
    , b{r, "b"}
    , cx{r, "cx"}
    , cy{r, "cy"}
    , cz{r, "cz"}
    , vx{r, "vx"}
    , vy{r, "vy"}
    , vz{r, "vz"} {}
dc::dc(TTreeReader& r)
    : dc_part{r, "dc_part"}
    , dc_sect{r, "dc_sect"}
    , dc_trk{r, "dc_trk"}
    , dc_stat{r, "dc_stat"}
    , tb_st{r, "tb_st"}
    , dc_xsc{r, "dc_xsc"}
    , dc_ysc{r, "dc_ysc"}
    , dc_zsc{r, "dc_zsc"}
    , dc_cxsc{r, "dc_cxsc"}
    , dc_cysc{r, "dc_cysc"}
    , dc_czsc{r, "dc_czsc"}
    , dc_vx{r, "dc_vx"}
    , dc_vy{r, "dc_vy"}
    , dc_vz{r, "dc_vz"}
    , dc_vr{r, "dc_vr"}
    , tl1_cx{r, "tl1_cx"}
    , tl1_cy{r, "tl1_cy"}
    , tl1_cz{r, "tl1_cz"}
    , tl1_x{r, "tl1_x"}
    , tl1_y{r, "tl1_y"}
    , tl1_z{r, "tl1_z"}
    , tl1_r{r, "tl1_r"}
    , dc_c2{r, "dc_c2"} {}
ec::ec(TTreeReader& r)
    : ec_part{r, "ec_part"}
    , ec_stat{r, "ec_stat"}
    , ec_sect{r, "ec_sect"}
    , ec_whol{r, "ec_whol"}
    , ec_inst{r, "ec_inst"}
    , ec_oust{r, "ec_oust"}
    , etot{r, "etot"}
    , ec_ei{r, "ec_ei"}
    , ec_eo{r, "ec_eo"}
    , ec_t{r, "ec_t"}
    , ec_r{r, "ec_r"}
    , ech_x{r, "ech_x"}
    , ech_y{r, "ech_y"}
    , ech_z{r, "ech_z"}
    , ec_m2{r, "ec_m2"}
    , ec_m3{r, "ec_m3"}
    , ec_m4{r, "ec_m4"}
    , ec_c2{r, "ec_c2"} {}
sc::sc(TTreeReader& r)
    : sc_part{r, "sc_part"}
    , sc_hit{r, "sc_hit"}
    , sc_pd{r, "sc_pd"}
    , sc_stat{r, "sc_stat"}
    , edep{r, "edep"}
    , sc_t{r, "sc_t"}
    , sc_r{r, "sc_r"}
    , sc_c2{r, "sc_c2"} {}
cc::cc(TTreeReader& r)
    : cc_part{r, "cc_part"}
    , cc_sect{r, "cc_sect"}
    , cc_hit{r, "cc_hit"}
    , cc_segm{r, "cc_segm"}
    , nphe{r, "nphe"}
    , cc_t{r, "cc_t"}
    , cc_r{r, "cc_r"}
    , cc_c2{r, "cc_c2"} {}
ichb::ichb(TTreeReader& r)
    : ic_part{r, "ic_part"}
    , et{r, "et"}
    , egl{r, "egl"}
    , time{r, "time"}
    , time_next{r, "time_next"}
    , ich_x{r, "ich_x"}
    , ich_y{r, "ich_y"}
    , ich_z{r, "ich_z"}
    , ich_xgl{r, "ich_xgl"}
    , ich_ygl{r, "ich_ygl"}
    , ich_xwid{r, "ich_xwid"}
    , ich_ywid{r, "ich_ywid"}
    , ich_xm3{r, "ich_xm3"}
    , ich_ym3{r, "ich_ym3"}
    , ic_stat{r, "ic_stat"} {}
gcpb::gcpb(TTreeReader& r)
    : gcpart{r, "gcpart"}
    , pid{r, "pid"}
    , x{r, "x"}
    , y{r, "y"}
    , z{r, "z"}
    , dedx{r, "dedx"}
    , px{r, "px"}
    , py{r, "py"}
    , pz{r, "pz"}
    , p_tot{r, "p_tot"}
    , x2{r, "x2"}
    , theta{r, "theta"}
    , charge{r, "charge"}
    , dca{r, "dca"}
    , index{r, "index"}
    , phi{r, "phi"}
    , vtl{r, "vtl"}
    , sdist{r, "sdist"}
    , edist{r, "edist"}
    , npts{r, "npts"}
    , r_0{r, "r_0"}
    , fiterr{r, "fiterr"}
    , tothits{r, "tothits"}
    , npd_track{r, "npd_track"}
    , npd_event{r, "npd_event"}
    , bonus_bits{r, "bonus_bits"}
    , q_tot{r, "q_tot"}
    , x_start{r, "x_start"}
    , y_start{r, "y_start"}
    , z_start{r, "z_start"}
    , x_end{r, "x_end"}
    , y_end{r, "y_end"}
    , z_end{r, "z_end"} {}
rtpc::rtpc(TTreeReader& r)
    : rtpc_npart{r, "rtpc_npart"}
    , rtpc_id1{r, "rtpc_id1"}
    , rtpc_id2{r, "rtpc_id2"}
    , rtpc_id3{r, "rtpc_id3"}
    , rtpc_id4{r, "rtpc_id4"}
    , rtpc_id5{r, "rtpc_id5"}
    , rtpc_p1{r, "rtpc_p1"}
    , rtpc_p2{r, "rtpc_p2"}
    , rtpc_p3{r, "rtpc_p3"}
    , rtpc_p4{r, "rtpc_p4"}
    , rtpc_p5{r, "rtpc_p5"}
    , rtpc_poverq{r, "rtpc_poverq"}
    , rtpc_dedx{r, "rtpc_dedx"}
    , rtpc_dedx2{r, "rtpc_dedx2"}
    , rtpc_dedxa{r, "rtpc_dedxa"}
    , rtpc_dedxl{r, "rtpc_dedxl"}
    , rtpc_dedxal{r, "rtpc_dedxal"}
    , rtpc_rxy{r, "rtpc_rxy"}
    , rtpc_slope{r, "rtpc_slope"}
    , rtpc_chisq{r, "rtpc_chisq"}
    , rtpc_dedxs{r, "rtpc_dedxs"}
    , rtpc_theta{r, "rtpc_theta"}
    , rtpc_phi{r, "rtpc_phi"}
    , rtpc_vz{r, "rtpc_vz"}
    , rtpc_bad{r, "rtpc_bad"}
    , rtpc_gcpb{r, "rtpc_gcpb"} {}
icpb::icpb(TTreeReader& r)
    : icpart{r, "icpart"}
    , etc{r, "etc"}
    , ecc{r, "ecc"}
    , tc{r, "tc"}
    , tn{r, "tn"}
    , xc{r, "xc"}
    , yc{r, "yc"}
    , zc{r, "zc"}
    , m2c{r, "m2c"}
    , m3c{r, "m3c"}
    , statc{r, "statc"} {}
} // ns h10_banks

h10_reader::h10_reader(const std::vector<std::string>& fnames,
                       const std::string& h10_name)
    : parent_type{fnames, h10_name}
    , head{reader_}
    , evnt{reader_}
    , dc{reader_}
    , ec{reader_}
    , sc{reader_}
    , cc{reader_}
    , ichb{reader_}
    , gcpb{reader_}
    , rtpc{reader_}
    , icpb{reader_} {}

} // ns eg6
} // ns clas
} // ns analyzer
