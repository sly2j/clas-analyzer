#ifndef ANALYZER_CLAS_EG6_EC_PHOTON_LOADED
#define ANALYZER_CLAS_EG6_EC_PHOTON_LOADED

#include <cstdint>
#include <memory>

#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/index.hh>
#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg6/specs.hh>

namespace analyzer {
namespace clas {
namespace eg6 {
// ec_photon candidate info
template <class Reader> struct ec_photon_candidate : clas_particle {
  constexpr static const double SAMPLING_FRACTION_EC = 0.31;
  // reference to the current entry
  Reader& r;
  const size_t iEVNT;
  const size_t iEC;
  // additional detector info
  double ec_etot;
  TVector3 ec_uvw;
  ec_photon_candidate(Reader& r, const size_t iEVNT,
                      const clas_particle& electron)
      : clas_particle{}
      , r{r}
      , iEVNT{iEVNT}
      , iEC{index::ec(r, iEVNT)}
      , ec_etot{fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC]) /
                SAMPLING_FRACTION_EC}
      , ec_uvw{clas::ec_xyz2uvw(
            {r.ec.ech_x[iEC], r.ec.ech_y[iEC], r.ec.ech_z[iEC]})} {
    // calculate the photon direction
    TVector3 dir{r.ec.ech_x[iEC], r.ec.ech_y[iEC],
                 r.ec.ech_z[iEC] - electron.vertex.Z()};
    // properly initialize the underlying clas_particle
    static_cast<clas_particle&>(*this) = clas_particle{
        electron.run,
        electron.event,
        {PDG_PHOTON, ec_etot * dir.Unit()},
        electron.vertex,
        r.ec.ec_t[iEC] - r.ec.ec_r[iEC] / constants::c_cm_ns - *r.head.tr_time};
    beta = r.evnt.b[iEVNT];
  }
};

// defined in ec_photon_histos.hh
template <class Reader, class Cut>
void ec_photon_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

template <class Reader>
class ec_photon_fiducial
    : public cut<ec_photon_fiducial<Reader>, ec_photon_candidate<Reader>&> {
public:
  using base_type =
      cut<ec_photon_fiducial<Reader>, ec_photon_candidate<Reader>&>;
  using candidate_type = ec_photon_candidate<Reader>;

  ec_photon_fiducial(const ptree& settings, const string_path& path,
                     const std::string& title,
                     std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ec_cut_{settings,
                path / "EC",
                "EC UVW Cut",
                {{"U", [](candidate_type& c) { return c.ec_uvw.X(); }},
                 {"V", [](candidate_type& c) { return c.ec_uvw.Y(); }},
                 {"W", [](candidate_type& c) { return c.ec_uvw.Z(); }}}} {
    ec_photon_histos<Reader>(ec_cut_, hfile);
    ec_photon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ec_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> ec_cut_;
};

template <class Reader>
class ec_photon_pid
    : public cut<ec_photon_pid<Reader>, ec_photon_candidate<Reader>&> {
public:
  using base_type = cut<ec_photon_pid<Reader>, ec_photon_candidate<Reader>&>;
  using candidate_type = ec_photon_candidate<Reader>;

  ec_photon_pid(const ptree& settings, const string_path& path,
                const std::string& title,
                std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ec_edep_cut_{settings,
                     path / "EC_E",
                     "EC Energy-Deposition Cut",
                     {"etot", [](candidate_type& c) { return c.ec_etot; }}}
      , beta_cut_{
            settings,
            path / "beta",
            "Beta Cut",
            {"range", [](candidate_type& c) { return c.r.evnt.b[c.iEVNT]; }}} {
    ec_photon_histos<Reader>(ec_edep_cut_, hfile);
    ec_photon_histos<Reader>(beta_cut_, hfile);
    ec_photon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ec_edep_cut_(c);
    ok &= beta_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> ec_edep_cut_;
  gaus_cut<double, candidate_type&> beta_cut_;
};

template <class Reader>
class ec_photon_detector
    : public detector_base<ec_photon_detector<Reader>, Reader,
                           ec_photon_candidate<Reader>, clas_particle> {
public:
  using base_type = detector_base<ec_photon_detector<Reader>, Reader,
                                  ec_photon_candidate<Reader>, clas_particle>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  ec_photon_detector(const ptree& settings, const string_path& path,
                     std::shared_ptr<TFile> hfile = nullptr)
      : fiducial_cuts_{settings, path / "fiducial", "EC Photon Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "EC Photon PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iEVNT,
                 const clas_particle& leading) {
    // integrity and charge check
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] != 0) {
      return false;
    }
    // valid entries in the EC banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    if (iEC < 0 || iEC >= *r.ec.ec_part) {
      return false;
    }
    //// require valid detector measurement for this track
    //if (r.ec.ec_stat[iEC] <= 0) {
    //  return false;
    //}
    // has NO matching DC track
    const int iDC = r.evnt.dc[iEVNT] - 1;
    if (iDC >= 0) {
      return false;
    }
    const double dummy{leading.mom};
    // all OK!
    return true;
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  ec_photon_fiducial<reader_type> fiducial_cuts_;
  ec_photon_pid<reader_type> pid_cuts_;
};
// =======================================================================================
// Implementation: ec_photon diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void ec_photon_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = ec_photon_candidate<Reader>;
  // XY position at the EC
  c.add_histo(hfile, "ec_y_vs_x", "EC y vs x position",
              {"x [cm]",
               [](candidate_type& c) { return c.r.ec.ech_x[c.iEC]; },
               100,
               {-400., 400}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.ec.ech_y[c.iEC]; },
               100,
               {-400., 400}});
  // phi vs theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               100,
               {0, 70.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               100,
               {0., 360.}});
  // calorimeter EC_eo vs EC_ei
  c.add_histo(hfile, "e_eo_vs_ei", "EC_eo vs EC_ei",
              {"E_{in} [GeV]",
               [](candidate_type& c) { return c.r.ec.ec_ei[c.iEC]; },
               100,
               {0.001, 0.6}},
              {"E_{out} [GeV]",
               [](candidate_type& c) { return double{c.r.ec.ec_eo[c.iEC]}; },
               100,
               {0.001, 0.4}});
  // calorimeter etot
  c.add_histo(hfile, "e_etot", "Total EC energy",
              {"E} [GeV]",
               [](candidate_type& c) { return c.ec_etot; },
               200,
               {0.001, 2.0}});
  // beta vs E
  c.add_histo(
      hfile, "beta_vs_E", "EC Beta vs E",
      {"E [GeV]", [](candidate_type& c) { return c.ec_etot; }, 100, {0., 1.}},
      {"#beta", [](candidate_type& c) { return c.beta; }, 100, {0.6, 1.4}});
  // beta
  c.add_histo(
      hfile, "beta", "EC Beta",
      {"#beta", [](candidate_type& c) { return c.beta; }, 100, {0.6, 1.4}});
}

} // ns eg6
} // ns clas
} // ns analyzer

#endif
