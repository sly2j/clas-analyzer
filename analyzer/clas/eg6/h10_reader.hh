#ifndef ANALYZER_CLAS_EG6_H10_READER_LOADED
#define ANALYZER_CLAS_EG6_H10_READER_LOADED

#include <memory>

#include <analyzer/core/tree.hh>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>

namespace analyzer {
namespace clas {
namespace eg6 {

constexpr const char* const H10_NAME{"h10"};

// the different h10 data banks
namespace h10_banks {
struct head {
  TTreeReaderValue<UInt_t> evntid;
  TTreeReaderValue<UInt_t> runnb;
  TTreeReaderValue<Float_t> tr_time;
  TTreeReaderValue<Float_t> q_l;
  TTreeReaderValue<Int_t> helicity_cor;

  head(TTreeReader& r);
};
struct evnt {
  TTreeReaderValue<Int_t> gpart;
  TTreeReaderArray<Int_t> id;
  TTreeReaderArray<Int_t> stat;
  TTreeReaderArray<Int_t> dc;
  TTreeReaderArray<Int_t> cc;
  TTreeReaderArray<Int_t> sc;
  TTreeReaderArray<Int_t> ec;
  TTreeReaderArray<Int_t> lec;
  TTreeReaderArray<Int_t> st;
  TTreeReaderArray<Float_t> p;
  TTreeReaderArray<Float_t> m;
  TTreeReaderArray<Int_t> q;
  TTreeReaderArray<Float_t> b;
  TTreeReaderArray<Float_t> cx;
  TTreeReaderArray<Float_t> cy;
  TTreeReaderArray<Float_t> cz;
  TTreeReaderArray<Float_t> vx;
  TTreeReaderArray<Float_t> vy;
  TTreeReaderArray<Float_t> vz;

  evnt(TTreeReader& r);
};
struct dc {
  TTreeReaderValue<Int_t> dc_part;
  TTreeReaderArray<Int_t> dc_sect;
  TTreeReaderArray<Int_t> dc_trk;
  TTreeReaderArray<Int_t> dc_stat;
  TTreeReaderArray<Int_t> tb_st;
  TTreeReaderArray<Float_t> dc_xsc;
  TTreeReaderArray<Float_t> dc_ysc;
  TTreeReaderArray<Float_t> dc_zsc;
  TTreeReaderArray<Float_t> dc_cxsc;
  TTreeReaderArray<Float_t> dc_cysc;
  TTreeReaderArray<Float_t> dc_czsc;
  TTreeReaderArray<Float_t> dc_vx;
  TTreeReaderArray<Float_t> dc_vy;
  TTreeReaderArray<Float_t> dc_vz;
  TTreeReaderArray<Float_t> dc_vr;
  TTreeReaderArray<Float_t> tl1_cx;
  TTreeReaderArray<Float_t> tl1_cy;
  TTreeReaderArray<Float_t> tl1_cz;
  TTreeReaderArray<Float_t> tl1_x;
  TTreeReaderArray<Float_t> tl1_y;
  TTreeReaderArray<Float_t> tl1_z;
  TTreeReaderArray<Float_t> tl1_r;
  TTreeReaderArray<Float_t> dc_c2;

  dc(TTreeReader& r);
};
struct ec {
  TTreeReaderValue<Int_t> ec_part;
  TTreeReaderArray<Int_t> ec_stat;
  TTreeReaderArray<Int_t> ec_sect;
  TTreeReaderArray<Int_t> ec_whol;
  TTreeReaderArray<Int_t> ec_inst;
  TTreeReaderArray<Int_t> ec_oust;
  TTreeReaderArray<Float_t> etot;
  TTreeReaderArray<Float_t> ec_ei;
  TTreeReaderArray<Float_t> ec_eo;
  TTreeReaderArray<Float_t> ec_t;
  TTreeReaderArray<Float_t> ec_r;
  TTreeReaderArray<Float_t> ech_x;
  TTreeReaderArray<Float_t> ech_y;
  TTreeReaderArray<Float_t> ech_z;
  TTreeReaderArray<Float_t> ec_m2;
  TTreeReaderArray<Float_t> ec_m3;
  TTreeReaderArray<Float_t> ec_m4;
  TTreeReaderArray<Float_t> ec_c2;

  ec(TTreeReader& r);
};
struct sc {
  TTreeReaderValue<Int_t> sc_part;
  TTreeReaderArray<Int_t> sc_hit;
  TTreeReaderArray<Int_t> sc_pd;
  TTreeReaderArray<Int_t> sc_stat;
  TTreeReaderArray<Float_t> edep;
  TTreeReaderArray<Float_t> sc_t;
  TTreeReaderArray<Float_t> sc_r;
  TTreeReaderArray<Float_t> sc_c2;

  sc(TTreeReader& r);
};
struct cc {
  TTreeReaderValue<Int_t> cc_part;
  TTreeReaderArray<Int_t> cc_sect;
  TTreeReaderArray<Int_t> cc_hit;
  TTreeReaderArray<Int_t> cc_segm;
  TTreeReaderArray<Int_t> nphe;
  TTreeReaderArray<Float_t> cc_t;
  TTreeReaderArray<Float_t> cc_r;
  TTreeReaderArray<Float_t> cc_c2;

  cc(TTreeReader& r);
};
struct ichb {
  TTreeReaderValue<Int_t> ic_part;
  TTreeReaderArray<Float_t> et;
  TTreeReaderArray<Float_t> egl;
  TTreeReaderArray<Float_t> time;
  TTreeReaderArray<Float_t> time_next;
  TTreeReaderArray<Float_t> ich_x;
  TTreeReaderArray<Float_t> ich_y;
  TTreeReaderArray<Float_t> ich_z;
  TTreeReaderArray<Float_t> ich_xgl;
  TTreeReaderArray<Float_t> ich_ygl;
  TTreeReaderArray<Float_t> ich_xwid;
  TTreeReaderArray<Float_t> ich_ywid;
  TTreeReaderArray<Float_t> ich_xm3;
  TTreeReaderArray<Float_t> ich_ym3;
  TTreeReaderArray<Int_t> ic_stat;

  ichb(TTreeReader& r);
};
struct gcpb {
  TTreeReaderValue<Int_t> gcpart;
  TTreeReaderArray<Int_t> pid;
  TTreeReaderArray<Float_t> x;
  TTreeReaderArray<Float_t> y;
  TTreeReaderArray<Float_t> z;
  TTreeReaderArray<Float_t> dedx;
  TTreeReaderArray<Float_t> px;
  TTreeReaderArray<Float_t> py;
  TTreeReaderArray<Float_t> pz;
  TTreeReaderArray<Float_t> p_tot;
  TTreeReaderArray<Float_t> x2;
  TTreeReaderArray<Float_t> theta;
  TTreeReaderArray<Float_t> charge;
  TTreeReaderArray<Float_t> dca;
  TTreeReaderArray<Int_t> index;
  TTreeReaderArray<Float_t> phi;
  TTreeReaderArray<Float_t> vtl;
  TTreeReaderArray<Float_t> sdist;
  TTreeReaderArray<Float_t> edist;
  TTreeReaderArray<Int_t> npts;
  TTreeReaderArray<Float_t> r_0;
  TTreeReaderArray<Int_t> fiterr;
  TTreeReaderArray<Int_t> tothits;
  TTreeReaderArray<Int_t> npd_track;
  TTreeReaderArray<Int_t> npd_event;
  TTreeReaderArray<Int_t> bonus_bits;
  TTreeReaderArray<Float_t> q_tot;
  TTreeReaderArray<Float_t> x_start;
  TTreeReaderArray<Float_t> y_start;
  TTreeReaderArray<Float_t> z_start;
  TTreeReaderArray<Float_t> x_end;
  TTreeReaderArray<Float_t> y_end;
  TTreeReaderArray<Float_t> z_end;

  gcpb(TTreeReader& r);
};
struct rtpc {
  TTreeReaderValue<Int_t> rtpc_npart;
  TTreeReaderArray<Int_t> rtpc_id1;
  TTreeReaderArray<Int_t> rtpc_id2;
  TTreeReaderArray<Int_t> rtpc_id3;
  TTreeReaderArray<Int_t> rtpc_id4;
  TTreeReaderArray<Int_t> rtpc_id5;
  TTreeReaderArray<Float_t> rtpc_p1;
  TTreeReaderArray<Float_t> rtpc_p2;
  TTreeReaderArray<Float_t> rtpc_p3;
  TTreeReaderArray<Float_t> rtpc_p4;
  TTreeReaderArray<Float_t> rtpc_p5;
  TTreeReaderArray<Float_t> rtpc_poverq;
  TTreeReaderArray<Float_t> rtpc_dedx;
  TTreeReaderArray<Float_t> rtpc_dedx2;
  TTreeReaderArray<Float_t> rtpc_dedxa;
  TTreeReaderArray<Float_t> rtpc_dedxl;
  TTreeReaderArray<Float_t> rtpc_dedxal;
  TTreeReaderArray<Float_t> rtpc_rxy;
  TTreeReaderArray<Float_t> rtpc_slope;
  TTreeReaderArray<Float_t> rtpc_chisq;
  TTreeReaderArray<Float_t> rtpc_dedxs;
  TTreeReaderArray<Float_t> rtpc_theta;
  TTreeReaderArray<Float_t> rtpc_phi;
  TTreeReaderArray<Float_t> rtpc_vz;
  TTreeReaderArray<Float_t> rtpc_bad;
  TTreeReaderArray<Int_t> rtpc_gcpb;

  rtpc(TTreeReader& r);
};
struct icpb {
  TTreeReaderValue<Int_t> icpart;
  TTreeReaderArray<Float_t> etc;
  TTreeReaderArray<Float_t> ecc;
  TTreeReaderArray<Float_t> tc;
  TTreeReaderArray<Float_t> tn;
  TTreeReaderArray<Float_t> xc;
  TTreeReaderArray<Float_t> yc;
  TTreeReaderArray<Float_t> zc;
  TTreeReaderArray<Float_t> m2c;
  TTreeReaderArray<Float_t> m3c;
  TTreeReaderArray<Int_t> statc;

  icpb(TTreeReader& r);
};
} // ns h10_banks

class h10_reader : public tree_reader {
public:
  using parent_type = tree_reader;

  h10_reader(const std::vector<std::string>& fnames,
             const std::string& h10_name = H10_NAME);

  h10_banks::head head;
  h10_banks::evnt evnt;
  h10_banks::dc dc;
  h10_banks::ec ec;
  h10_banks::sc sc;
  h10_banks::cc cc;
  h10_banks::ichb ichb;
  h10_banks::gcpb gcpb;
  h10_banks::rtpc rtpc;
  h10_banks::icpb icpb;
};

} // ns eg6
} // ns clas
} // ns analyzer

#endif
