#ifndef ANALYZER_CLAS_EG6_ELECTRON_LOADED
#define ANALYZER_CLAS_EG6_ELECTRON_LOADED

#include <cstdint>
#include <memory>

#include <TVector2.h>
#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/index.hh>
#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg6/specs.hh>

namespace analyzer {
namespace clas {
namespace eg6 {
// electron candidate info
template <class Reader> struct electron_candidate : clas_particle {
  // reference to the current entry
  Reader& r;
  const size_t iEVNT;
  const size_t iCC;
  const size_t iDC;
  const size_t iEC;
  const size_t iSC;
  // additional detector info
  double ec_etot;
  size_t sector;
  double phi_sector;
  size_t dc_sector;
  double phi_dc_sector; // phi position (relative to sector) in DC
  TVector3 ec_uvw;
  TVector3 ic_coords;

  electron_candidate(Reader& r, const size_t iEVNT)
      : clas_particle{*r.head.runnb,
                      *r.head.evntid,
                      {PDG_ELECTRON, r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                       atan2_pos(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])},
                      {0, 0, 0},
                      0}
      , r{r}
      , iEVNT{iEVNT}
      , iCC{index::cc(r, iEVNT)}
      , iDC{index::dc(r, iEVNT)}
      , iEC{index::ec(r, iEVNT)}
      , iSC{index::sc(r, iEVNT)}
      , ec_etot{fmax(r.ec.etot[iEC], r.ec.ec_ei[iEC] + r.ec.ec_eo[iEC])}
      , sector{clas::sector(phi)}
      , phi_sector{clas::angle_in_sector(sector, phi)}
      , dc_sector{clas::sector(r.dc.tl1_x[iDC], r.dc.tl1_y[iDC])}
      , phi_dc_sector{clas::angle_in_sector(dc_sector, r.dc.tl1_x[iDC],
                                            r.dc.tl1_y[iDC])}
      , ec_uvw{clas::ec_xyz2uvw(
            {r.ec.ech_x[iEC], r.ec.ech_y[iEC], r.ec.ech_z[iEC]})}
      , ic_coords{clas::project_dc1_to_ic(
            {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
            {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})} {
    vertex.SetXYZ(0, 0, eg6::correct_vz(run, r.evnt.vz[iEVNT], theta, phi));
    dt = r.sc.sc_t[iSC] - r.sc.sc_r[iSC] / (constants::c_cm_ns * beta) -
         *r.head.tr_time;
  }
};

// defined in below
template <class Reader, class Cut>
void electron_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

template <class Reader>
class electron_fiducial
    : public cut<electron_fiducial<Reader>, electron_candidate<Reader>&> {
public:
  using base_type = cut<electron_fiducial<Reader>, electron_candidate<Reader>&>;
  using candidate_type = electron_candidate<Reader>;

  electron_fiducial(const ptree& settings, const string_path& path,
                    const std::string& title,
                    std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , vertex_cut_{settings,
                    path / "vertex",
                    "Vertex Cut",
                    {"vz", [](candidate_type& c) { return c.vertex.Z(); }}}
      , ec_cut_{settings,
                path / "EC",
                "EC UVW Cut",
                {{"U", [](candidate_type& c) { return c.ec_uvw.X(); }},
                 {"V", [](candidate_type& c) { return c.ec_uvw.Y(); }},
                 {"W", [](candidate_type& c) { return c.ec_uvw.Z(); }}}}
      , ic_cut_{settings,
                path / "IC",
                "IC Shadow Cut",
                {"coords", [](candidate_type& c) { return c.ic_coords.X(); },
                 [](candidate_type& c) { return c.ic_coords.Y(); }}}
      , dc_cut_{settings,
                path / "DC",
                "DC Cut",
                {"angle", [](candidate_type& c) { return c.phi_dc_sector; }}}
      , cc_cut_{
            settings,
            path / "CC",
            "CC Theta Cut",
            {"param",
             [](candidate_type& c) { return radian_to_degree(c.phi_sector); },
             [](candidate_type& c) { return radian_to_degree(c.theta); }}} {
    electron_histos<Reader>(vertex_cut_, hfile);
    electron_histos<Reader>(ec_cut_, hfile);
    electron_histos<Reader>(ic_cut_, hfile);
    electron_histos<Reader>(dc_cut_, hfile);
    electron_histos<Reader>(cc_cut_, hfile);
    electron_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= vertex_cut_(c);
    ok &= ec_cut_(c);
    ok &= ic_cut_(c);
    ok &= dc_cut_(c);
    ok &= cc_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> vertex_cut_;
  range_cut<double, candidate_type&> ec_cut_;
  shape_cut<double, candidate_type&> ic_cut_;
  range_cut<double, candidate_type&> dc_cut_;
  polyrange_cut<double, candidate_type&> cc_cut_;
};

template <class Reader>
class electron_pid
    : public cut<electron_pid<Reader>, electron_candidate<Reader>&> {
public:
  using base_type = cut<electron_pid<Reader>, electron_candidate<Reader>&>;
  using candidate_type = electron_candidate<Reader>;

  electron_pid(const ptree& settings, const string_path& path,
               const std::string& title, std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ec_edep_cut_{settings,
                     path / "EC_E",
                     "EC Energy-Deposition Cut",
                     {{"ec_ei",
                       [](candidate_type& c) { return c.r.ec.ec_ei[c.iEC]; }},
                      {"ec_eo",
                       [](candidate_type& c) { return c.r.ec.ec_eo[c.iEC]; }}}}
      , cc_nphe_cut_{settings,
                     path / "CC",
                     "CC Photo-electron Cut",
                     {"nphe",
                      [](candidate_type& c) { return c.r.cc.nphe[c.iCC]; }}}
      , e_over_p_cut_{settings,
                      path / "EC_E_over_p",
                      "EC E/p Cut",
                      {"param", [](candidate_type& c) { return c.mom; },
                       [](candidate_type& c) { return c.ec_etot / c.mom; }}} {
    electron_histos<Reader>(ec_edep_cut_, hfile);
    electron_histos<Reader>(cc_nphe_cut_, hfile);
    electron_histos<Reader>(e_over_p_cut_, hfile);
    electron_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ec_edep_cut_(c);
    ok &= cc_nphe_cut_(c);
    ok &= e_over_p_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> ec_edep_cut_;
  range_cut<double, candidate_type&> cc_nphe_cut_;
  polygaus_cut<double, candidate_type&> e_over_p_cut_;
};

template <class Reader>
class electron_detector
    : public detector_base<electron_detector<Reader>, Reader,
                           electron_candidate<Reader>> {
public:
  using base_type = detector_base<electron_detector<Reader>, Reader,
                                  electron_candidate<Reader>>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  electron_detector(const ptree& settings, const string_path& path,
                    std::shared_ptr<TFile> hfile = nullptr)
      : momentum_cut_{settings,
                      path / "momentum",
                      "Momentum Cut",
                      {"p", [](const double p) { return p; }}}
      , fiducial_cuts_{settings, path / "fiducial", "Electron Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "Electron PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iEVNT) {
    // integrety and charge check
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] >= 0 ||
        !momentum_cut_(r.evnt.p[iEVNT])) {
      return false;
    }
    // valid entries in all relevant banks
    const int iEC = r.evnt.ec[iEVNT] - 1;
    const int iCC = r.evnt.cc[iEVNT] - 1;
    const int iDC = r.evnt.dc[iEVNT] - 1;
    const int iSC = r.evnt.sc[iEVNT] - 1;
    if (iEC < 0 || iEC >= *r.ec.ec_part || iCC < 0 || iCC >= *r.cc.cc_part ||
        iDC < 0 || iDC >= *r.dc.dc_part || iSC < 0 || iSC >= *r.sc.sc_part) {
      return false;
    }
    // require valid detector measurement for this track
    if (r.ec.ec_stat[iEC] <= 0 /*|| r.cc.cc_stat[iCC] <= 0*/ ||
        r.dc.dc_stat[iDC] <= 0 || r.sc.sc_stat[iSC] <= 0) {
      return false;
    }
    // check if the Cherenkov sector and the CLAS sector match
    if (clas::sector(r.evnt.cx[iEVNT], r.evnt.cy[iEVNT]) + 1 !=
        r.cc.cc_sect[iCC]) {
      return false;
    }
    return true;
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  range_cut<double, const double> momentum_cut_;
  electron_fiducial<reader_type> fiducial_cuts_;
  electron_pid<reader_type> pid_cuts_;
};

// =======================================================================================
// Implementation: electron diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void electron_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = electron_candidate<Reader>;
  // z-vertex
  c.add_histo(hfile, "vz", "z-vertex",
              {"z [cm]",
               [](candidate_type& c) { return c.vertex.Z(); },
               78,
               {-99., -21.}});
  // XY position at the EC
  c.add_histo(hfile, "ec_y_vs_x", "Track position at the EC",
              {"x [cm]",
               [](candidate_type& c) { return c.r.ec.ech_x[c.iEC]; },
               200,
               {-400., 400.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.ec.ech_y[c.iEC]; },
               200,
               {-400., 400.}});
  // XY position at the IC
  c.add_histo(hfile, "icpos_y_vs_x", "Track position at IC",
              {"x [cm]",
               [](candidate_type& c) { return c.ic_coords.X(); },
               200,
               {-50., 50.}},
              {"y [cm]",
               [](candidate_type& c) { return c.ic_coords.Y(); },
               200,
               {-50., 50.}});
  // XY position at the DC
  c.add_histo(hfile, "dcpos_y_vs_x", "Track position at DC1",
              {"x [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_x[c.iDC]; },
               200,
               {-60., 60.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_y[c.iDC]; },
               200,
               {-60., 60.}});
  // Phi vs Theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0., 50.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               200,
               {0, 360.}});
  // Phi_sector vs Theta
  c.add_histo(hfile, "theta_vs_phisec", "#theta vs #phi_{sec}",
              {"#phi_{sec} [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi_sector); },
               200,
               {-35, 35.}},
              {"#theta} [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0, 50.}});
  // electron EC_eo vs EC_ei
  c.add_histo(hfile, "e_eo_vs_ei", "EC_eo vs EC_ei",
              {"E_{in} [GeV]",
               [](candidate_type& c) { return c.r.ec.ec_ei[c.iEC]; },
               200,
               {0.001, 0.6}},
              {"E_{out} [GeV]",
               [](candidate_type& c) { return double{c.r.ec.ec_eo[c.iEC]}; },
               200,
               {0.001, 0.4}});
  // electron (e_tot/p vs p)
  c.add_histo(
      hfile, "e_etot_p_vs_p", "E_{tot}/p vs p",
      {"p_{e} [GeV]",
       [](candidate_type& c) { return c.r.evnt.p[c.iEVNT]; },
       200,
       {0.5, 5.}},
      {"E_{tot}/p",
       [](candidate_type& c) { return (c.mom > 0) ? c.ec_etot / c.mom : -1; },
       200,
       {0., 1.}});
  // number of photo-electrons
  c.add_histo(hfile, "e_nphe", "N_{phe}",
              {"#",
               [](candidate_type& c) { return c.r.cc.nphe[c.iCC]; },
               50,
               {0., 350.}});
}

} // ns eg6
} // ns clas
} // ns analyzer

#endif
