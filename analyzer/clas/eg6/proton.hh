#ifndef ANALYZER_CLAS_EG6_PROTON_LOADED
#define ANALYZER_CLAS_EG6_PROTON_LOADED

#include <cstdint>
#include <memory>

#include <TVector2.h>
#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/index.hh>
#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg6/specs.hh>

namespace analyzer {
namespace clas {
namespace eg6 {
// proton candidate info
template <class Reader> struct proton_candidate : clas_particle {
  // reference to the current entry
  Reader& r;
  const size_t iEVNT;
  const size_t iDC;
  const size_t iSC;
  // additional detector info
  double dbeta;
  size_t sector;
  double phi_sector;
  size_t dc_sector;
  double phi_dc_sector; // phi position (relative to sector) in DC
  TVector3 ic_coords;

  proton_candidate(Reader& r, const size_t iEVNT)
      : clas_particle{*r.head.runnb,
                      *r.head.evntid,
                      {PDG_PROTON, r.evnt.p[iEVNT], acos(r.evnt.cz[iEVNT]),
                       atan2_pos(r.evnt.cy[iEVNT], r.evnt.cx[iEVNT])},
                      {0, 0, 0},
                      0}
      , r{r}
      , iEVNT{iEVNT}
      , iDC{index::dc(r, iEVNT)}
      , iSC{index::sc(r, iEVNT)}
      , dbeta{((r.sc.sc_r[iSC] / (r.sc.sc_t[iSC] - *r.head.tr_time)) /
               constants::c_cm_ns) -
              beta}
      , sector{clas::sector(phi)}
      , phi_sector{clas::angle_in_sector(sector, phi)}
      , dc_sector{clas::sector(r.dc.tl1_x[iDC], r.dc.tl1_y[iDC])}
      , phi_dc_sector{clas::angle_in_sector(dc_sector, r.dc.tl1_x[iDC],
                                            r.dc.tl1_y[iDC])}
      , ic_coords{clas::project_dc1_to_ic(
            {r.dc.tl1_x[iDC], r.dc.tl1_y[iDC], r.dc.tl1_z[iDC]},
            {r.dc.tl1_cx[iDC], r.dc.tl1_cy[iDC], r.dc.tl1_cz[iDC]})} {
    vertex.SetXYZ(0, 0, eg6::correct_vz(run, r.evnt.vz[iEVNT], theta, phi));
    dt = r.sc.sc_t[iSC] - r.sc.sc_r[iSC] / (constants::c_cm_ns * beta) -
         *r.head.tr_time;
  }
};

// defined below
template <class Reader, class Cut>
void proton_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

template <class Reader>
class proton_fiducial
    : public cut<proton_fiducial<Reader>, proton_candidate<Reader>&> {
public:
  using base_type =
      cut<proton_fiducial<Reader>, proton_candidate<Reader>&>;
  using candidate_type = proton_candidate<Reader>;

  proton_fiducial(const ptree& settings, const string_path& path,
                  const std::string& title,
                  std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , vertex_cut_{settings,
                    path / "vertex",
                    "Vertex Cut",
                    {"vz", [](candidate_type& c) { return c.vertex.Z(); }}}
      , ic_cut_{settings,
                path / "IC",
                "IC Shadow Cut",
                {"coords", [](candidate_type& c) { return c.ic_coords.X(); },
                 [](candidate_type& c) { return c.ic_coords.Y(); }}}
      , dc_cut_{settings,
                path / "DC",
                "DC Cut",
                {"angle", [](candidate_type& c) { return c.phi_dc_sector; }}} {
    proton_histos<Reader>(*this, hfile);
    proton_histos<Reader>(vertex_cut_, hfile);
    proton_histos<Reader>(ic_cut_, hfile);
    proton_histos<Reader>(dc_cut_, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= vertex_cut_(c);
    ok &= ic_cut_(c);
    ok &= dc_cut_(c);
    return ok;
  }

private:
  range_cut<double, candidate_type&> vertex_cut_;
  shape_cut<double, candidate_type&> ic_cut_;
  range_cut<double, candidate_type&> dc_cut_;
};

template <class Reader>
class proton_pid
    : public cut<proton_pid<Reader>, proton_candidate<Reader>&> {
public:
  using base_type = cut<proton_pid<Reader>, proton_candidate<Reader>&>;
  using candidate_type = proton_candidate<Reader>;

  proton_pid(const ptree& settings, const string_path& path,
             const std::string& title, std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , dbeta_cut_{settings,
                   path / "dbeta",
                   "#Delta#beta Cut",
                   {"range", [](candidate_type& c) { return c.dbeta; }}} {
    proton_histos<Reader>(*this, hfile);
    proton_histos<Reader>(dbeta_cut_, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= dbeta_cut_(c);
    return ok;
  }

private:
  gaus_cut<double, candidate_type&> dbeta_cut_;
};

template <class Reader>
class proton_detector : public detector_base<proton_detector<Reader>, Reader,
                                             proton_candidate<Reader>> {
public:
  using base_type =
      detector_base<proton_detector<Reader>, Reader, proton_candidate<Reader>>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  proton_detector(const ptree& settings, const string_path& path,
                  std::shared_ptr<TFile> hfile = nullptr)
      : momentum_cut_{settings,
                      path / "momentum",
                      "Momentum Cut",
                      {"p", [](const double p) { return p; }}}
      , fiducial_cuts_{settings, path / "fiducial", "Proton Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "Proton PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iEVNT) {
    // integrety and charge check
    if (r.evnt.stat[iEVNT] <= 0 || r.evnt.q[iEVNT] <= 0 ||
        !momentum_cut_(r.evnt.p[iEVNT])) {
      return false;
    }
    // valid entries in all relevant banks
    const int iDC = r.evnt.dc[iEVNT] - 1;
    const int iSC = r.evnt.sc[iEVNT] - 1;
    if (iDC < 0 || iDC >= *r.dc.dc_part || iSC < 0 || iSC >= *r.sc.sc_part) {
      return false;
    }
    // require valid detector measurement for this track
    if (/*r.dc.dc_stat[iDC] <= 0 || */r.sc.sc_stat[iSC] <= 0) {
      return false;
    }
    return true;
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  range_cut<double, const double> momentum_cut_;
  proton_fiducial<reader_type> fiducial_cuts_;
  proton_pid<reader_type> pid_cuts_;
};


// =======================================================================================
// Implementation: proton diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void proton_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = proton_candidate<Reader>;
  // z-vertex
  c.add_histo(hfile, "vz", "z-vertex",
              {"z [cm]",
               [](candidate_type& c) { return c.vertex.Z(); },
               78,
               {-99., -21.}});
  // XY position at the IC
  c.add_histo(hfile, "icpos_y_vs_x", "Track position at IC",
              {"x [cm]",
               [](candidate_type& c) { return c.ic_coords.X(); },
               100,
               {-50., 50.}},
              {"y [cm]",
               [](candidate_type& c) { return c.ic_coords.Y(); },
               100,
               {-50., 50.}});
  // XY position at the DC
  c.add_histo(hfile, "dcpos_y_vs_x", "Track position at DC1",
              {"x [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_x[c.iDC]; },
               100,
               {-60., 60.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.dc.tl1_y[c.iDC]; },
               100,
               {-60., 60.}});
  // Phi vs Theta
  c.add_histo(hfile, "phi_vs_theta", "#phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               100,
               {0., 50.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               100,
               {0, 360.}});
  // delta_beta
  c.add_histo(hfile, "dbeta_vs_p", "#Delta#beta vs p",
              {"p [GeV]", [](candidate_type& c) { return c.mom; }, 200, {0, 3}},
              {"#Delta#beta",
               [](candidate_type& c) { return c.dbeta; },
               200,
               {-.7, .7}});
}

} // ns eg6
} // ns clas
} // ns analyzer

#endif
