#ifndef ANALYZER_CLAS_EG6_IC_PHOTON_LOADED
#define ANALYZER_CLAS_EG6_IC_PHOTON_LOADED

#include <cstdint>
#include <memory>

#include <TVector2.h>
#include <TVector3.h>
#include <TFile.h>

#include <analyzer/core/cut.hh>
#include <analyzer/core/math.hh>

#include <analyzer/physics/pdg.hh>
#include <analyzer/physics/constants.hh>

#include <analyzer/clas/detector.hh>
#include <analyzer/clas/index.hh>
#include <analyzer/clas/geometry.hh>
#include <analyzer/clas/particle.hh>

#include <analyzer/clas/eg6/specs.hh>

namespace analyzer {
namespace clas {
namespace eg6 {
// ic_photon candidate info
template <class Reader> struct ic_photon_candidate : clas_particle {
  // reference to the current entry
  Reader& r;
  const size_t iIC;

  ic_photon_candidate(Reader& r, const size_t iIC,
                      const clas_particle& electron)
      : clas_particle{}, r{r}, iIC{iIC} {
    // calculate the photon direction
    TVector3 dir{r.icpb.xc[iIC], r.icpb.yc[iIC], 0. - electron.vertex.Z()};
    // properly initialize the underlying clas_particle
    static_cast<clas_particle&>(*this) = clas_particle{
        electron.run,
        electron.event,
        {PDG_PHOTON, r.icpb.etc[iIC] * dir.Unit()},
        electron.vertex,
        r.icpb.tc[iIC] - dir.Mag() / constants::c_cm_ns - *r.head.tr_time,
        true};
  }
};

// defined in ic_photon_histos.hh
template <class Reader, class Cut>
void ic_photon_histos(Cut& c, std::shared_ptr<TFile> hfile = nullptr);

template <class Reader>
class ic_photon_fiducial : public cut<ic_photon_fiducial<Reader>,
                                      ic_photon_candidate<Reader>&> {
public:
  using base_type =
      cut<ic_photon_fiducial<Reader>, ic_photon_candidate<Reader>&>;
  using candidate_type = ic_photon_candidate<Reader>;

  ic_photon_fiducial(const ptree& settings, const string_path& path,
                     const std::string& title,
                     std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , step_x{configurable::conf().template get<double>("octagon/step_x")}
      , step_y{configurable::conf().template get<double>("octagon/step_y")}
      , n_in{configurable::conf().template get<double>("octagon/n_in")}
      , n_out{configurable::conf().template get<double>("octagon/n_out")}
      , sqrt2{sqrt(2)} {
    ic_photon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    const double x{c.r.icpb.xc[c.iIC]};
    const double y{c.r.icpb.yc[c.iIC]};
    // outside inner octagon
    if (fabs(x / step_x) <= n_in && fabs(y / step_y) <= n_in &&
        fabs(x / step_x - y / step_y) <= sqrt2 * n_in &&
        fabs(x / step_x + y / step_y) <= sqrt2 * n_in) {
      return false;
    }
    // inside (not outside) outer octagon
    if (fabs(x / step_x) >= n_out || fabs(y / step_y) >= n_out ||
        fabs(x / step_x - y / step_y) >= sqrt2 * n_out ||
        fabs(x / step_x + y / step_y) >= sqrt2 * n_out) {
      return false;
    }
    // all OK
    return true;
  }

private:
  const double step_x;
  const double step_y;
  const double n_in;
  const double n_out;
  const double sqrt2;
};

template <class Reader>
class ic_photon_pid
    : public cut<ic_photon_pid<Reader>, ic_photon_candidate<Reader>&> {
public:
  using base_type = cut<ic_photon_pid<Reader>, ic_photon_candidate<Reader>&>;
  using candidate_type = ic_photon_candidate<Reader>;

  ic_photon_pid(const ptree& settings, const string_path& path,
                const std::string& title,
                std::shared_ptr<TFile> hfile = nullptr)
      : base_type{settings, path, title}
      , ic_etc_cut_{
            settings,
            path / "IC_etc",
            "IC Energy Deposition Cut vs #theta",
            {{"flat",
              [](candidate_type& c) { return radian_to_degree(c.theta); },
              [](candidate_type& c) { return c.mom; }},
             {"slope",
              [](candidate_type& c) { return radian_to_degree(c.theta); },
              [](candidate_type& c) { return c.mom; }}}} {
    ic_photon_histos<Reader>(ic_etc_cut_, hfile);
    ic_photon_histos<Reader>(*this, hfile);
  }

  bool cut_impl(candidate_type& c) {
    bool ok{true};
    ok &= ic_etc_cut_(c);
    return ok;
  }

private:
  polyrange_cut<double, candidate_type&> ic_etc_cut_;
};

template <class Reader>
class ic_photon_detector
    : public detector_base<ic_photon_detector<Reader>, Reader,
                           ic_photon_candidate<Reader>, clas_particle> {
public:
  using base_type = detector_base<ic_photon_detector<Reader>, Reader,
                                  ic_photon_candidate<Reader>, clas_particle>;
  using reader_type = typename base_type::reader_type;
  using candidate_type = typename base_type::candidate_type;

  ic_photon_detector(const ptree& settings, const string_path& path,
                     std::shared_ptr<TFile> hfile = nullptr)
      : fiducial_cuts_{settings, path / "fiducial", "IC Photon Fiducial Cuts",
                       hfile}
      , pid_cuts_{settings, path / "pid", "IC Photon PID Cuts", hfile} {}

  bool preselect(reader_type& r, const size_t iIC, const clas_particle& leading) {
    const double dummy = leading.mom;
    return !nathan_is_hot_channel(r, iIC);
  }
  bool fiducial(candidate_type& c) { return fiducial_cuts_(c); }
  bool pid(candidate_type& c) { return pid_cuts_(c); }

private:
  // ugly copy-paste from Mohammad
  // not sure which IC coordinate to use here, so for now using Nathan's
  // (unambiguous) code instead)
  bool moh_is_hot_channel(Reader& r, const unsigned iIC) {
    unsigned iICHB = index::ichb(r, iIC);
    double ic_x = r.ichb.ich_xgl[iICHB];
    double ic_y = r.ichb.ich_ygl[iICHB];
    return (-11.0 < ic_x && ic_x < -10.3 && -3.0 < ic_y && ic_y < -2.2) ||
           (-5.8 < ic_x && ic_x < -5.1 && -8.5 < ic_y && ic_y < -7.9) ||
           (-1.7 < ic_x && ic_x < -1.1 && -11.3 < ic_y && ic_y < -10.7) ||
           (-3.0 < ic_x && ic_x < -2.3 && -8.5 < ic_y && ic_y < -7.9) ||
           (-7.5 < ic_x && ic_x < -6.0 && 10.5 < ic_y && ic_y < 11.5) ||
           (-12.8 < ic_x && ic_x < -11.5 && -8.5 < ic_y && ic_y < -7.5) ||
           (3.9 < ic_x && ic_x < 4.5 && -14.1 < ic_y && ic_y < -13.5);
  }
  bool nathan_is_hot_channel(Reader& r, const unsigned iIC) {
    static constexpr const std::array<int, 9 * 2> GOOD_IC_BADPIX{
        0, 0, 3, 4, -8, -2, -4, -6, -2, -6, -1, -8, 3, -10, -5, 8, -9, -6};
    static constexpr const double DY{1.346};
    static constexpr const double DX{1.360};
    unsigned iICHB = index::ichb(r, iIC);
    const int xpix{static_cast<int>(round(r.ichb.ich_xgl[iICHB] / DX))};
    const int ypix{static_cast<int>(round(r.ichb.ich_ygl[iICHB] / DY))};
    for (unsigned ii = 0; ii < GOOD_IC_BADPIX.size() /2; ++ii) {
      if (xpix == GOOD_IC_BADPIX[ii * 2] &&
          ypix == GOOD_IC_BADPIX[ii * 2 + 1]) {
        return true;
      }
    }
    return false;
  }

  ic_photon_fiducial<reader_type> fiducial_cuts_;
  ic_photon_pid<reader_type> pid_cuts_;
};

// =======================================================================================
// Implementation: ic_photon diagnostic histos
// =======================================================================================
template <class Reader, class Cut>
void ic_photon_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  using candidate_type = ic_photon_candidate<Reader>;
  // IC xy postion
  c.add_histo(hfile, "ic_y_vs_x", "IC y vs x",
              {"x [cm]",
               [](candidate_type& c) { return c.r.icpb.xc[c.iIC]; },
               200,
               {-16., 16.}},
              {"y [cm]",
               [](candidate_type& c) { return c.r.icpb.yc[c.iIC]; },
               200,
               {-16., 16.}});
  // IC phi vs theta
  c.add_histo(hfile, "ic_phi_vs_theta", "IC #phi vs #theta",
              {"#theta [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.theta); },
               200,
               {0, 70.}},
              {"#phi [deg.]",
               [](candidate_type& c) { return radian_to_degree(c.phi); },
               200,
               {0, 360.}});
  // IC energy vs theta
  c.add_histo(
      hfile, "ic_etc_vs_theta", "IC Energy vs #theta",
      {"E_{tc} [GeV]", [](candidate_type& c) { return c.mom; }, 200, {0, 3.}},
      {"#theta [deg.]",
       [](candidate_type& c) { return radian_to_degree(c.theta); },
       200,
       {0, 30.}});
}

} // ns eg6
} // ns clas
} // ns analyzer

#endif
