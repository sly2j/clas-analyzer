#ifndef ANALYZER_CLAS_GEOMETRY_LOADED
#define ANALYZER_CLAS_GEOMETRY_LOADED

#include <TVector3.h>

#include <cmath>
#include <analyzer/core/math.hh>
#include <cassert>

namespace analyzer {
namespace clas {

// =============================================================================
// get the corresponding sector number for an angle phi [0, 2pi]
// in the range [0-5]. User must add 1 to get CLAS sectors.
// =============================================================================
inline size_t sector(const double phi) {
  // formula: (phi + pi/6)/(pi/3) modulo the number of sectors to account for
  // rollover for angles close to 2pi
  return static_cast<size_t>((3 * phi / constants::pi + 0.5)) % 6;
}
inline size_t sector(const double x, const double y) {
  return sector(atan2_pos(y, x));
}

// =============================================================================
// get the angle relative to the center of the sector
// =============================================================================
inline double angle_in_sector(size_t sect, double phi) {
  phi -= sect * constants::pi / 3;
  // deal with rollover
  if (phi > constants::pi) {
    phi -= 2 * constants::pi;
  }
  assert(fabs(phi) < constants::pi);
  return phi;
}
inline double angle_in_sector(size_t sect, const double x, const double y) {
  return angle_in_sector(sect, atan2_pos(y, x));
}

// =============================================================================
// calculate the UVW coordinates at the EC surface
// based on Nathan's SpecsFID::ECxyz2uvw
// =============================================================================
TVector3 ec_xyz2uvw(const TVector3& xyz) {
  constexpr static const double zoffset = 510.32;
  constexpr static const double ec_the = 0.43633230;
  constexpr static const double ylow = -182.97400000;
  constexpr static const double yhi = 189.95600000;
  constexpr static const double tgrho = 1.95325000; // 1.097620829
  constexpr static const double sinrho = 0.89012560;
  constexpr static const double cosrho = 0.45571500;
  static const double sinthe = sin(ec_the);
  static const double costhe = cos(ec_the);

  // phi of the current sector
  const double sector_phi{sector(xyz.X(), xyz.Y()) * constants::pi / 3};

  double rot[3][3];
  rot[0][0] = costhe * cos(sector_phi);
  rot[0][1] = -sin(sector_phi);
  rot[0][2] = sinthe * cos(sector_phi);
  rot[1][0] = costhe * sin(sector_phi);
  rot[1][1] = cos(sector_phi);
  rot[1][2] = sinthe * sin(sector_phi);
  rot[2][0] = -sinthe;
  rot[2][1] = 0.;
  rot[2][2] = costhe;

  double xyzi[3];
  xyzi[1] = xyz.X() * rot[0][0] + xyz.Y() * rot[1][0] + xyz.Z() * rot[2][0];
  xyzi[0] = xyz.X() * rot[0][1] + xyz.Y() * rot[1][1] + xyz.Z() * rot[2][1];
  xyzi[2] =
      xyz.X() * rot[0][2] + xyz.Y() * rot[1][2] + xyz.Z() * rot[2][2] - zoffset;

  TVector3 uvw;
  uvw.SetX((xyzi[1] - ylow) / sinrho);
  uvw.SetY((yhi - ylow) / tgrho - xyzi[0] + (yhi - xyzi[1]) / tgrho);
  uvw.SetZ(((yhi - ylow) / tgrho + xyzi[0] + (yhi - xyzi[1]) / tgrho) / 2. /
           cosrho);

  return uvw;
}

// =============================================================================
// project DC1 position onto the IC surface
// =============================================================================
inline TVector3 project_dc1_to_ic(const TVector3& r1pos,
                                  const TVector3& r1dir) {
  constexpr const double IC_FROM_DC{-16.};
  const TVector3 shift{(r1pos.Z() + IC_FROM_DC) / r1dir.Z() * r1dir};
  return r1pos - shift;
}

} // ns clas
} // ns analyzer

#endif
