#ifndef ANALYZER_PHYSICS_SCATTERING_LOADED
#define ANALYZER_PHYSICS_SCATTERING_LOADED

#include <TLorentzVector.h>

#include <analyzer/physics/particle.hh>
#include <analyzer/physics/pdg.hh>
#include <analyzer/core/math.hh>

namespace analyzer {

// POD for inclusive e-N scattering
struct inclusive_data {
  double Q2;
  double W;
  double nu;
  double y;
  double x;            // Bjorken x
  TLorentzVector p_cm; // CM production frame
  inclusive_data() {}
  inclusive_data(const particle& beam, const particle& target,
                 const particle& vphoton)
      : Q2{-vphoton.p.M2()}
      , W{(vphoton.p + target.p).M()}
      , nu{target.p * vphoton.p / target.mass}
      , y{(target.p * vphoton.p) / (target.p * beam.p)}
      , x{Q2 / (2 * target.mass * nu)}
      , p_cm{vphoton.p + target.p} {}
  ClassDefNV(inclusive_data, 1);
};

// POD for exclusive e-N -> e-R-X scattering
struct exclusive_data {
  // t-channel info
  double t;
  double t_gamma;
  double t_c;
  double t_min;
  // missing momentum
  TLorentzVector pX;
  // missing mass
  double M2X;
  double M2X_A; // gamma*,target,gamma system
  double M2X_g; // gamma*,target,recoil system
  // missing Pperp (to the virtual photon direction)
  double pXperp;
  // angles
  double theta_ggs;  // (gamma*,gamma)
  double theta_cone; // cone angle between measured and expected gamma
  // plane angles
  double phi_scat_prod;   // angle between scattering and production planes
  double phi_coplanarity; // coplanarity angle (gamma*,recoil),(recoil,gamma)

  exclusive_data() {}
  exclusive_data(const inclusive_data& incl, const particle& beam,
                 const particle& target, const particle& vphoton,
                 const particle& produced, const particle& recoil)
      : t{(target.p - recoil.p).M2()}
      , t_gamma{(produced.p - vphoton.p).M2()}
      , t_c{calc_t_c(incl, target.mass, angle(vphoton.p, produced.p))}
      , t_min{calc_t_min(incl, target.mass)}
      , pX{incl.p_cm - produced.p - recoil.p}
      , M2X{pX.M2()}
      , M2X_A{(incl.p_cm - produced.p).M2()}
      , M2X_g{(incl.p_cm - recoil.p).M2()}
      , pXperp{pX.Vect().Mag() * sin(angle(incl.p_cm, recoil.p))}
      , theta_ggs{angle(vphoton.p, produced.p)}
      , theta_cone{angle(produced.p, incl.p_cm - recoil.p)}
      , phi_scat_prod{planar_angle(vphoton.p, beam.p, produced.p)}
      , phi_coplanarity{coplanarity(recoil.p, vphoton.p, produced.p)} {}

  static double calc_t_c(const inclusive_data& incl, const double m_target,
                         const double theta_ggs) {
    const double alpha{
        incl.nu - sqrt(incl.nu * incl.nu + incl.Q2 * incl.Q2) * cos(theta_ggs)};
    return -(incl.Q2 + 2 * incl.nu * alpha) / (1. + (1. / m_target) * (alpha));
  }
  static double calc_t_min(const inclusive_data& incl, const double m_target) {
    static const double m_proton{PDG_PROTON.Mass()};
    const double x_A{incl.x * m_proton/m_target};
    const double epsilon2{4*m_target*m_target*x_A*x_A/incl.Q2};
    return -incl.Q2 * 2 * (1 - x_A) * (1 + epsilon2 - sqrt(1 + epsilon2)) /
           (4 * x_A * (1 - x_A) + epsilon2);
  }

  ClassDefNV(exclusive_data, 1);
};

} // ns analyzer

#endif
