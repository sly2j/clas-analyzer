#ifndef ANALYZER_PHYSICS_PARTICLE_LOADED
#define ANALYZER_PHYSICS_PARTICLE_LOADED

#include <cstddef>

#include <TLorentzVector.h>
#include <TParticlePDG.h>

#include <analyzer/core/math.hh>

// =============================================================================
// (almost) POD particle type (TLorentzVector is not POD)
// =============================================================================
namespace analyzer {
struct particle {
  int id{0};
  double charge{0};
  double mass{0};
  double mom{0};
  double theta{0};
  double phi{0};
  double beta{0};
  TLorentzVector p;
  bool off_shell{false};

  particle() = default;
  particle(const particle&) = default;
  particle(const TParticlePDG& pdg, double mom, double theta, double phi)
      : id{pdg.PdgCode()}
      , charge{pdg.Charge() / 3} // TPDGParticle uses units of |e|/3
      , mass{pdg.Mass()}
      , mom{mom}
      , theta{theta}
      , phi{(phi < 0) ? phi + 2 * constants::pi : phi}
      , beta{mom / sqrt(mass * mass + mom * mom)}
      , p{{mom * sin(theta) * cos(phi), mom * sin(theta) * sin(phi),
           mom * cos(theta)},
          sqrt(mass * mass + mom * mom)} {}
  particle(const TParticlePDG& pdg, const TVector3& p3)
      : particle{pdg, p3.Mag(), p3.Theta(), p3.Phi()} {}
  particle(const TParticlePDG& pdg, const TLorentzVector& p)
      : id{pdg.PdgCode()}
      , charge{pdg.Charge() / 3}
      , mass{pdg.Mass()}
      , mom{p.Vect().Mag()}
      , theta{p.Vect().Theta()}
      , phi{(p.Vect().Phi() < 0) ? p.Vect().Phi() + 2 * constants::pi
                                 : p.Vect().Phi()}
      , beta{mom / sqrt(mass * mass + mom * mom)}
      , p{p}
      , off_shell{mass != p.M()} {
        if (off_shell) {
          beta = 0;
        }
      }
  particle& operator=(const particle&) = default;
};
} // ns analyzer

#endif
