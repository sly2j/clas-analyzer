#ifndef ANALYZER_PHYSICS_CONSTANTS_LOADED
#define ANALYZER_PHYSICS_CONSTANTS_LOADED

#include <cstdint>
#include <analyzer/core/math.hh>

// =============================================================================
// PHYSICAL CONSTANTS
// =============================================================================
namespace analyzer {
namespace constants {

// speed of light in m/s and cm/ns
constexpr double c_m_s = 299792458.;
constexpr double c_cm_ns = 29.9792458;

} // ns constants
} // ns analyzer

#endif
