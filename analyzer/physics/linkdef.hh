#ifdef __CINT__
#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#endif

#include <analyzer/physics/scattering.hh>
#include <analyzer/physics/particle.hh>
#ifdef __CINT__
#pragma link C++ class analyzer::inclusive_data+;
#pragma link C++ class analyzer::exclusive_data+;
#pragma link C++ class analyzer::particle+;
#endif
