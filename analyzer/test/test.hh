// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME 

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "analyzer/physics/scattering.hh"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_analyzercLcLinclusive_data(void *p = 0);
   static void *newArray_analyzercLcLinclusive_data(Long_t size, void *p);
   static void delete_analyzercLcLinclusive_data(void *p);
   static void deleteArray_analyzercLcLinclusive_data(void *p);
   static void destruct_analyzercLcLinclusive_data(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::analyzer::inclusive_data*)
   {
      ::analyzer::inclusive_data *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::analyzer::inclusive_data >(0);
      static ::ROOT::TGenericClassInfo 
         instance("analyzer::inclusive_data", ::analyzer::inclusive_data::Class_Version(), "analyzer/physics/scattering.hh", 10,
                  typeid(::analyzer::inclusive_data), DefineBehavior(ptr, ptr),
                  &::analyzer::inclusive_data::Dictionary, isa_proxy, 4,
                  sizeof(::analyzer::inclusive_data) );
      instance.SetNew(&new_analyzercLcLinclusive_data);
      instance.SetNewArray(&newArray_analyzercLcLinclusive_data);
      instance.SetDelete(&delete_analyzercLcLinclusive_data);
      instance.SetDeleteArray(&deleteArray_analyzercLcLinclusive_data);
      instance.SetDestructor(&destruct_analyzercLcLinclusive_data);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::analyzer::inclusive_data*)
   {
      return GenerateInitInstanceLocal((::analyzer::inclusive_data*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::analyzer::inclusive_data*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_analyzercLcLexclusive_data(void *p = 0);
   static void *newArray_analyzercLcLexclusive_data(Long_t size, void *p);
   static void delete_analyzercLcLexclusive_data(void *p);
   static void deleteArray_analyzercLcLexclusive_data(void *p);
   static void destruct_analyzercLcLexclusive_data(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::analyzer::exclusive_data*)
   {
      ::analyzer::exclusive_data *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::analyzer::exclusive_data >(0);
      static ::ROOT::TGenericClassInfo 
         instance("analyzer::exclusive_data", ::analyzer::exclusive_data::Class_Version(), "analyzer/physics/scattering.hh", 28,
                  typeid(::analyzer::exclusive_data), DefineBehavior(ptr, ptr),
                  &::analyzer::exclusive_data::Dictionary, isa_proxy, 4,
                  sizeof(::analyzer::exclusive_data) );
      instance.SetNew(&new_analyzercLcLexclusive_data);
      instance.SetNewArray(&newArray_analyzercLcLexclusive_data);
      instance.SetDelete(&delete_analyzercLcLexclusive_data);
      instance.SetDeleteArray(&deleteArray_analyzercLcLexclusive_data);
      instance.SetDestructor(&destruct_analyzercLcLexclusive_data);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::analyzer::exclusive_data*)
   {
      return GenerateInitInstanceLocal((::analyzer::exclusive_data*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::analyzer::exclusive_data*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace analyzer {
//______________________________________________________________________________
atomic_TClass_ptr inclusive_data::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *inclusive_data::Class_Name()
{
   return "analyzer::inclusive_data";
}

//______________________________________________________________________________
const char *inclusive_data::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::analyzer::inclusive_data*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int inclusive_data::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::analyzer::inclusive_data*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *inclusive_data::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::analyzer::inclusive_data*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *inclusive_data::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::analyzer::inclusive_data*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace analyzer
namespace analyzer {
//______________________________________________________________________________
atomic_TClass_ptr exclusive_data::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *exclusive_data::Class_Name()
{
   return "analyzer::exclusive_data";
}

//______________________________________________________________________________
const char *exclusive_data::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::analyzer::exclusive_data*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int exclusive_data::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::analyzer::exclusive_data*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *exclusive_data::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::analyzer::exclusive_data*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *exclusive_data::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::analyzer::exclusive_data*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace analyzer
namespace analyzer {
//______________________________________________________________________________
void inclusive_data::Streamer(TBuffer &R__b)
{
   // Stream an object of class analyzer::inclusive_data.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(analyzer::inclusive_data::Class(),this);
   } else {
      R__b.WriteClassBuffer(analyzer::inclusive_data::Class(),this);
   }
}

} // namespace analyzer
namespace ROOT {
   // Wrappers around operator new
   static void *new_analyzercLcLinclusive_data(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::analyzer::inclusive_data : new ::analyzer::inclusive_data;
   }
   static void *newArray_analyzercLcLinclusive_data(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::analyzer::inclusive_data[nElements] : new ::analyzer::inclusive_data[nElements];
   }
   // Wrapper around operator delete
   static void delete_analyzercLcLinclusive_data(void *p) {
      delete ((::analyzer::inclusive_data*)p);
   }
   static void deleteArray_analyzercLcLinclusive_data(void *p) {
      delete [] ((::analyzer::inclusive_data*)p);
   }
   static void destruct_analyzercLcLinclusive_data(void *p) {
      typedef ::analyzer::inclusive_data current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::analyzer::inclusive_data

namespace analyzer {
//______________________________________________________________________________
void exclusive_data::Streamer(TBuffer &R__b)
{
   // Stream an object of class analyzer::exclusive_data.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(analyzer::exclusive_data::Class(),this);
   } else {
      R__b.WriteClassBuffer(analyzer::exclusive_data::Class(),this);
   }
}

} // namespace analyzer
namespace ROOT {
   // Wrappers around operator new
   static void *new_analyzercLcLexclusive_data(void *p) {
      return  p ? ::new((::ROOT::TOperatorNewHelper*)p) ::analyzer::exclusive_data : new ::analyzer::exclusive_data;
   }
   static void *newArray_analyzercLcLexclusive_data(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::TOperatorNewHelper*)p) ::analyzer::exclusive_data[nElements] : new ::analyzer::exclusive_data[nElements];
   }
   // Wrapper around operator delete
   static void delete_analyzercLcLexclusive_data(void *p) {
      delete ((::analyzer::exclusive_data*)p);
   }
   static void deleteArray_analyzercLcLexclusive_data(void *p) {
      delete [] ((::analyzer::exclusive_data*)p);
   }
   static void destruct_analyzercLcLexclusive_data(void *p) {
      typedef ::analyzer::exclusive_data current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::analyzer::exclusive_data

namespace {
  void TriggerDictionaryInitialization__Impl() {
    static const char* headers[] = {
"analyzer/physics/scattering.hh",
0
    };
    static const char* includePaths[] = {
"/usr/local/Cellar/root6/6.04.00/include/root",
"/Users/sjjooste/Dropbox/Work/CLAS/analyzer/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace analyzer{struct __attribute__((annotate("$clingAutoload$analyzer/physics/scattering.hh")))  inclusive_data;}
namespace analyzer{struct __attribute__((annotate("$clingAutoload$analyzer/physics/scattering.hh")))  exclusive_data;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "analyzer/physics/scattering.hh"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"analyzer::exclusive_data", payloadCode, "@",
"analyzer::inclusive_data", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization__Impl, {}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization__Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_() {
  TriggerDictionaryInitialization__Impl();
}
