################################################################################
## CMAKE Settings
################################################################################
set (EXE "test_ana")
set (TARGETS ${TARGETS} ${EXE} PARENT_SCOPE)

################################################################################
## Sources and install headers
################################################################################
set (SOURCES "test.cc")

################################################################################
## Include directories
################################################################################
include_directories("${PROJECT_SOURCE_DIR}")

################################################################################
## External Libraries
################################################################################
## ROOT
find_package(ROOT REQUIRED)
include_directories(${ROOT_INCLUDE_DIR})

################################################################################
## Compile and Link
################################################################################
add_executable(${EXE} ${SOURCES})
target_link_libraries(${EXE}
  analyzer_core 
  analyzer_physics 
  ${ROOT_LIBRARIES})
set_target_properties(${EXE} PROPERTIES VERSION ${ANALYZER_VERSION} )

################################################################################
## Export and Install
################################################################################
install(TARGETS ${EXE}
  EXPORT ${PROJECT_NAME}-targets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin)
