#include <analyzer/physics/scattering.hh>
#include <analyzer/core/tree.hh>

#include <TFile.h>
#include <memory>

using namespace analyzer;

int main() {

  std::shared_ptr<TFile> file{std::make_shared<TFile>("test.root", "RECREATE")};
  tree_writer<inclusive_data> scat(file, "incl", "Inclusive Scattering Data");

  inclusive_data incl;

  incl.Q2 = 3;
  scat.fill(incl);

  incl.Q2 = 5;
  scat.fill(incl);

  incl.Q2 = 7;
  scat.fill(incl);

  return 0;

}
