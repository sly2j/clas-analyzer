#ifndef ANALYZER_PROGRAM_CLAS_EG6_EXCL_LOADED
#define ANALYZER_PROGRAM_CLAS_EG6_EXCL_LOADED

#include <memory>

#include <TFile.h>

#include <analyzer/core/math.hh>
#include <analyzer/core/cut.hh>
#include <analyzer/core/configuration.hh>

#include <analyzer/clas/event.hh>

namespace analyzer {
namespace clas {

class exclusive_selection
    : public cut<exclusive_selection, const exclusive_event&> {
public:
  using base_type = cut<exclusive_selection, const exclusive_event&>;

  exclusive_selection(const ptree& settings, const string_path& path,
                      std::shared_ptr<TFile> hfile = nullptr);

  bool cut_impl(const exclusive_event& e) {
    return (event_cut_(e) && hard_cut_(e) && exclusivity_cut_(e));
  }

private:
  range_cut<double, exclusive_event> event_cut_;
  range_cut<double, exclusive_event> hard_cut_;
  gaus_cut<double, exclusive_event> exclusivity_cut_;
};

} // ns clas
} // ns analyzer

#endif
