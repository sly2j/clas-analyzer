#include <analyzer/core/configuration.hh>
#include <analyzer/core/progress_meter.hh>
#include <analyzer/core/framework.hh>
#include <analyzer/physics/scattering.hh>

#include <analyzer/clas/index.hh>
#include <analyzer/clas/eg6/h10_reader.hh>
#include <analyzer/clas/eg6/electron.hh>
#include <analyzer/clas/eg6/proton.hh>
#include <analyzer/clas/eg6/ec_photon.hh>
#include <analyzer/clas/eg6/ic_photon.hh>

#include "excl.hh"

#include <string>
#include <memory>

using namespace analyzer;
using namespace analyzer::clas;
using namespace analyzer::clas::eg6;

int analyze_idvcs(const ptree& settings, const std::vector<std::string>& input,
                  const std::string& output) {
  // output file
  std::shared_ptr<TFile> ofile{
      std::make_shared<TFile>((output + ".root").c_str(), "recreate")};

  // output dvcs event
  using dvcs_writer_type = tree_writer<exclusive_event>;
  dvcs_writer_type dvcs_out{ofile, "dvcs_events"};
  using dvcs_selection_type = clas::exclusive_selection;
  dvcs_selection_type dvcs_selector{settings, "dvcs", ofile};

  // detection
  using reader_type = clas::eg6::h10_reader;
  using electron_detector_type = clas::eg6::electron_detector<reader_type>;
  using proton_detector_type = clas::eg6::proton_detector<reader_type>;
  using ec_photon_detector_type = clas::eg6::ec_photon_detector<reader_type>;
  using ic_photon_detector_type = clas::eg6::ic_photon_detector<reader_type>;

  reader_type reader{input};
  electron_detector_type electron_detector{settings, "detector/electron",
                                           ofile};
  proton_detector_type proton_detector{settings, "detector/proton", ofile};
  ec_photon_detector_type ec_photon_detector{settings, "detector/ec_photon",
                                             ofile};
  ic_photon_detector_type ic_photon_detector{settings, "detector/ic_photon",
                                             ofile};

  // particles and events
  using particle_type = clas::clas_particle;

  // progress meter
  progress_meter progress{reader.size()};

  // skim loop
  while (reader.next()) {
    progress.update();

    const size_t nEVNT{clas::index::nEVNT(reader)};
    const size_t nIC{clas::index::nIC(reader)};
    // one good electron
    auto electrons = electron_detector.detect(reader, nEVNT);
    if (electrons.size() < 1) {
      continue;
    }
    // one good photon (either EC or IC) and one good proton
    auto photons = ic_photon_detector.detect(reader, nIC, electrons[0]);
    auto ec_photons = ec_photon_detector.detect(reader, nEVNT, electrons[0]);
    photons.insert(photons.end(), ec_photons.begin(), ec_photons.end());
    auto protons = proton_detector.detect(reader, nEVNT);
    if (electrons.size() != 1 || protons.size() == 0 || photons.size() == 0) {
      continue;
    }

    // also need precise beam and target info
    particle beam{PDG_ELECTRON,
                  {0, 0, sqrt(pow(beam_energy(*reader.head.runnb), 2) -
                              pow(PDG_ELECTRON.Mass(), 2))}};
    // proton target for iDVCS!
    particle target{PDG_PROTON, {0, 0, 0}};

    // create an exclusive event
    // inclusive scattering info and hard cuts
    exclusive_event ev{beam, target, electrons[0], photons[0], protons[0]};
    if (!dvcs_selector(ev)) {
      continue;
    }
    dvcs_out.fill(ev);
  }
  return 0;
}

int main(int argc, char* argv[]) {
  try {
    framework analyzer{argc, argv, analyze_idvcs};
    analyzer.run();
    return 0;
  } catch (...) {
    return 1;
  }
}
