#ifndef ANALYZER_CLAS_EG1DVCS_ANACONF_LOADED
#define ANALYZER_CLAS_EG1DVCS_ANACONF_LOADED

#include <string>

#include <boost/lexical_cast.hpp>

#include <analyzer/clas/eg1dvcs/specs.hh>
#include <analyzer/core/configuration.hh>

namespace analyzer {
namespace clas {
namespace eg1dvcs {

struct anaconf {
  std::string target_name;
  target_type target_material;
  bool enable_dq;

  anaconf(const ptree& settings, const string_path& path) {
    configuration conf {settings, path};
    // target info
    target_name = conf.get<std::string>("target", "other");
    update_target();
    // data quality?
    enable_dq = conf.get<bool>("enable_dq", true);
    LOG_INFO("anaconf", "DQ " + (enable_dq ? "enabled" : "disabled");
  }

private:
  update_target() {
    if (target_name == "NH3" || target_name == "nh3") {
      target_name = "NH3";
      target_material = target_type::NH3;
    } else if (target_name == "ND3" || target_name == "nd3") {
      target_name = "ND3";
      target_material = target_type::ND3;
    } else if (target_name == "empty" || target_name == "EMPTY") {
      target_name = "empty";
      target_material = target_type::EMPTY;
    } else {
      target_name = "other";
      target_material = target_type::OTHER;
    }
    LOG_INFO("anaconf", "Target material: " + target_name);
  }
};


} // ns eg1dvcs
} // ns clas
} // ns analyzer

#endif
