#include "excl.hh"

namespace analyzer {
namespace clas {

template <class Cut>
void exclusive_histos(Cut& c, std::shared_ptr<TFile> hfile);

exclusive_selection::exclusive_selection(const ptree& settings,
                                         const string_path& path,
                                         std::shared_ptr<TFile> hfile)
    : base_type{settings, path, "Exclusive Event Selection"}
    , event_cut_{settings,
                 path / "selection",
                 "Initial Event Selection",
                 {{"dvertex",
                   [](const exclusive_event& e) {
                     return (e.scat.vertex.Z() - e.recoil.vertex.Z());
                   }},
                  {"Eprod",
                   [](const exclusive_event& e) {
                     return (e.produced.p.E());
                   }}}}
    , hard_cut_{settings,
                path / "hard",
                "Hard Scattering Cuts",
                {{"W", [](const exclusive_event& e) { return e.incl.W; }},
                 {"y", [](const exclusive_event& e) { return e.incl.y; }},
                 {"Q2", [](const exclusive_event& e) { return e.incl.Q2; }}}}
    , exclusivity_cut_{
          settings,
          path / "exclusivity",
          "Exclusivity Cuts",
          {{"M2X", [](const exclusive_event& e) { return e.excl.M2X; }},
           {"M2X_A", [](const exclusive_event& e) { return e.excl.M2X_A; }},
           {"M2X_g", [](const exclusive_event& e) { return e.excl.M2X_g; }},
           {"EX", [](const exclusive_event& e) { return e.excl.pX.E(); }},
           {"coplanarity",
            [](const exclusive_event& e) {
              return radian_to_degree(e.excl.phi_coplanarity);
            }},
           {"cone_angle",
            [](const exclusive_event& e) {
              return radian_to_degree(e.excl.theta_cone);
            }},
           {"pXperp",
            [](const exclusive_event& e) { return e.excl.pXperp; }}}} {
    exclusive_histos(*this, hfile);
    exclusive_histos(event_cut_, hfile);
    exclusive_histos(hard_cut_, hfile);
    exclusive_histos(exclusivity_cut_, hfile);
}

template <class Cut>
void exclusive_histos(Cut& c, std::shared_ptr<TFile> hfile) {
  // 1D histos
  // * missing masses
  c.add_histo(hfile, "M2X", "M_{X=0}^{2}",
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               500,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A", "M_{x=A}^{2}",
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               500,
               {0., 5.}});
  c.add_histo(hfile, "M2X_g", "M_{X=#gamma}^{2}",
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               500,
               {-1.5, 3.5}});
  // missing pT and pPerp
  c.add_histo(hfile, "pX_T", "p_{T}^{X=0} (with z-axis)",
              {"p_{T}^{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.Perp(); },
               500,
               {0, 1.}});
  c.add_histo(hfile, "pXperp", "p_{#perp}^{X=0} (with virtual photon)",
              {"p_{#perp}^{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pXperp; },
               500,
               {0, 1.0}});
  // missing energy
  c.add_histo(hfile, "E_X", "E_{X=0}",
              {"E_{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.E(); },
               500,
               {-1., 3.}});
  // cone angle
  c.add_histo(hfile, "theta_cone", "Cone Angle (#gamma,X=#gamma)",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.theta_cone);
               },
               500,
               {-5, 55}});
  // coplanarity angles
  c.add_histo(hfile, "phi_coplanarity",
              "Coplanarity Angle (#gamma^{*}r,r#gamma^{*})",
              {"#phi [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.phi_coplanarity);
               },
               500,
               {-40, 40}});
  // angle between scattering and production plane
  c.add_histo(hfile, "phi_scat_prod",
              "Angle Between Scattering and Production Plane",
              {"#phi [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.phi_scat_prod);
               },
               500,
               {-40, 40}});
  // 2D histos
  // MX vs MX
  c.add_histo(hfile, "M2X_vs_M2X_A", "M_{X=0}^{2} vs M_{x=A}^{2}",
              {"M_{x=A}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}},
              {"M_{X=0}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_vs_g", "M_{X=0}^{2} vs M_{X=#gamma}^{2}",
              {"M_{X=#gamma}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}},
              {"M_{X=0}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_g", "M_{x=A}^{2} vs M_{X=#gamma}^{2}",
              {"M_{X=#gamma}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}},
              {"M_{x=A}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}});
  // MX vs pX
  c.add_histo(hfile, "M2X_gr_vs_pX", "M_{X=0}^{2} vs p_{X}",
              {"p_{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.Vect().Mag(); },
               150,
               {0, 1}},
              {"M_{X=0}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_pX", "M_{x=A}^{2} vs p_{X}",
              {"p_{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.Vect().Mag(); },
               150,
               {0, 1}},
              {"M_{x=A}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}});
  c.add_histo(hfile, "M2X_g_vs_pX", "M_{X=#gamma}^{2} vs p_{X}",
              {"p_{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.Vect().Mag(); },
               150,
               {0, 1}},
              {"M_{X=#gamma}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});

  // MX vs EX, pT and Pperp
  c.add_histo(hfile, "M2X_vs_EX", "M_{X=0}^{2} vs E_{X}",
              {"E_{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.E(); },
               150,
               {-1., 2.}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_vs_pXT", "M_{X=0}^{2} vs p_{T}^{X}",
              {"p_{T}^{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pX.Perp(); },
               150,
               {0, 0.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_vs_pXperp", "M_{X=0}^{2} vs p_{#perp}^{X}",
              {"p_{#perp}^{X} [GeV]",
               [](const exclusive_event& e) { return e.excl.pXperp; },
               150,
               {0, 0.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  // MX versus cone angle
  c.add_histo(hfile, "M2X_vs_cone", "M_{X=0}^{2} vs Cone Angle",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.theta_cone);
               },
               150,
               {-5, 55}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_cone", "M_{x=A}^{2} vs Cone Angle",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.theta_cone);
               },
               150,
               {-5, 55}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}});
  c.add_histo(hfile, "M2X_g_vs_cone", "M_{X=#gamma}^{2} vs Cone Angle",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.theta_cone);
               },
               150,
               {-5, 55}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus coplanarity angle
  c.add_histo(hfile, "M2X_vs_coplanarity", "M_{X=0}^{2} vs Coplanarity angle",
              {"#phi [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.phi_coplanarity);
               },
               150,
               {-40, 40}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_coplanarity", "M_{x=A}^{2} vs Coplanarity angle",
              {"#phi [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.phi_coplanarity);
               },
               150,
               {-40, 40}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}});
  c.add_histo(hfile, "M2X_g_vs_coplanarity",
              "M_{X=#gamma}^{2} vs Coplanarity angle",
              {"#phi [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.excl.phi_coplanarity);
               },
               150,
               {-40, 40}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus nu
  c.add_histo(hfile, "M2X_vs_nu", "M_{X=0}^{2} vs #nu",
              {"#nu [GeV]",
               [](const exclusive_event& e) { return e.incl.nu; },
               150,
               {2, 5.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X__A_vs_nu", "M_{X=A}^{2} vs #nu",
              {"#nu [GeV]",
               [](const exclusive_event& e) { return e.incl.nu; },
               150,
               {2, 5.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {-0, 5}});
  c.add_histo(hfile, "M2X_g_vs_nu", "M_{X=#gamma}^{2} vs #nu",
              {"#nu [GeV]",
               [](const exclusive_event& e) { return e.incl.nu; },
               150,
               {2, 5.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus W
  c.add_histo(hfile, "M2X_vs_W", "M_{X=0}^{2} vs W",
              {"W [GeV]",
               [](const exclusive_event& e) { return e.incl.W; },
               150,
               {1.5, 3.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_W", "M_{x=A}^{2} vs W",
              {"W [GeV]",
               [](const exclusive_event& e) { return e.incl.W; },
               150,
               {1.5, 3.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {-0, 5}});
  c.add_histo(hfile, "M2X_g_vs_W", "M_{X=#gamma}^{2} vs W",
              {"W [GeV]",
               [](const exclusive_event& e) { return e.incl.W; },
               150,
               {1.5, 3.5}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus theta_scat
  c.add_histo(
      hfile, "M2X_vs_theta_scat", "M_{X=0}^{2} vs #theta_{scat}",
      {"#theta [deg.]",
       [](const exclusive_event& e) { return radian_to_degree(e.scat.theta); },
       150,
       {10, 60}},
      {"M_{X}^{2} [GeV^{2}]",
       [](const exclusive_event& e) { return e.excl.M2X; },
       150,
       {-0.2, 0.2}});
  c.add_histo(
      hfile, "M2X_A_vs_theta_scat", "M_{x=A}^{2} vs #theta_{scat}",
      {"#theta [deg.]",
       [](const exclusive_event& e) { return radian_to_degree(e.scat.theta); },
       150,
       {10, 60}},
      {"M_{X=#gamma}^{2} [GeV^{2}]",
       [](const exclusive_event& e) { return e.excl.M2X_A; },
       150,
       {-0, 5}});
  c.add_histo(
      hfile, "M2X_g_vs_theta_scat", "M_{X=#gamma}^{2} vs #theta_{scat}",
      {"#theta [deg.]",
       [](const exclusive_event& e) { return radian_to_degree(e.scat.theta); },
       150,
       {10, 60}},
      {"M_{X}^{2} [GeV^{2}]",
       [](const exclusive_event& e) { return e.excl.M2X_g; },
       150,
       {-1.5, 3.5}});
  // MX versus theta_recoil
  c.add_histo(hfile, "M2X_vs_theta_A", "M_{X=0}^{2} vs #theta_{A}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.recoil.theta);
               },
               150,
               {10, 80}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_theta_A", "M_{x=A}^{2} vs #theta_{A}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.recoil.theta);
               },
               150,
               {10, 80}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {-0, 5}});
  c.add_histo(hfile, "M2X_g_vs_theta_A", "M_{X=#gamma}^{2} vs #theta_{A}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.recoil.theta);
               },
               150,
               {10, 80}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus theta_gamma
  c.add_histo(hfile, "M2X_vs_theta_g", "M_{X=0}^{2} vs #theta_{g}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.produced.theta);
               },
               150,
               {0, 50}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_theta_g", "M_{x=A}^{2} vs #theta_{g}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.produced.theta);
               },
               150,
               {0, 50}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {0, 5}});
  c.add_histo(hfile, "M2X_g_vs_theta_g", "M_{X=#gamma}^{2} vs #theta_{g}",
              {"#theta [deg.]",
               [](const exclusive_event& e) {
                 return radian_to_degree(e.produced.theta);
               },
               150,
               {0, 50}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus p_recoil
  c.add_histo(hfile, "M2X_vs_p_f", "M_{X=0}^{2} vs p_{A}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.recoil.mom; },
               150,
               {0, 4}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_p_A", "M_{X=A}^{2} vs p_{A}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.recoil.mom; },
               150,
               {0, 4}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {-0, 5}});
  c.add_histo(hfile, "M2X_g_vs_p_A", "M_{X=#gamma}^{2} vs p_{A}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.recoil.mom; },
               150,
               {0, 4}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // MX versus p_gamma
  c.add_histo(hfile, "M2X_vs_p_g", "M_{X=0}^{2} vs p_{#gamma}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.produced.mom; },
               150,
               {1.8, 6}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X; },
               150,
               {-0.2, 0.2}});
  c.add_histo(hfile, "M2X_A_vs_p_g", "M_{X=A}^{2} vs p_{#gamma}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.produced.mom; },
               150,
               {1.8, 6}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_A; },
               150,
               {-0, 5}});
  c.add_histo(hfile, "M2X_g_vs_p_g", "M_{X=#gamma}^{2} vs p_{#gamma}",
              {"p [GeV]",
               [](const exclusive_event& e) { return e.produced.mom; },
               150,
               {1.8, 6}},
              {"M_{X}^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.excl.M2X_g; },
               150,
               {-1.5, 3.5}});
  // =====================
  // kinematic spectra
  c.add_histo(hfile, "Q2_vs_xB", "Q^{2} vs x_{B}",
              {"x_{B}",
               [](const exclusive_event& e) { return e.incl.x; },
               150,
               {0.05, .55}},
              {"Q^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.incl.Q2; },
               150,
               {.5, 5}});
  c.add_histo(hfile, "t_vs_Q2", "t vs Q^{2}",
              {"Q^{2} [GeV^{2}]",
               [](const exclusive_event& e) { return e.incl.Q2; },
               150,
               {.5, 5}},
              {"-t [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t; },
               150,
               {-1, 5}});
  // t spectra
  c.add_histo(hfile, "t_min_vs_t", "-t_{min} vs t",
              {"-t [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t; },
               150,
               {-1, 5}},
              {"t_{min} [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t_min; },
               150,
               {-4, 4}});
  c.add_histo(hfile, "t_gamma_vs_t", "t_{#gamma} vs t",
              {"-t [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t; },
               150,
               {-1, 5}},
              {"-t_{#gamma} [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t_gamma; },
               150,
               {-1, 5}});
  c.add_histo(hfile, "t_c_vs_t", "t_{c} vs t",
              {"-t [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t; },
               150,
               {-1, 5}},
              {"-t_{#gamma} [GeV^{2}]",
               [](const exclusive_event& e) { return -e.excl.t_c; },
               150,
               {-4, 4}});
}

} // ns clas
} // ns analyzer
