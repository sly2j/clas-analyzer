################################################################################
## CMAKE Settings
################################################################################
set (EXE "analyze_clas_eg1dvcs_dvcs14N")
set (TARGETS ${TARGETS} ${EXE} PARENT_SCOPE)


################################################################################
## Sources and install headers
################################################################################
set (SOURCES "dvcs14N.cc" "excl.cc")

################################################################################
## Include directories
################################################################################
include_directories("${PROJECT_SOURCE_DIR}")
include_directories("${CMAKE_CURRENT_SOURCE_DIR}")

################################################################################
## External Libraries
################################################################################
## ROOT
find_package(ROOT REQUIRED)
include_directories(${ROOT_INCLUDE_DIR})
## boost
find_package(Boost COMPONENTS program_options filesystem system REQUIRED)
include_directories(${Boost_INCLUDE_DIRS})

################################################################################
## Compile and Link
################################################################################
add_executable(${EXE} ${SOURCES})
target_link_libraries(${EXE} 
  analyzer_core 
  analyzer_physics 
  analyzer_clas
  ${ROOT_LIBRARIES} ${Boost_LIBRARIES})
set_target_properties(${EXE} PROPERTIES VERSION ${ANALYZER_VERSION})

################################################################################
## Export and Install
################################################################################
install(TARGETS ${EXE}
  EXPORT ${PROJECT_NAME}-targets
  RUNTIME DESTINATION "${INSTALL_BIN_DIR}" COMPONENT bin)
