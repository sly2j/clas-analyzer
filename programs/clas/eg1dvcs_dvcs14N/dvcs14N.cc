#include <analyzer/core/progress_meter.hh>
#include <analyzer/core/framework.hh>
#include <analyzer/core/logger.hh>
#include <analyzer/physics/scattering.hh>

#include <analyzer/clas/index.hh>
#include <analyzer/clas/eg1dvcs/h22_reader.hh>
#include <analyzer/clas/eg1dvcs/electron.hh>
#include <analyzer/clas/eg1dvcs/ic_photon.hh>
#include <analyzer/clas/eg1dvcs/kaon.hh>
#include <analyzer/clas/eg1dvcs/proton.hh>
#include <analyzer/clas/eg1dvcs/anaconf.hh>
#include <analyzer/clas/eg1dvcs/specs.hh>

#include "excl.hh"

#include <memory>

using namespace analyzer;
using namespace analyzer::clas;
using namespace analyzer::clas::eg1dvcs;

int analyze_dvcs(const ptree& settings, const std::vector<std::string>& input,
                  const std::string& output) {

  // analyzer configuration:
  anaconf conf{settings, "main"};

  // output file
  std::shared_ptr<TFile> ofile{
      std::make_shared<TFile>((output + ".root").c_str(), "recreate")};

  // output dvcs event
  using dvcs_writer_type = tree_writer<exclusive_event>;
  dvcs_writer_type p_excl_out{ofile, "dvcs_p_excl"};
  dvcs_writer_type p_excl_v2_out{ofile, "dvcs_p_excl_v2"};
  dvcs_writer_type A_excl_out{ofile, "dvcs_A_excl"};
  dvcs_writer_type p_veto_out{ofile, "dvcs_p_veto"};
  dvcs_writer_type A_veto_out{ofile, "dvcs_A_veto"};
  dvcs_writer_type A_out{ofile, "dvcs_A"};
  dvcs_writer_type p_out{ofile, "dvcs_p"};

  // dvcs event selection
  //using dvcs_selection_type = clas::exclusive_selection;
  //dvcs_selection_type dvcs_selector{settings, "dvcs", ofile};

  // input reader
  using reader_type = clas::eg1dvcs::h22_reader;
  reader_type reader{input};

  // our "detectors"
  using electron_detector_type =
      clas::eg1dvcs::leading_electron_detector<reader_type>;
  using ic_photon_detector_type =
      clas::eg1dvcs::ic_photon_detector<reader_type>;
  using kaon_detector_type = clas::eg1dvcs::kaon_detector<reader_type>;
  using proton_detector_type = clas::eg1dvcs::proton_detector<reader_type>;
  electron_detector_type electron_detector{settings, "detector/electron",
                                           ofile};
  ic_photon_detector_type ic_photon_detector{settings, "detector/ic_photon",
                                             ofile};
  kaon_detector_type k_plus_detector{settings, "detector/k_plus",
                                     kaon_detector_type::flavor::PLUS, ofile};
  kaon_detector_type k_minus_detector{settings, "detector/k_minus",
                                      kaon_detector_type::flavor::MINUS, ofile};
  proton_detector_type proton_detector{settings, "detector/proton",
                                      proton_detector_type::flavor::PLUS, ofile};

  // particle type
  using particle_type = clas_particle;

  // a nice progress meter
  progress_meter progress{reader.size()};

  // skim loop
  while (reader.next()) {
    progress.update();

    // basic DQ and check if we have the right target
    if (conf.enable_dq && !reader.slow.good_run) {
      LOG_JUNK("analyzer", "DQ failed, skipping...");
      continue;
    } else if (reader.slow.target_material != conf.target_material) {
      LOG_JUNK("analyzer", "Target check failed, skipping...");
      continue;
    }
    LOG_JUNK("analyzer", "Analyzing good event candidate");

    // get number of particles in CLAS and IC
    // TODO: this should be done automatically by the reader
    const size_t nEVNT{clas::index::nEVNT(reader)};
    const size_t nIC{static_cast<size_t>(
        (*reader.ic.svicpart > 1) ? *reader.ic.svicpart - 1 : 0)};

    // look for candidate primary electrons
    auto electrons = electron_detector.detect(reader, nEVNT);
    if (electrons.size() < 1) {
      continue;
    }

    // get some kaons for michael
    auto kp = k_plus_detector.detect(reader, nEVNT, electrons[0]);
    auto km = k_minus_detector.detect(reader, nEVNT, electrons[0]);

    // protons for fully exclusive events
    auto protons = proton_detector.detect(reader, nEVNT, electrons[0]);

    // photons for all dvcs events
    auto photons = ic_photon_detector.detect(reader, nIC, electrons[0]);

    // some raw event selection cuts
    if (electrons.size() != 1 || photons.size() == 0) {
      continue;
    }
    if (photons[0].mom < 1) {
      //continue;
    }
    
    // fully exclusive proton DVCS selection
    if (protons.size() == 1) {
      exclusive_event ev_p{reader.slow.beam_energy, PDG_PROTON, electrons[0],
                           photons[0], protons[0]};
      //if (ev_p.incl.Q2 < 1 || ev_p.incl.W < 2 || ev_p.incl.y > .85) {
      //  continue;
      //}
      exclusive_event ev_p_v2{reader.slow.beam_energy, PDG_PROTON, electrons[0],
                              photons[0], PDG_PROTON};
      exclusive_event ev_A{reader.slow.beam_energy, PDG_N14, electrons[0],
                           photons[0], PDG_N14};
      p_excl_out.fill(ev_p);
      p_excl_v2_out.fill(ev_p_v2);
      A_excl_out.fill(ev_A);
    }
    // proton and nitrogen DVCS with a proton veto
    if (protons.size() == 0) {
      exclusive_event ev_p{reader.slow.beam_energy, PDG_PROTON, electrons[0],
                           photons[0], PDG_PROTON};
      //if (ev_p.incl.Q2 < 1 || ev_p.incl.W < 2 || ev_p.incl.y > .85) {
      //  continue;
      //}
      exclusive_event ev_A{reader.slow.beam_energy, PDG_N14, electrons[0],
                           photons[0], PDG_N14};
      p_veto_out.fill(ev_p);
      A_veto_out.fill(ev_A);
    }
    // proton and nitrogen DVCS without any proton requirements
    {
      exclusive_event ev_p{reader.slow.beam_energy, PDG_PROTON, electrons[0],
                           photons[0], PDG_PROTON};
      //if (ev_p.incl.Q2 < 1 || ev_p.incl.W < 2 || ev_p.incl.y > .85) {
      //  continue;
      //}
      exclusive_event ev_A{reader.slow.beam_energy, PDG_N14, electrons[0],
                           photons[0], PDG_N14};
      p_out.fill(ev_p);
      A_out.fill(ev_A);
    }
  }

  return 0;
}

#ifndef MAKE_ANALYZER_FRAMEWORK
#error analyzer framework not defined
#else
MAKE_ANALYZER_FRAMEWORK(analyze_dvcs);
#endif
