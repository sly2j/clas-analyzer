# analyzer

A physics data analysis framework using ROOT6, written in C++11/14.
It features various analysis routines for analysis of old CLAS data.